function varargout = miniViewerGUI(varargin)
% Mini Viewer Main GUI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Opens the main GUI and handles all user interactions
%
% Author: Torsten Hopp
%         Karlsruhe Institute of Technology
%         Institute for Data Processing and Electronics
%
% email:  torsten.hopp@kit.edu
% Website: http://www.ipe.kit.edu
%
%
%
%


gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @miniViewerGUI_OpeningFcn, ...
    'gui_OutputFcn',  @miniViewerGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if(nargin == 0)
    error('Calling this function without argument is not allowed!');
end

if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end



%% Opening Function: Executes just before miniViewerGUI is made visible. %%
%  Performs basic settings
function miniViewerGUI_OpeningFcn(hObject, ~, handles, varargin)

handles.output = hObject;
handles.drawHistogram = true;
handles.linkedFig = [];
handles.windowWidthGiven = false;
handles.windowCenterGiven = false;


set(handles.optionsMenu_linkedViewer,'Enable','off');
set(handles.optionsMenu_linkedViewer_enableSlicingLink,'Enable','off');
set(handles.optionsMenu_linkedViewer_enableWindowingLink,'Enable','off');

% transfer data to internal variable
if (~isempty(varargin))
    if(ndims(varargin{1})>2) %#ok<ISMAT>
        handles.volume = varargin{1};
    end
    
    % init of default values
    handles.imageResolutionAvailable = 0; % default: resolution not available
    handles.startpointAvailable = 0;
    
    centerSliceX = round(size(handles.volume,1) / 2);  % default setting: show center slice
    centerSliceY = round(size(handles.volume,2) / 2);
    centerSliceZ = round(size(handles.volume,3) / 2);
    
    if(size(varargin,2) == 2) % handle the input argument structure parsed before
        pResult = varargin{2};
        if(~isempty(pResult.resolution))
            if(length(pResult.resolution) == 3)
                handles.imageResolution = pResult.resolution; % [3 x 1] --> res in all dimensions
            else
                handles.imageResolution = [pResult.resolution pResult.resolution pResult.resolution]; % assuming isotropic resolution
            end
            handles.imageResolutionAvailable = 1;
        else
            handles.imageResolution = [1 1 1];
        end
        
        if(~isempty(pResult.startpoint))
            handles.imageStartpoint = pResult.startpoint;
            handles.startpointAvailable = 1;
        else
            handles.imageStartpoint = [0 0 0];
        end
        
        if(~isempty(pResult.initSlice)) % if initial slices are given: overwrite this variable
            if(pResult.initSlice(1) > 0 && pResult.initSlice(1) <= size(handles.volume,1) && pResult.initSlice(2) > 0 && pResult.initSlice(2) <= size(handles.volume,2) && pResult.initSlice(3) > 0 && pResult.initSlice(3) <= size(handles.volume,3))
                centerSliceX = pResult.initSlice(1);
                centerSliceY = pResult.initSlice(2);
                centerSliceZ = pResult.initSlice(3);
            else
                disp('[MiniViewer:] Warning - initial slices given are out of bounce. Check the values to be > 0 and <= volume size! Using default center slices instead.');
            end
        end
        
        
        if(~isempty(pResult.linkTo)) % if handle to another miniViewer is passed
            linkedHandles = guidata(pResult.linkTo);
            if(size(handles.volume,1) == size(linkedHandles.volume,1) && size(handles.volume,2) == size(linkedHandles.volume,2) && size(handles.volume,3) == size(linkedHandles.volume,3) && size(handles.volume,4) == size(linkedHandles.volume,4))
                handles.linkedFig = pResult.linkTo;
                
                % set the current MiniViewer instance as linked axes in the
                % linked miniViewer instance
                tempHandles = guidata(handles.linkedFig);
                tempHandles.linkedFig = hObject;
                guidata(handles.linkedFig,tempHandles);
                
                set(handles.optionsMenu_linkedViewer,'Enable','on');
                set(handles.optionsMenu_linkedViewer_enableSlicingLink,'Enable','on');
                set(handles.optionsMenu_linkedViewer_enableWindowingLink,'Enable','on');
                
                set(tempHandles.optionsMenu_linkedViewer,'Enable','on');
                set(tempHandles.optionsMenu_linkedViewer_enableSlicingLink,'Enable','on');
                set(tempHandles.optionsMenu_linkedViewer_enableWindowingLink,'Enable','on');
            else
                disp('[MiniViewer:] Warning - link to given MiniViewer is not working since volumes have different size');
                set(handles.optionsMenu_linkedViewer,'Enable','off');
                set(handles.optionsMenu_linkedViewer_enableSlicingLink,'Enable','off');
                set(handles.optionsMenu_linkedViewer_enableWindowingLink,'Enable','off');
            end
        end

        if(~isempty(pResult.histogram))
            handles.drawHistogram = pResult.histogram;
        end
        
        if(~isempty(pResult.windowWidth))
            handles.windowWidthGiven = true;
        end
        
        if(~isempty(pResult.windowCenter))
            handles.windowCenterGiven = true;
        end
    end
else
    return;
end

% addig subpaths
currentFile = mfilename('fullpath');
[miniViewerGUIDir] = fileparts(currentFile);
miniViewerDir = strrep(miniViewerGUIDir,'mainGUI','');
% check if already in path
if isempty(strfind(path,fullfile(miniViewerDir,'helpers')))
    addpath(genpath(fullfile(miniViewerDir,'helpers')));
end

handles.miniViewerDir = miniViewerDir;

% Fill Menu with possible colormaps
listBoxString = {   'jet'; ...
    'hsv'; ...
    'hot'; ...
    'cool';...
    'spring';...
    'summer';...
    'autumn';...
    'winter';...
    'gray';...
    'bone';...
    'copper';...
    'pink';...
    'lines';...
    'parula';};
for n = 1:size(listBoxString,1)
    handles.optionsMenu_colormap_set(n) = uimenu(handles.optionsMenu_colormap_colormapSetting,'Label',listBoxString{n},'Tag',num2str(n),'Callback',@colormapListCallback);
end


% Trying to load the user settings
userSettingAvailable = 0;
try
    load(fullfile(handles.miniViewerDir,'userSettings.mat'),'userSetting');
    userSettingAvailable = 1;
catch %#ok<*CTCH>
    disp('   User settings not available. Using default values instead.');
end

% initial setting: colormap
if(userSettingAvailable == 1)
    colormap(hObject,userSetting.colormap);
    handles.colormap = userSetting.colormap;
    
    %reset checked items in menu
    set(handles.optionsMenu_colormap_set,'Checked','off');
    
    for idx=1:size(handles.optionsMenu_colormap_set,2)
        if(strcmp(get(handles.optionsMenu_colormap_set(idx),'Label'),userSetting.colormap))
            set(handles.optionsMenu_colormap_set(idx),'Checked','on');
        end
    end
else
    handles.colormap = 'jet';
end

% initial setting: save directory for exported images
if(userSettingAvailable == 1)
    try
        handles.lastSaveDir = userSetting.lastSaveDir;
    catch
        handles.lastSaveDir = pwd;
    end
else
    handles.lastSaveDir = pwd;
end


% initial setting: colormap is fixed or user setting
if(userSettingAvailable == 1)
    set(handles.optionsMenu_colormap_keepFixed,'Checked',userSetting.colormapFixed);
    handles.cmapFixed = userSetting.colormapFixed;
else
    set(handles.optionsMenu_colormap_keepFixed,'Checked','on');
    handles.cmapFixed = 'on';
end


handles.linkSlicing = true;
handles.linkWindowing = true;
if(userSettingAvailable == 1)
    handles.linkSlicing = userSetting.linkSlicing;
    handles.linkWindowing = userSetting.linkWindowing;   
end

if(handles.linkSlicing)
    set(handles.optionsMenu_linkedViewer_enableSlicingLink,'Checked','on');
else
    set(handles.optionsMenu_linkedViewer_enableSlicingLink,'Checked','off');
end
if(handles.linkWindowing)
    set(handles.optionsMenu_linkedViewer_enableWindowingLink,'Checked','on');
else
    set(handles.optionsMenu_linkedViewer_enableWindowingLink,'Checked','off');
end



if(size(size(handles.volume),2) == 4)
    % default values if image startpoint is not given
    s = size(squeeze(handles.volume(centerSliceX,:,:,:)));
    xAxValuesX = 1:s(2);
    xAxValuesY = 1:s(1);
    
    s = size(squeeze(handles.volume(:,centerSliceY,:,:)));
    yAxValuesX = 1:s(2);
    yAxValuesY = 1:s(1);
    
    s = size(squeeze(handles.volume(:,:,centerSliceZ,:)));
    zAxValuesX = 1:s(2);
    zAxValuesY = 1:s(1);
    
    % if startpoint in real world coordinates is available --> update the axes
    % of the plots.
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        xAxValuesX = handles.imageStartpoint(3) + ((xAxValuesX - 1) .* handles.imageResolution(3));
        xAxValuesY = handles.imageStartpoint(2) + ((xAxValuesY - 1) .* handles.imageResolution(2));
        yAxValuesX = handles.imageStartpoint(3) + ((yAxValuesX - 1) .* handles.imageResolution(3));
        yAxValuesY = handles.imageStartpoint(1) + ((yAxValuesY - 1) .* handles.imageResolution(1));
        zAxValuesX = handles.imageStartpoint(2) + ((zAxValuesX - 1) .* handles.imageResolution(2));
        zAxValuesY = handles.imageStartpoint(1) + ((zAxValuesY - 1) .* handles.imageResolution(1));
    end
    
    isItRGB = 1;% RGB
    set(handles.optionsMenu_colormap,'Enable','off');
    handles.xImage = image(xAxValuesX,xAxValuesY,squeeze(handles.volume(centerSliceX,:,:,:)),'Parent',handles.axesX);
    handles.yImage = image(yAxValuesX,yAxValuesY,squeeze(handles.volume(:,centerSliceY,:,:)),'Parent',handles.axesY);
    handles.zImage = image(zAxValuesX,zAxValuesY,squeeze(handles.volume(:,:,centerSliceZ,:)),'Parent',handles.axesZ);
    
else
    
    % default values if image startpoint is not given
    s = size(squeeze(handles.volume(centerSliceX,:,:)));
    xAxValuesX = 1:s(2);
    xAxValuesY = 1:s(1);
    
    s = size(squeeze(handles.volume(:,centerSliceY,:)));
    yAxValuesX = 1:s(2);
    yAxValuesY = 1:s(1);
    
    s = size(squeeze(handles.volume(:,:,centerSliceZ)));
    zAxValuesX = 1:s(2);
    zAxValuesY = 1:s(1);
    
    % if startpoint and/or resoltuion in real world coordinates is available --> update the axes of the plots.
    if(handles.startpointAvailable == 1 || handles.imageResolutionAvailable == 1)
        xAxValuesX = handles.imageStartpoint(3) + ((xAxValuesX - 1) .* handles.imageResolution(3));
        xAxValuesY = handles.imageStartpoint(2) + ((xAxValuesY - 1) .* handles.imageResolution(2));
        yAxValuesX = handles.imageStartpoint(3) + ((yAxValuesX - 1) .* handles.imageResolution(3));
        yAxValuesY = handles.imageStartpoint(1) + ((yAxValuesY - 1) .* handles.imageResolution(1));
        zAxValuesX = handles.imageStartpoint(2) + ((zAxValuesX - 1) .* handles.imageResolution(2));
        zAxValuesY = handles.imageStartpoint(1) + ((zAxValuesY - 1) .* handles.imageResolution(1));
    end
    isItRGB = 0;
    handles.xImage = imagesc(xAxValuesX,xAxValuesY,squeeze(handles.volume(centerSliceX,:,:)),'Parent',handles.axesX);
    handles.yImage = imagesc(yAxValuesX,yAxValuesY,squeeze(handles.volume(:,centerSliceY,:)),'Parent',handles.axesY);
    handles.zImage = imagesc(zAxValuesX,zAxValuesY,squeeze(handles.volume(:,:,centerSliceZ)),'Parent',handles.axesZ);
    
    
    
    % Enabling colormap chosing menu
    set(handles.optionsMenu_colormap,'Enable','on');
end

set(handles.axesX,'DataAspectRatio',[1 1 1],'DataAspectRatioMode','manual','PlotBoxAspectRatioMode','auto');
set(handles.axesY,'DataAspectRatio',[1 1 1],'DataAspectRatioMode','manual','PlotBoxAspectRatioMode','auto');
set(handles.axesZ,'DataAspectRatio',[1 1 1],'DataAspectRatioMode','manual','PlotBoxAspectRatioMode','auto');

set(handles.xImage,'Tag','x');
set(handles.yImage,'Tag','y');
set(handles.zImage,'Tag','z');

set(handles.xImage,'ButtonDownFcn',@sliceButtonDownFcn);
set(handles.yImage,'ButtonDownFcn',@sliceButtonDownFcn);
set(handles.zImage,'ButtonDownFcn',@sliceButtonDownFcn);

set(handles.xImage,'UIContextMenu',handles.sliceImageContextMenu);
set(handles.yImage,'UIContextMenu',handles.sliceImageContextMenu);
set(handles.zImage,'UIContextMenu',handles.sliceImageContextMenu);



if(userSettingAvailable == 1 && isfield(userSetting, 'swapXY'))
    handles.swapXY = userSetting.swapXY;
    if(handles.swapXY == 1)
        set(handles.axesX, 'View',[-90 90]);
        set(handles.axesY, 'View',[-90 90]);
        set(handles.axesZ, 'View',[-90 90]);
        set(handles.optionsMenu_swapXY,'Checked','on');
    else
        set(handles.axesX, 'View',[0 90]);
        set(handles.axesY, 'View',[0 90]);
        set(handles.axesZ, 'View',[0 90]);
        set(handles.optionsMenu_swapXY,'Checked','off');
    end
else % default if not in userSetting
    handles.swapXY = 0;
    set(handles.axesX, 'View',[0 90]);
    set(handles.axesY, 'View',[0 90]);
    set(handles.axesZ, 'View',[0 90]);
end


handles.xFrameArray = [handles.xColorFrame1 handles.xColorFrame2 handles.xColorFrame3 handles.xColorFrame4];
handles.yFrameArray = [handles.yColorFrame1 handles.yColorFrame2 handles.yColorFrame3 handles.yColorFrame4];
handles.zFrameArray = [handles.zColorFrame1 handles.zColorFrame2 handles.zColorFrame3 handles.zColorFrame4];

handles.xNormalColor = [0.6 0 0];
handles.xHighlightColor = [1 0 0];

handles.yNormalColor = [0 0.6 0];
handles.yHighlightColor = [0 1 0];

handles.zNormalColor = [0 0 0.6];
handles.zHighlightColor = [0 0 1];

handles.activeSlice = [];

handles.windowButtonDown = 0;

handles.currentViewPosition = [0 90];

set(handles.xFrameArray,'BackgroundColor',handles.xNormalColor);
set(handles.yFrameArray,'BackgroundColor',handles.yNormalColor);
set(handles.zFrameArray,'BackgroundColor',handles.zNormalColor);

% setting colors of x/y-ticks for axes to white
set(handles.axesX,'XColor',[1 1 1]);
set(handles.axesX,'YColor',[1 1 1]);
set(handles.axesX,'FontSize',8);

set(handles.axesY,'XColor',[1 1 1]);
set(handles.axesY,'YColor',[1 1 1]);
set(handles.axesY,'FontSize',8);

set(handles.axesZ,'XColor',[1 1 1]);
set(handles.axesZ,'YColor',[1 1 1]);
set(handles.axesZ,'FontSize',8);

set(handles.histogramAxes,'XColor',[0.3 0.3 0.3]);
set(handles.histogramAxes,'YColor',[0.3 0.3 0.3]);
set(handles.histogramAxes,'Color',[0.2 0.2 0.2]);
set(handles.histogramAxes,'XTick',[]);
set(handles.histogramAxes,'YTick',[]);


% initially setting the edit boxes to the displayed slice
set(handles.xSliceEditbox,'String',num2str(centerSliceX));
set(handles.ySliceEditbox,'String',num2str(centerSliceY));
set(handles.zSliceEditbox,'String',num2str(centerSliceZ));

xAxYlineX = [size(handles.volume,3)*0.1 size(handles.volume,3) * 0.9];
xAxYlineY = [centerSliceY centerSliceY];
xAxZlineX = [centerSliceZ centerSliceZ];
xAxZlineY = [size(handles.volume,2)*0.1 size(handles.volume,2) * 0.9];

yAxXlineX = [size(handles.volume,3)*0.1 size(handles.volume,3) * 0.9];
yAxXlineY = [centerSliceX centerSliceX];
yAxZlineX = [centerSliceZ centerSliceZ];
yAxZlineY = [size(handles.volume,1)*0.1 size(handles.volume,1) * 0.9];

zAxXlineX = [size(handles.volume,2)*0.1 size(handles.volume,2) * 0.9];
zAxXlineY = [centerSliceX centerSliceX];
zAxYlineX = [centerSliceY centerSliceY];
zAxYlineY = [size(handles.volume,1)*0.1 size(handles.volume,1) * 0.9];


if(handles.startpointAvailable == 1 || handles.imageResolutionAvailable == 1)
    xAxYlineX = handles.imageStartpoint(3) + ((xAxYlineX - 1) .* handles.imageResolution(3));
    xAxYlineY = handles.imageStartpoint(2) + ((xAxYlineY - 1) .* handles.imageResolution(2));
    xAxZlineX = handles.imageStartpoint(3) + ((xAxZlineX - 1) .* handles.imageResolution(3));
    xAxZlineY = handles.imageStartpoint(2) + ((xAxZlineY - 1) .* handles.imageResolution(2));
    
    yAxXlineX = handles.imageStartpoint(3) + ((yAxXlineX - 1) .* handles.imageResolution(3));
    yAxXlineY = handles.imageStartpoint(1) + ((yAxXlineY - 1) .* handles.imageResolution(1));
    yAxZlineX = handles.imageStartpoint(3) + ((yAxZlineX - 1) .* handles.imageResolution(3));
    yAxZlineY = handles.imageStartpoint(1) + ((yAxZlineY - 1) .* handles.imageResolution(1));
    
    zAxYlineX = handles.imageStartpoint(2) + ((zAxYlineX - 1) .* handles.imageResolution(2));
    zAxYlineY = handles.imageStartpoint(1) + ((zAxYlineY - 1) .* handles.imageResolution(1));
    zAxXlineX = handles.imageStartpoint(2) + ((zAxXlineX - 1) .* handles.imageResolution(2));
    zAxXlineY = handles.imageStartpoint(1) + ((zAxXlineY - 1) .* handles.imageResolution(1));
end

% drawing line representing y slice
handles.xly = line(xAxYlineX,xAxYlineY,'Color',[0 1 0],'LineWidth',1,'Tag','xly','Parent',handles.axesX);
% drawing line representing z slice
handles.xlz = line(xAxZlineX,xAxZlineY,'Color',[0 0 1],'LineWidth',1,'Tag','xlz','Parent',handles.axesX);
draggable(handles.xly,'v',[1 size(handles.volume,2)],@lineMoved);
draggable(handles.xlz,'h',[1 size(handles.volume,3)],@lineMoved);
set(handles.xSliceSlider,'min',1,'max',size(handles.volume,1),'Value',centerSliceX,'SliderStep',[1/size(handles.volume,1) 1/size(handles.volume,1)*3]);

% drawing line representing x slice
handles.ylx = line(yAxXlineX,yAxXlineY,'Color',[1 0 0],'LineWidth',1,'Tag','ylx','Parent',handles.axesY);
% drawing line representing z slice
handles.ylz = line(yAxZlineX,yAxZlineY,'Color',[0 0 1],'LineWidth',1,'Tag','ylz','Parent',handles.axesY);
draggable(handles.ylx,'v',[1 size(handles.volume,1)],@lineMoved);
draggable(handles.ylz,'h',[1 size(handles.volume,3)],@lineMoved);
set(handles.ySliceSlider,'min',1,'max',size(handles.volume,2),'Value',centerSliceY,'SliderStep',[1/size(handles.volume,2) 1/size(handles.volume,2)*3]);

% drawing line representing x slice
handles.zlx = line(zAxXlineX,zAxXlineY,'Color',[1 0 0],'LineWidth',1,'tag','zlx','Parent',handles.axesZ);
% drawing line representing y slice
handles.zly = line(zAxYlineX,zAxYlineY,'Color',[0 1 0],'LineWidth',1,'tag','zly','Parent',handles.axesZ);
draggable(handles.zlx,'v',[1 size(handles.volume,1)],@lineMoved);
draggable(handles.zly,'h',[1 size(handles.volume,2)],@lineMoved);
set(handles.zSliceSlider,'min',1,'max',size(handles.volume,3),'Value',centerSliceZ,'SliderStep',[1/size(handles.volume,3) 1/size(handles.volume,3)*3]);


% setting the CLims for all axes if colormap is fixed
handles.maxValue = double(max(handles.volume(:)));
handles.minValue = double(min(handles.volume(:)));


if(isfloat(handles.volume))
    % fix for floating point numbers... eps alone is too small for
    % large max values. increasing the eps-size by the number number of
    % digits of the max value
    try
        eval(sprintf('f = 1e%i;',(floor(log10(handles.maxValue)))));
    catch
        f = 1;
    end
    handles.addOnEps = eps*f;
else
    handles.addOnEps = eps;
end

% catching the case that all voxels have the same intensity ...
if(handles.minValue == handles.maxValue)
    if(isfloat(handles.volume)) % adding eps in case of floating point volumes
        handles.maxValue = handles.minValue + handles.addOnEps;
    elseif(isinteger(handles.volume))  % adding 1 in case of integer volumes
        handles.maxValue = handles.minValue + 1;
    elseif(islogical(handles.volume)) % fixed range
        handles.minValue = 0;
        handles.maxValue = 1;
    end
end



if(isItRGB == 1)
    handles.maxValue = -1;
    handles.minValue = -1;
elseif(isItRGB == 0)
    if(strcmp(handles.cmapFixed,'on'))
        set(handles.axesX,'CLimMode','manual');
        set(handles.axesX,'CLim',[handles.minValue handles.maxValue]);
        set(handles.axesY,'CLimMode','manual');
        set(handles.axesY,'CLim',[handles.minValue handles.maxValue]);
        set(handles.axesZ,'CLimMode','manual');
        set(handles.axesZ,'CLim',[handles.minValue handles.maxValue]);
        set(handles.volumeViewAxes,'CLimMode','manual');
        set(handles.volumeViewAxes,'CLim',[handles.minValue handles.maxValue]);
    elseif(strcmp(handles.cmapFixed,'off'))
        set(handles.axesX,'CLimMode','auto');
        set(handles.axesY,'CLimMode','auto');
        set(handles.axesZ,'CLimMode','auto');
        set(handles.volumeViewAxes,'CLimMode','auto');
    end
end



% setting up volume view axes. volume view is disabled at startup
handles.volumeViewEnables = 0;
set(handles.volumeViewAxes,'Visible','off');
handles.volumeViewPos = get(handles.volumeViewAxes,'Position');
    

% setting the azimuth and elevation sliders & editboxes initially
set(handles.azimuthSlider,'Value',0);
set(handles.elevationSlider,'Value',45);
set(handles.azimuthEditbox,'String',0);
set(handles.elevationEditbox,'String',45);
set(handles.alphamapSlider,'Value',1);
set(handles.alphamapEditbox,'String',1);

% setting the threshold sliders to initial values
if(~isItRGB)
    set(handles.lowerThresholdSlider,'Min',handles.minValue);
    set(handles.lowerThresholdSlider,'Max',handles.maxValue);
    set(handles.upperThresholdSlider,'Min',handles.minValue);
    set(handles.upperThresholdSlider,'Max',handles.maxValue);
    set(handles.lowerThresholdSlider,'Value',handles.minValue);
    set(handles.upperThresholdSlider,'Value',handles.maxValue);
    %setting the windowing sliders to initial values
    set(handles.windowCenterSlider,'Min',handles.minValue);
    set(handles.windowCenterSlider,'Max',handles.maxValue);
else
    set(handles.upperThresholdSlider,'Value',0.5);
    set(handles.lowerThresholdSlider,'Value',0.5);
    set(handles.lowerThresholdSlider,'Min',0);
    set(handles.lowerThresholdSlider,'Max',1);
    set(handles.upperThresholdSlider,'Min',0);
    set(handles.upperThresholdSlider,'Max',1);
    set(handles.lowerThresholdSlider,'Enable','off');
    set(handles.upperThresholdSlider,'Enable','off');
    %setting the windowing sliders to initial values
    set(handles.windowCenterSlider,'Min',0);
    set(handles.windowCenterSlider,'Max',1);
end

% setting up measurement tool
% handles.measurementLineHandle = line([0 1],[0 1]);
% handles.measurementTextHandle = text(0,0,'');
% set(handles.measurementLineHandle,'Visible','off');
% set(handles.measurementTextHandle,'Visible','off');


if(~isItRGB)
    % width has to be normalized to [0 max] for images which contain negative
    % values! width is not allowed to be zero!
    maxWidth = handles.maxValue - handles.minValue;
    minWidth = 0.0001*maxWidth; % min value relative to max value very small
    set(handles.windowWidthSlider,'Min',minWidth);
    set(handles.windowWidthSlider,'Max',maxWidth);
    
    if(handles.windowCenterGiven)
        set(handles.windowCenterSlider,'Value',pResult.windowCenter);
    else
        set(handles.windowCenterSlider,'Value',handles.minValue + 0.5 * (handles.maxValue - handles.minValue));
    end
    
    if(handles.windowWidthGiven)
        set(handles.windowWidthSlider,'Value',pResult.windowWidth);
    else
        set(handles.windowWidthSlider,'Value',maxWidth);
    end
    set(handles.windowCenterSlider,'UIContextMenu',handles.windowCenterContextMenu);
    set(handles.windowWidthSlider,'UIContextMenu',handles.windowWidthContextMenu);
    set(handles.windowCenterEditbox,'UIContextMenu',handles.windowCenterContextMenu);
    set(handles.windowWidthEditbox,'UIContextMenu',handles.windowWidthContextMenu);
else
    set(handles.windowWidthSlider,'Min',0);
    set(handles.windowWidthSlider,'Max',1);
    set(handles.windowCenterSlider,'Value',0.5);
    set(handles.windowWidthSlider,'Value',1);
end




if(isItRGB)
    set(handles.windowCenterSlider,'Enable','off');
    set(handles.windowWidthSlider,'Enable','off');
    set(handles.windowCenterEditbox,'Enable','off');
    set(handles.windowWidthEditbox,'Enable','off');
else
    set(handles.windowWidthEditbox,'String',num2str(get(handles.windowWidthSlider,'Value')));
    set(handles.windowCenterEditbox,'String',num2str(get(handles.windowCenterSlider,'Value')));
end

handles.fig = hObject;
set(handles.fig,'InvertHardcopy','off');


if(userSettingAvailable == 1)
    try
        set(handles.fig,'Renderer',userSetting.renderer);
        
        if(strcmp(userSetting.renderer,'OpenGL'))
            set(handles.optionsMenu_renderer_openGL,'Checked','on');
            set(handles.optionsMenu_renderer_zBuffer,'Checked','off');
            set(handles.optionsMenu_renderer_painter,'Checked','off');
        elseif(strcmp(userSetting.renderer,'zbuffer'))
            set(handles.optionsMenu_renderer_openGL,'Checked','off');
            set(handles.optionsMenu_renderer_zBuffer,'Checked','on');
            set(handles.optionsMenu_renderer_painter,'Checked','off');
        elseif(strcmp(userSetting.renderer,'painters'))
            set(handles.optionsMenu_renderer_openGL,'Checked','off');
            set(handles.optionsMenu_renderer_zBuffer,'Checked','off');
            set(handles.optionsMenu_renderer_painter,'Checked','on');
        end
    catch
        
    end
end


binsString = {'2'; ...
    '4'; ...
    '8'; ...
    '16';...
    '32';...
    '64';...
    '128';...
    '256';...
    '512';...
    '1024';}; % add more or other values in this list if needed

for n = 1:size(binsString,1)
    handles.histogramContextMenu_numBins_set(n) = uimenu('Parent',handles.histogramContextMenu_numBins,'Label',binsString{n},'Tag',num2str(n),'Callback',@histogramListCallback);
end




if(userSettingAvailable == 1)
    % view mode for rendering from user settings
    try
        handles.view3Dmode = userSetting.view3Dmode;
    catch
        handles.view3Dmode = 'off';
    end
    if(strcmp(handles.view3Dmode,'on'))
        set(handles.optionsMenu_view3D_mode,'Checked','off'); % setting it off...
    else
        set(handles.optionsMenu_view3D_mode,'Checked','on');
    end
    optionsMenu_view3D_mode_Callback(handles.optionsMenu_view3D_mode, [], handles); %... to turn it on again --> inverted setting because functions changes back the values! tricky :(.
    % Cursors for 2D axes
    try
        handles.cursors2D = userSetting.cursors2D;
    catch
        handles.cursors2D = 'on';
    end
    if(strcmp(handles.cursors2D,'on'))
        set(handles.optionsMenu_cursor_2D,'Checked','off');
    else
        set(handles.optionsMenu_cursor_2D,'Checked','on');
    end
    optionsMenu_cursor_2D_Callback(handles.optionsMenu_cursor_2D, [], handles);
    
    % Cursors for 3D axes
    try
        handles.cursors3D = userSetting.cursors3D;
    catch
        handles.cursors3D = 'on';
    end
    if(strcmp(handles.cursors3D,'on'))
        set(handles.optionsMenu_cursor_3D,'Checked','off');
    else
        set(handles.optionsMenu_cursor_3D,'Checked','on');
    end
    optionsMenu_cursor_3D_Callback(handles.optionsMenu_cursor_3D, [], handles);
    
    % Reverse Z Setting
    try
        handles.reverseZ = userSetting.reverseZ;
    catch
        handles.reverseZ = 'off';
    end
    if(strcmp(handles.reverseZ,'on'))
        set(handles.optionsMenu_3D_reverseZ,'Checked','off');
    else
        set(handles.optionsMenu_3D_reverseZ,'Checked','on');
    end
    optionsMenu_3D_reverseZ_Callback(handles.optionsMenu_3D_reverseZ,[],handles);
    
    % window by mouse
    try
        handles.windowByMouse = userSetting.windowByMouse;
    catch
        handles.windowByMouse = 0;
    end
    if(handles.windowByMouse == 1)
        set(handles.optionsMenu_colormap_windowByMouse,'Checked','on');
    else
        set(handles.optionsMenu_colormap_windowByMouse,'Checked','off');
    end
    
    try
        handles.histogramBins = userSetting.histogramBins;
    catch
        handles.histogramBins = 32;
    end
    
    try
        handles.histogramFilterMax = userSetting.histogramFilterMax;
        if(handles.histogramFilterMax == 1)
            set(handles.histogramContextMenu_filterMax,'Checked','on');
        else
            set(handles.histogramContextMenu_filterMax,'Checked','off');
        end
    catch
        handles.histogramFilterMax = 0;
        set(handles.histogramContextMenu_filterMax,'Checked','off');
    end
    
else
    handles.view3Dmode = 'off';
    handles.cursors2D = 'on';
    handles.cursors3D = 'on';
    handles.reverseZ = 'off';
    handles.histogramBins = 32;
    handles.windowByMouse = 0;
    handles.histogramFilterMax = 0;
end

for n = 1:size(binsString,1)
    if(strcmp(get(handles.histogramContextMenu_numBins_set(n),'Label'),num2str(handles.histogramBins)))
        set(handles.histogramContextMenu_numBins_set(n),'Checked','on');
    end
end





if(isItRGB)
    set(handles.optionsMenu_3D,'Enable','off');
    set(handles.rotateButton,'Enable','off');
end

if(~isItRGB)
    handles = updateHistogram(handles);
    adaptVolumeWindowing(handles,1,0);
else
    
end


handles.scrollSteps = 1;
handles.updateSliceFunction = @updateSlice;

handles.adaptVolumeWindowingFunction = @adaptVolumeWindowing;
handles.adaptSliceWindowingFunction = @adaptSliceWindowing;

sliceButtonDownFcn(handles.xImage,[]);

% Update handles structure
guidata(hObject, handles);



%% Outputs from this function are returned to the command line. %%%%%%%%%%%
function varargout = miniViewerGUI_OutputFcn(~, ~, handles)

% Get default command line output from handles structure
varargout{1} = handles.output;



%% Callback function for the lines. executed when a line is moved %%%%%%%%%
function lineMoved(lineHandle)


handles = guidata(gcf);
tag = get(lineHandle,'Tag');
if(strcmp(tag,'xly'))
    yPos = get(lineHandle,'YData');
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        yPos =  round(((yPos - handles.imageStartpoint(2)) ./ handles.imageResolution(1)) + 1);
    end
    if(yPos(1) > 0 && yPos(1) <= size(handles.volume,2))
        updateSlice(handles, 2, round(yPos(1)),1);
    end
elseif(strcmp(tag,'xlz'))
    zPos = get(lineHandle,'XData');
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        zPos =  round(((zPos - handles.imageStartpoint(3)) ./ handles.imageResolution(3)) + 1);
    end
    if(zPos(1) > 0 && zPos(1) <= size(handles.volume,3))
        updateSlice(handles, 3, round(zPos(1)),1);
    end
elseif(strcmp(tag,'ylx'))
    xPos = get(lineHandle,'YData');
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        xPos =  round(((xPos - handles.imageStartpoint(1)) ./ handles.imageResolution(2)) + 1);
    end
    if(xPos(1) > 0 && xPos(1) <= size(handles.volume,1))
        updateSlice(handles, 1, round(xPos(1)),1);
    end
elseif(strcmp(tag,'ylz'))
    zPos = get(lineHandle,'XData');
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        zPos =  round(((zPos - handles.imageStartpoint(3)) ./ handles.imageResolution(3)) + 1);
    end
    if(zPos(1) > 0 && zPos(1) <= size(handles.volume,3))
        updateSlice(handles, 3, round(zPos(1)),1);
    end
elseif(strcmp(tag,'zlx'))
    xPos = get(lineHandle,'YData');
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        xPos =  round(((xPos - handles.imageStartpoint(1)) ./ handles.imageResolution(2)) + 1);
    end
    if(xPos(1) > 0 && xPos(1) <= size(handles.volume,1))
        updateSlice(handles, 1, round(xPos(1)),1);
    end
elseif(strcmp(tag,'zly'))
    yPos = get(lineHandle,'XData');
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        yPos =  round(((yPos - handles.imageStartpoint(2)) ./ handles.imageResolution(1)) + 1);
    end
    if(yPos(1) > 0 && yPos(1) <= size(handles.volume,2))
        updateSlice(handles, 2, round(yPos(1)),1);
    end
end
handles.windowButtonDown = 0;
guidata(handles.fig,handles);

%% Callback function for the edit box (x-Axes) %%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xSliceEditbox_Callback(hObject, ~, handles)

resetColorFrame(handles);
handles = guidata(gcf);
value = get(hObject,'String');
try
    valueNum = str2double(value);
catch
    disp('ERROR: Input must be a number!');
    return;
end

if(valueNum < 1)
    disp('ERROR: value must be > 0');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end
if(valueNum > size(handles.volume,1))
    disp('ERROR: value must be < volume size in x');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end

updateSlice(handles, 1, round(valueNum),1);




%% Create function for the edit box (x-Axes) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xSliceEditbox_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Callback function for the edit box (y-Axes) %%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ySliceEditbox_Callback(hObject, ~, handles)

resetColorFrame(handles);
handles = guidata(gcf);
value = get(hObject,'String');
try
    valueNum = str2double(value);
catch
    disp('ERROR: Input must be a number!');
    return;
end

if(valueNum < 1)
    disp('ERROR: value must be > 0');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end
if(valueNum > size(handles.volume,2))
    disp('ERROR: value must be < volume size in y');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end

updateSlice(handles, 2, round(valueNum),1);


%% Create function for the edit box (y-Axes) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ySliceEditbox_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Callback function for the edit box (z-Axes) %%%%%%%%%%%%%%%%%%%%%%%%%%%%
function zSliceEditbox_Callback(hObject, ~, handles)

resetColorFrame(handles);
handles = guidata(gcf);
value = get(hObject,'String');
try
    valueNum = str2double(value);
catch
    disp('ERROR: Input must be a number!');
    return;
end

if(valueNum < 1)
    disp('ERROR: value must be > 0');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end
if(valueNum > size(handles.volume,3))
    disp('ERROR: value must be < volume size in z');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end

updateSlice(handles, 3, round(valueNum),1);


%% Create function for the edit box (z-Axes) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function zSliceEditbox_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% Draws the Volume rendering in the according axes object. %%%%%%%%%%%%%%%
% Volume is masked according to the lower and upper threshold.
function drawVolumeView(handles, source)

lowerValue = get(handles.lowerThresholdSlider,'Value');
upperValue = get(handles.upperThresholdSlider,'Value');
if(lowerValue > upperValue)
    if(source == 1) % lower Slider
        set(handles.upperThresholdSlider,'Value',lowerValue);
    elseif(source == 2) % upper Slider
        set(handles.lowerThresholdSlider,'Value',upperValue);
    end
end

if(get(handles.invertLogicCheckbox,'Value') == get(handles.invertLogicCheckbox,'Min')) % unchecked --> normal logic
    maske = (handles.volume >= lowerValue & handles.volume <= upperValue);
else
    maske = ((handles.volume <= lowerValue) + (handles.volume >= upperValue));
end

if(handles.volumeViewEnables == 1)
    clear handles.vView;
    if(strcmp(handles.view3Dmode,'on'))
        handles.vView = vol3d('CData',single(handles.volume) .* single(maske),'texture','3D','Parent',handles.volumeViewAxes);
    else
        handles.vView = vol3d('CData',single(handles.volume) .* single(maske),'texture','2D','Parent',handles.volumeViewAxes);
    end
    clear maske;
    handles.vView = vol3d(handles.vView,'Parent',handles.volumeViewAxes);
    set(handles.vView.handles,'UIContextMenu',handles.sliceImageContextMenu);
elseif(handles.volumeViewEnables == 2)
    yPos = get(handles.xSliceSlider,'Value');
    xPos = get(handles.ySliceSlider,'Value');
    zPos = get(handles.zSliceSlider,'Value');
    
    h1 = slice(double(handles.volume) .* double(maske),xPos,yPos,zPos,'Parent',handles.volumeViewAxes);
    
    set(h1(1:3),'EdgeAlpha',0); % deleting the black edges
    set(h1(1:3),'UIContextMenu',handles.sliceImageContextMenu);
    handles.volumeViewAxes
    
    handles.slice3DhandlesX = h1(1);
    handles.slice3DhandlesY = h1(2);
    handles.slice3DhandlesZ = h1(3);
    
    % setting alpha maps for slices. 0 = translucent, opacity slider is
    % applied!
    set(handles.slice3DhandlesX,'AlphaData',double(get(handles.slice3DhandlesX,'CData') > 0).* get(handles.alphamapSlider,'Value'));
    set(handles.slice3DhandlesX,'AlphaDataMapping','none');
    
    set(handles.slice3DhandlesY,'AlphaData',double(get(handles.slice3DhandlesY,'CData') > 0).* get(handles.alphamapSlider,'Value'));
    set(handles.slice3DhandlesY,'AlphaDataMapping','none');
    
    set(handles.slice3DhandlesZ,'AlphaData',double(get(handles.slice3DhandlesZ,'CData') > 0).* get(handles.alphamapSlider,'Value'));
    set(handles.slice3DhandlesZ,'AlphaDataMapping','none');
    
    
    %set(h1(1:3),'FaceAlpha','flat');   % commented. makes values == 0
    %translucent!
elseif(handles.volumeViewEnables == 3)
    rotMat = get3DRotationMatrix(-get(handles.azimuthSlider,'Value')*(pi/180), 1); 
    rotMat2 = get3DRotationMatrix(-get(handles.elevationSlider,'Value')*(pi/180), 2);
    rotMat = rotMat2 * rotMat;
    
    rotMat(:,4) = 0;
    rotMat(4,:) = 0;
    rotMat(4,4) = 1;
    
    options.RenderType = 'mip';
    options.Mview=rotMat; %makeViewMatrix([10 10 0],[1 1 1],[0 0 0]);
    options.ImageSize = [max(size(handles.rotatedVolume)) max(size(handles.rotatedVolume))];
    
    % Render and show image
    I = render(handles.rotatedVolume,options);
    
    % transform to original max and min values
    I = I./max(I(:));
    I = I .* (max(handles.volume(:))-min(handles.volume(:)))+min(handles.volume(:));
    imagesc(I,'Parent',handles.volumeViewAxes);
    adaptVolumeWindowing(handles,1,1);
end

handles.currentViewPosition = [get(handles.azimuthSlider,'Value'),get(handles.elevationSlider,'Value')];

if(~(handles.volumeViewEnables == 3))
    view(handles.volumeViewAxes,get(handles.azimuthSlider,'Value'),get(handles.elevationSlider,'Value'));
    
    if(strcmp(handles.reverseZ,'on'))
        set(handles.volumeViewAxes,'ZDir','reverse');
    elseif(strcmp(handles.reverseZ,'off'))
        set(handles.volumeViewAxes,'ZDir','normal');
    end
    set(handles.volumeViewAxes,'OuterPosition',handles.volumeViewPos);
end

set(handles.volumeViewAxes,'Visible','off'); % eq. to 'axis off'
set(handles.volumeViewAxes,'DataAspectRatio',[1 1 1],'DataAspectRatioMode','manual');

guidata(handles.fig,handles);








%% Draws rectangles into the volume rendering according to the line %%%%%%%
%  positions in the slice images.
function handles = drawRectangle(handles)

yPos = str2double(get(handles.xSliceEditbox,'String'));
xPos = str2double(get(handles.ySliceEditbox,'String'));
zPos = str2double(get(handles.zSliceEditbox,'String'));

if(isfield(handles,'yBox'))
    if(isfield(handles.yBox,'line1'))
        delete(handles.yBox.line1);
    end
    if(isfield(handles.yBox,'line2'))
        delete(handles.yBox.line2);
    end
    if(isfield(handles.yBox,'line3'))
        delete(handles.yBox.line3);
    end
    if(isfield(handles.yBox,'line4'))
        delete(handles.yBox.line4);
    end
end
handles.yBox.line1 = line([xPos xPos],[0 0],[0 size(handles.volume,3)],'Parent',handles.volumeViewAxes);
handles.yBox.line2 = line([xPos xPos],[size(handles.volume,1) size(handles.volume,1)],[0 size(handles.volume,3)],'Parent',handles.volumeViewAxes);
handles.yBox.line3 = line([xPos xPos],[0 size(handles.volume,1)],[0 0],'Parent',handles.volumeViewAxes);
handles.yBox.line4 = line([xPos xPos],[0 size(handles.volume,1)],[size(handles.volume,3) size(handles.volume,3)],'Parent',handles.volumeViewAxes);

if(isfield(handles,'xBox'))
    if(isfield(handles.xBox,'line1'))
        delete(handles.xBox.line1);
    end
    if(isfield(handles.xBox,'line2'))
        delete(handles.xBox.line2);
    end
    if(isfield(handles.xBox,'line3'))
        delete(handles.xBox.line3);
    end
    if(isfield(handles.xBox,'line4'))
        delete(handles.xBox.line4);
    end
end
handles.xBox.line1 = line([0 0],[yPos yPos],[0 size(handles.volume,3)],'Parent',handles.volumeViewAxes);
handles.xBox.line2 = line([size(handles.volume,2) size(handles.volume,2)],[yPos yPos],[0 size(handles.volume,3)],'Parent',handles.volumeViewAxes);
handles.xBox.line3 = line([0 size(handles.volume,2)],[yPos yPos],[0 0],'Parent',handles.volumeViewAxes);
handles.xBox.line4 = line([0 size(handles.volume,2)],[yPos yPos],[size(handles.volume,3) size(handles.volume,3)],'Parent',handles.volumeViewAxes);

if(isfield(handles,'zBox'))
    if(isfield(handles.zBox,'line1'))
        delete(handles.zBox.line1);
    end
    if(isfield(handles.zBox,'line2'))
        delete(handles.zBox.line2);
    end
    if(isfield(handles.zBox,'line3'))
        delete(handles.zBox.line3);
    end
    if(isfield(handles.zBox,'line4'))
        delete(handles.zBox.line4);
    end
end
handles.zBox.line1 = line([0 0],[0 size(handles.volume,1)],[zPos zPos],'Parent',handles.volumeViewAxes);
handles.zBox.line2 = line([size(handles.volume,2) size(handles.volume,2)],[0 size(handles.volume,1)],[zPos zPos],'Parent',handles.volumeViewAxes);
handles.zBox.line3 = line([0 size(handles.volume,2)],[0 0],[zPos zPos],'Parent',handles.volumeViewAxes);
handles.zBox.line4 = line([0 size(handles.volume,2)],[size(handles.volume,1) size(handles.volume,1)],[zPos zPos],'Parent',handles.volumeViewAxes);

set(handles.zBox.line1,'Color',[0 0 1]);
set(handles.zBox.line2,'Color',[0 0 1]);
set(handles.zBox.line3,'Color',[0 0 1]);
set(handles.zBox.line4,'Color',[0 0 1]);

set(handles.yBox.line1,'Color',[0 1 0]);
set(handles.yBox.line2,'Color',[0 1 0]);
set(handles.yBox.line3,'Color',[0 1 0]);
set(handles.yBox.line4,'Color',[0 1 0]);

set(handles.xBox.line1,'Color',[1 0 0]);
set(handles.xBox.line2,'Color',[1 0 0]);
set(handles.xBox.line3,'Color',[1 0 0]);
set(handles.xBox.line4,'Color',[1 0 0]);

if(strcmp(get(handles.optionsMenu_cursor_3D,'Checked'),'off'))
    set(handles.zBox.line1,'Visible','off');
    set(handles.zBox.line2,'Visible','off');
    set(handles.zBox.line3,'Visible','off');
    set(handles.zBox.line4,'Visible','off');
    
    set(handles.yBox.line1,'Visible','off');
    set(handles.yBox.line2,'Visible','off');
    set(handles.yBox.line3,'Visible','off');
    set(handles.yBox.line4,'Visible','off');
    
    set(handles.xBox.line1,'Visible','off');
    set(handles.xBox.line2,'Visible','off');
    set(handles.xBox.line3,'Visible','off');
    set(handles.xBox.line4,'Visible','off');
end



%% Updates the rectangle position according to the given positions %%%%%%%%
%  by the lines in the slice images. x/y/z-Pos is read out from sliders
function updateRectangle(handles)

yPos = get(handles.xSliceSlider,'Value'); %str2num(get(handles.xSliceEditbox,'String'));
xPos = get(handles.ySliceSlider,'Value'); %str2num(get(handles.ySliceEditbox,'String'));
zPos = get(handles.zSliceSlider,'Value'); %str2num(get(handles.zSliceEditbox,'String'));

try
    set(handles.yBox.line1,'XData',[xPos xPos]);
    set(handles.yBox.line2,'XData',[xPos xPos]);
    set(handles.yBox.line3,'XData',[xPos xPos]);
    set(handles.yBox.line4,'XData',[xPos xPos]);
catch % not painted
end

try
    set(handles.xBox.line1,'YData',[yPos yPos]);
    set(handles.xBox.line2,'YData',[yPos yPos]);
    set(handles.xBox.line3,'YData',[yPos yPos]);
    set(handles.xBox.line4,'YData',[yPos yPos]);
catch % not painted
end

try
    set(handles.zBox.line1,'ZData',[zPos zPos]);
    set(handles.zBox.line2,'ZData',[zPos zPos]);
    set(handles.zBox.line3,'ZData',[zPos zPos]);
    set(handles.zBox.line4,'ZData',[zPos zPos]);
catch % not painted
end


%% Callback function for the azimuth slider. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sets the view properties of the volume rendering axes
function azimuthSlider_Callback(~, ~, handles)

resetColorFrame(handles);
if(handles.volumeViewEnables ~= 3)
    set(handles.volumeViewAxes,'View', [get(handles.azimuthSlider,'Value') get(handles.elevationSlider,'Value')]);
else
    drawVolumeView(handles, 0);
end
set(handles.azimuthEditbox,'String',num2str(round(get(handles.azimuthSlider,'Value'))));



%% Create function for the azimuth slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function azimuthSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Callback function for the elevation slider. %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sets the view properties of the volume rendering axes
function elevationSlider_Callback(~, ~, handles)

resetColorFrame(handles);
set(handles.elevationEditbox,'String',num2str(round(get(handles.elevationSlider,'Value'))));
if(handles.volumeViewEnables ~= 3)
    set(handles.volumeViewAxes,'View', [get(handles.azimuthSlider,'Value') get(handles.elevationSlider,'Value')]);
else
    drawVolumeView(handles, 0);
end



%% Create function for the elevation slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function elevationSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Callback function for the azimuth editbox %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sets the view properties of the volume rendering axes
function azimuthEditbox_Callback(hObject, ~, handles)

resetColorFrame(handles);
value = get(hObject,'String');
try
    valueNum = str2double(value);
catch
    disp('ERROR: Input must be a number!');
    return;
end

if(valueNum < -360)
    disp('ERROR: value must be > -360�');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end
if(valueNum > 360)
    disp('ERROR: value must be < 360�');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end


set(handles.azimuthSlider,'Value',valueNum);
if(handles.volumeViewEnables ~= 3)
    set(handles.volumeViewAxes,'View', [get(handles.azimuthSlider,'Value') get(handles.elevationSlider,'Value')]);
else
    drawVolumeView(handles, 0);
end



%% Create function for the azimuith editbox %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function azimuthEditbox_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Callback function for the elevation editbox %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sets the view properties of the volume rendering axes
function elevationEditbox_Callback(hObject, ~, handles)

resetColorFrame(handles);
value = get(hObject,'String');
try
    valueNum = str2double(value);
catch
    disp('ERROR: Input must be a number!');
    return;
end

if(valueNum < -90)
    disp('ERROR: value must be > -90�');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end
if(valueNum > 90)
    disp('ERROR: value must be < 90�');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end

set(handles.elevationSlider,'Value',valueNum);
if(handles.volumeViewEnables ~= 3)
    set(handles.volumeViewAxes,'View', [get(handles.azimuthSlider,'Value') get(handles.elevationSlider,'Value')]);
else
    drawVolumeView(handles, 0);
end



%% Create function for the elevation editbox %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function elevationEditbox_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Callback function for the alphamap slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sets the alpha channel of the volume rendering axes
function alphamapSlider_Callback(~, ~, handles)

resetColorFrame(handles);
aMap = 0:(1/64):1;
set(handles.fig,'AlphaMap',aMap.*get(handles.alphamapSlider,'Value'));
set(handles.alphamapEditbox,'String',num2str(get(handles.alphamapSlider,'Value')));
if(handles.volumeViewEnables == 2) % slice 3D view
    updateSlice3Dview(handles);
end


%% Create function for the alphamap slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function alphamapSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Callback function for the alphamap editbox %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sets the alpha channel of the volume rendering axes
function alphamapEditbox_Callback(hObject, ~, handles)

resetColorFrame(handles);
value = get(hObject,'String');
try
    valueNum = str2double(value);
catch
    disp('ERROR: Input must be a number!');
    return;
end

if(valueNum < 0)
    disp('ERROR: value must be > 0');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end
if(valueNum > 1)
    disp('ERROR: value must be < 1');
    set(hObject,'BackgroundColor',[1 0 0]);
    return;
else
    set(hObject,'BackgroundColor',[1 1 1]);
end
set(handles.alphamapSlider,'Value',valueNum);
aMap = 0:(1/64):1;
set(handles.fig,'AlphaMap',aMap.*get(handles.alphamapSlider,'Value'));
if(handles.volumeViewEnables == 2) % slice 3D view
    updateSlice3Dview(handles);
end


%% Create function for the alphamap editbox %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function alphamapEditbox_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Callback function for the lower threshold slider %%%%%%%%%%%%%%%%%%%%%%%
% sets the lower threshold for image segmentation and updates the volume
% rendering
function lowerThresholdSlider_Callback(~, ~, handles)

resetColorFrame(handles);
drawVolumeView(handles, 1);
handles = drawRectangle(handles);
guidata(handles.fig,handles);



%% Create function for the lower threshold slider %%%%%%%%%%%%%%%%%%%%%%%%%
function lowerThresholdSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Callback function for the upper threshold slider %%%%%%%%%%%%%%%%%%%%%%%
% sets the upper threshold for image segmentation and updates the volume
% rendering
function upperThresholdSlider_Callback(~, ~, handles)

resetColorFrame(handles);
drawVolumeView(handles, 2);
handles = drawRectangle(handles);
guidata(handles.fig,handles);



%% Create function for the upper threshold slider %%%%%%%%%%%%%%%%%%%%%%%%%
function upperThresholdSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



%% Callback function for the menu Options %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nothing to do here
function optionsMenu_Callback(~, ~, ~)


%% Callback function for the menu Options -> Colormap %%%%%%%%%%%%%%%%%%%%%
% nothing to do here
function optionsMenu_colormap_Callback(~, ~, ~)



%% Callback function for the menu Options -> Colormap -> Keep fixed %%%%%%%
%  sets the CLims for all axes objects and updates windowing on slice or
%  volume basis
function optionsMenu_colormap_keepFixed_Callback(hObject, ~, handles)

if(strcmp(get(hObject,'Checked'),'off'))
    set(hObject,'Checked','on');
    set(handles.axesX,'CLimMode','manual');
    set(handles.axesX,'CLim',[handles.minValue handles.maxValue]);
    set(handles.axesY,'CLimMode','manual');
    set(handles.axesY,'CLim',[handles.minValue handles.maxValue]);
    set(handles.axesZ,'CLimMode','manual');
    set(handles.axesZ,'CLim',[handles.minValue handles.maxValue]);
    set(handles.volumeViewAxes,'CLimMode','manual');
    set(handles.volumeViewAxes,'CLim',[handles.minValue handles.maxValue]);
    adaptVolumeWindowing(handles,1,1);
elseif(strcmp(get(hObject,'Checked'),'on'))
    set(hObject,'Checked','off');
    set(handles.axesX,'CLimMode','auto');
    set(handles.axesY,'CLimMode','auto');
    set(handles.axesZ,'CLimMode','auto');
    set(handles.volumeViewAxes,'CLimMode','auto');
    adaptSliceWindowing(handles,1,1);
end
handles.cmapFixed = get(hObject,'Checked');
guidata(handles.fig,handles);


%% Callback function for the menu Options -> Colormap -> Colormap setting %
% nothing to do here
function optionsMenu_colormap_colormapSetting_Callback(~, ~, ~)


%% Callback function for the entries of the menu Options -> colormap -> Colorap setting %
%  updates the colormap of the figure
function colormapListCallback(hObject, ~)

handles = guidata(gcf);
colormap(handles.fig,get(hObject,'Label'));
handles.colormap = get(hObject,'Label');

%reset checked items
set(handles.optionsMenu_colormap_set,'Checked','off');
% set new checked item
set(hObject,'Checked','on');
guidata(handles.fig,handles);


%% Callback function for the x slice slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  updates the slice image
function xSliceSlider_Callback(hObject, ~, handles)

resetColorFrame(handles);
valueNum = get(hObject,'Value');
updateSlice(handles, 1, round(valueNum), 1);




%% Create function for the x slice slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xSliceSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Callback function for the y slice slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  updates the slice image
function ySliceSlider_Callback(hObject, ~, handles)

resetColorFrame(handles);
valueNum = get(hObject,'Value');
updateSlice(handles, 2, round(valueNum),1);


%% Create function for the y slice slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ySliceSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Callback function for the z slice slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  updates the slice image
function zSliceSlider_Callback(hObject, ~, handles)

resetColorFrame(handles);
valueNum = get(hObject,'Value');
updateSlice(handles, 3, round(valueNum),1);



%% Create function for the z slice slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function zSliceSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



%% Callback function of menu Options => 2D View %%%%%%%%%%%%%%%%%%%%%%%%%%%
% nothing to do here.
function optionsMenu_view2D_Callback(~, ~, ~)



%% Callback function of menu Options => 2D View => Show Cursor %%%%%%%%%%%%
%  toggles visibility of the cursors in all slice axes
function optionsMenu_cursor_2D_Callback(hObject, ~, handles)

if(strcmp(get(hObject,'Checked'),'off'))
    handles.cursors2D = 'on';
else
    handles.cursors2D = 'off';
end

if(strcmp(get(hObject,'Checked'),'off'))
    set(hObject,'Checked','on');
    set(handles.xly,'Visible','on');
    set(handles.xlz,'Visible','on');
    set(handles.ylx,'Visible','on');
    set(handles.ylz,'Visible','on');
    set(handles.zlx,'Visible','on');
    set(handles.zly,'Visible','on');
else
    set(hObject,'Checked','off');
    set(handles.xly,'Visible','off');
    set(handles.xlz,'Visible','off');
    set(handles.ylx,'Visible','off');
    set(handles.ylz,'Visible','off');
    set(handles.zlx,'Visible','off');
    set(handles.zly,'Visible','off');
end
guidata(handles.fig,handles);

%% Callback function of menu Options => 3D View => Show Cursor %%%%%%%%%%%%
%  toggles visibility of the cursors (rectangles) in the volume rendering
%  axes
function optionsMenu_cursor_3D_Callback(hObject, ~, handles)

if(strcmp(get(hObject,'Checked'),'off'))
    set(hObject,'Checked','on');
    handles.cursors3D = 'on';
    try
        set(handles.zBox.line1,'Visible','on');
        set(handles.zBox.line2,'Visible','on');
        set(handles.zBox.line3,'Visible','on');
        set(handles.zBox.line4,'Visible','on');
        
        set(handles.yBox.line1,'Visible','on');
        set(handles.yBox.line2,'Visible','on');
        set(handles.yBox.line3,'Visible','on');
        set(handles.yBox.line4,'Visible','on');
        
        set(handles.xBox.line1,'Visible','on');
        set(handles.xBox.line2,'Visible','on');
        set(handles.xBox.line3,'Visible','on');
        set(handles.xBox.line4,'Visible','on');
    catch
        
    end
else
    set(hObject,'Checked','off');
    handles.cursors3D = 'off';
    try
        set(handles.zBox.line1,'Visible','off');
        set(handles.zBox.line2,'Visible','off');
        set(handles.zBox.line3,'Visible','off');
        set(handles.zBox.line4,'Visible','off');
        
        set(handles.yBox.line1,'Visible','off');
        set(handles.yBox.line2,'Visible','off');
        set(handles.yBox.line3,'Visible','off');
        set(handles.yBox.line4,'Visible','off');
        
        set(handles.xBox.line1,'Visible','off');
        set(handles.xBox.line2,'Visible','off');
        set(handles.xBox.line3,'Visible','off');
        set(handles.xBox.line4,'Visible','off');
    catch
        
    end
end
guidata(handles.fig,handles);


%% Callback function of the window center slider %%%%%%%%%%%%%%%%%%%%%%%%%%
%  sets new value and updates the windowing slice- or volume based.
function windowCenterSlider_Callback(~, ~, handles)
resetColorFrame(handles);
set(handles.windowCenterEditbox,'String',num2str(get(handles.windowCenterSlider,'Value')));
if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'on')) % max Values of all
    adaptVolumeWindowing(handles,1,1);
else
    adaptSliceWindowing(handles,1,1);
end





%% Create function of the window center slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%
function windowCenterSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Callback function of the window width slider %%%%%%%%%%%%%%%%%%%%%%%%%%%
%  sets new value and updates the windowing slice- or volume based.
function windowWidthSlider_Callback(~, ~, handles)
resetColorFrame(handles);

set(handles.windowWidthEditbox,'String',num2str(get(handles.windowWidthSlider,'Value')));
if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'on')) % max Values of all
    adaptVolumeWindowing(handles,1,1);
else
    adaptSliceWindowing(handles,1,1);
end



%% Create function of the window width slider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function windowWidthSlider_CreateFcn(hObject, ~, ~)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



%% updates the windowing olume-based %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  window center/width is calculated based on min/max of the entire volume.
function adaptVolumeWindowing(handles, updateHist, updateLinked)

centerValue = get(handles.windowCenterSlider,'Value');
widthValue = get(handles.windowWidthSlider,'Value');

windowCenter = centerValue; % handles.minValue + (centerValue * (handles.maxValue-handles.minValue));
windowWidth =  widthValue; % (handles.maxValue-handles.minValue) * widthValue;
lowerBound = windowCenter - (windowWidth/2);
upperBound = windowCenter + (windowWidth/2);

if(upperBound <= lowerBound)
    upperBound = lowerBound+handles.addOnEps;
end

try
    set(handles.axesX,'CLim',[lowerBound upperBound]);
    set(handles.axesY,'CLim',[lowerBound upperBound]);
    set(handles.axesZ,'CLim',[lowerBound upperBound]);
    set(handles.volumeViewAxes,'CLim',[lowerBound upperBound]);
catch
    
end

if(updateHist == 1)
    updateHistogram(handles);
end


if(~isempty(handles.linkedFig) && updateLinked && handles.linkWindowing)
    try
        tempHandles = guidata(handles.linkedFig);
        linkedWindowCenterValue = centerValue;
        linkedWindowWidthValue = widthValue;
        if(centerValue > get(tempHandles.windowCenterSlider,'Max'))
            linkedWindowCenterValue = get(tempHandles.windowCenterSlider,'Max');
            disp('[MiniViewer] Warning - window center is higher than allowed window center for linked miniViewer. Set to bounds');
        end
        if(centerValue < get(tempHandles.windowCenterSlider,'Min'))
            linkedWindowCenterValue = get(tempHandles.windowCenterSlider,'Min');
            disp('[MiniViewer] Warning - window center is lower than allowed window center for linked miniViewer. Set to bounds');
        end
        set(tempHandles.windowCenterSlider,'Value',linkedWindowCenterValue);
        set(tempHandles.windowCenterEditbox,'String',num2str(linkedWindowCenterValue));
        
        if(widthValue > get(tempHandles.windowWidthSlider,'Max'))
            linkedWindowWidthValue = get(tempHandles.windowWidthSlider,'Max');
            disp('[MiniViewer] Warning - window width is higher than allowed window width for linked miniViewer. Set to bounds');
        end
        if(widthValue < get(tempHandles.windowWidthSlider,'Min'))
            linkedWindowWidthValue = get(tempHandles.windowWidthSlider,'Min');
            disp('[MiniViewer] Warning - window width is lower than allowed window width for linked miniViewer. Set to bounds');
        end
        set(tempHandles.windowWidthSlider,'Value',linkedWindowWidthValue);
        set(tempHandles.windowWidthEditbox,'String',num2str(linkedWindowWidthValue));
        
        tempHandles.adaptVolumeWindowingFunction(tempHandles, updateHist, 0);
    catch
        disp('[MiniViewer] Warning - linked miniViewer was closed');
    end
end



%% updates the windowing slice-based %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  for each slice the window center/width is calculated based on min/max of
%  the particular slice
function adaptSliceWindowing(handles, updateHist, updateLinked)

% recaclculating relative values
centerValueInitial = get(handles.windowCenterSlider,'Value');
widthValueInitial = get(handles.windowWidthSlider,'Value');

centerValue = (get(handles.windowCenterSlider,'Value')-get(handles.windowCenterSlider,'Min')) / (get(handles.windowCenterSlider,'Max') - get(handles.windowCenterSlider,'Min'));
widthValue = (get(handles.windowWidthSlider,'Value')-get(handles.windowWidthSlider,'Min')) / (get(handles.windowWidthSlider,'Max') - get(handles.windowWidthSlider,'Min'));

slice = handles.volume(get(handles.xSliceSlider,'Value'),:,:);
windowCenter = min(slice(:)) + (centerValue * (max(slice(:))-min(slice(:))));
windowWidth =  (max(slice(:))-min(slice(:))) * widthValue;
lowerBound = windowCenter - (windowWidth/2);
upperBound = windowCenter + (windowWidth/2);
if(lowerBound == upperBound)
    upperBound = lowerBound+eps;
end
set(handles.axesX,'CLim',[lowerBound upperBound]);


slice = handles.volume(:,get(handles.ySliceSlider,'Value'),:);
windowCenter = min(slice(:)) + (centerValue * (max(slice(:))-min(slice(:))));
windowWidth =  (max(slice(:))-min(slice(:))) * widthValue;
lowerBound = windowCenter - (windowWidth/2);
upperBound = windowCenter + (windowWidth/2);
if(lowerBound == upperBound)
    upperBound = lowerBound+eps;
end
set(handles.axesY,'CLim',[lowerBound upperBound]);

slice = handles.volume(:,:,get(handles.zSliceSlider,'Value'));
windowCenter = min(slice(:)) + (centerValue * (max(slice(:))-min(slice(:))));
windowWidth =  (max(slice(:))-min(slice(:))) * widthValue;
lowerBound = windowCenter - (windowWidth/2);
upperBound = windowCenter + (windowWidth/2);
if(lowerBound == upperBound)
    upperBound = lowerBound+eps;
end
set(handles.axesZ,'CLim',[lowerBound upperBound]);


windowCenter = handles.minValue + (centerValue * (handles.maxValue-handles.minValue));
windowWidth =  (handles.maxValue-handles.minValue) * widthValue;
lowerBound = windowCenter - (windowWidth/2);
upperBound = windowCenter + (windowWidth/2);
if(lowerBound == upperBound)
    upperBound = lowerBound+eps;
end
set(handles.volumeViewAxes,'CLim',[lowerBound upperBound]);

if(updateHist == 1)
    updateHistogram(handles);
end

if(~isempty(handles.linkedFig) && updateLinked && handles.linkWindowing)
    try
        tempHandles = guidata(handles.linkedFig);
        linkedWindowCenterValue = centerValueInitial;
        linkedWindowWidthValue = widthValueInitial;
        if(centerValueInitial > get(tempHandles.windowCenterSlider,'Max'))
            linkedWindowCenterValue = get(tempHandles.windowCenterSlider,'Max');
            disp('[MiniViewer] Warning - window center is higher than allowed window center for linked miniViewer. Set to bounds');
        end
        if(centerValueInitial < get(tempHandles.windowCenterSlider,'Min'))
            linkedWindowCenterValue = get(tempHandles.windowCenterSlider,'Min');
            disp('[MiniViewer] Warning - window center is lower than allowed window center for linked miniViewer. Set to bounds');
        end
        set(tempHandles.windowCenterSlider,'Value',linkedWindowCenterValue);
        set(tempHandles.windowCenterEditbox,'String',num2str(linkedWindowCenterValue));
        
        if(widthValueInitial > get(tempHandles.windowWidthSlider,'Max'))
            linkedWindowWidthValue = get(tempHandles.windowWidthSlider,'Max');
            disp('[MiniViewer] Warning - window width is higher than allowed window width for linked miniViewer. Set to bounds');
        end
        if(widthValueInitial < get(tempHandles.windowWidthSlider,'Min'))
            linkedWindowWidthValue = get(tempHandles.windowWidthSlider,'Min');
            disp('[MiniViewer] Warning - window width is lower than allowed window width for linked miniViewer. Set to bounds');
        end
        set(tempHandles.windowWidthSlider,'Value',linkedWindowWidthValue);
        set(tempHandles.windowWidthEditbox,'String',num2str(linkedWindowWidthValue));
        
        tempHandles.adaptSliceWindowingFunction(tempHandles, updateHist, 0);
    catch
        disp('[MiniViewer] Warning - linked miniViewer was closed');
    end
end



%% Updates the slice in a particular axes %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% by ax the axis is determined. 1 = x, 2 = y, 3 = z.
% sets all control units to the appropriate slice number
function updateSlice(handles, ax, sliceNumber, updateLinked)

sliceNumberPixel = sliceNumber;

if(ax == 1)
    % setting edit box
    set(handles.xSliceEditbox,'String',num2str(round(sliceNumber)));
    if(size(size(handles.volume),2) == 4)
        isItRGB = 1;% RGB
    else
        isItRGB = 0;
    end
    
    % setting the slider
    set(handles.xSliceSlider,'Value',round(sliceNumber));
    
    % setting the image
    if(isItRGB == 1)
        set(handles.xImage,'CData',squeeze(handles.volume(round(sliceNumber),:,:,:)));
    else
        set(handles.xImage,'CData',squeeze(handles.volume(round(sliceNumber),:,:)));
    end
    
    % setting the lines
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        sliceNumber =  handles.imageStartpoint(1) + ((sliceNumber - 1) .* handles.imageResolution(1));
    end
    set(handles.zlx,'YData',[sliceNumber sliceNumber]);
    set(handles.ylx,'YData',[sliceNumber sliceNumber]);
    
    
    
elseif(ax == 2)
    % setting edit box
    set(handles.ySliceEditbox,'String',num2str(round(sliceNumber)));
    
    % setting the slider
    set(handles.ySliceSlider,'Value',round(sliceNumber));
    
    if(size(size(handles.volume),2) == 4)
        isItRGB = 1;% RGB
    else
        isItRGB = 0;
    end
    
    % setting the image
    if(isItRGB == 1)
        set(handles.yImage,'CData',squeeze(handles.volume(:,round(sliceNumber),:,:)));
    else
        set(handles.yImage,'CData',squeeze(handles.volume(:,round(sliceNumber),:)));
    end
    
    % setting the lines
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        sliceNumber =  handles.imageStartpoint(2) + ((sliceNumber - 1) .* handles.imageResolution(2));
    end
    set(handles.xly,'YData',[sliceNumber sliceNumber]);
    set(handles.zly,'XData',[sliceNumber sliceNumber]);
    
elseif(ax == 3)
    % setting edit box
    set(handles.zSliceEditbox,'String',num2str(round(sliceNumber)));
    
    % setting the slider
    set(handles.zSliceSlider,'Value',round(sliceNumber));
    
    if(size(size(handles.volume),2) == 4)
        isItRGB = 1;% RGB
    else
        isItRGB = 0;
    end
    
    % setting image in according axes object
    if(isItRGB == 1)
        set(handles.zImage,'CData',squeeze(handles.volume(:,:,round(sliceNumber),:)));
    else
        set(handles.zImage,'CData',squeeze(handles.volume(:,:,round(sliceNumber))));
    end
    
    % setting lines
    if(handles.startpointAvailable || handles.imageResolutionAvailable)
        sliceNumber =  handles.imageStartpoint(3) + ((sliceNumber - 1) .* handles.imageResolution(3));
    end
    set(handles.xlz,'XData',[sliceNumber sliceNumber]);
    set(handles.ylz,'XData',[sliceNumber sliceNumber]);
end
% updating lines in 3D mode
if(handles.volumeViewEnables >= 1)
    updateRectangle(handles);
end

if(~isempty(handles.linkedFig) && updateLinked && handles.linkSlicing)
    try
        tempHandles = guidata(handles.linkedFig);
        tempHandles.updateSliceFunction(tempHandles, ax, sliceNumberPixel, 0);
    catch
        disp('[MiniViewer] Warning - linked miniViewer was closed');
    end
end

guidata(handles.fig,handles);

% update the windowing if slice windowing is active
if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'off') && ~isItRGB)
    adaptSliceWindowing(handles,1,1);
end

if(handles.volumeViewEnables == 2) % slice 3D view
    updateSlice3Dview(handles);
end



%% Window Scroll function. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  performs running through slices. the last clicked image is set the focus
%  on. Scrolling is enabled for this image.
function miniViewer_WindowScrollWheelFcn(~, eventdata, handles)

type = get(gco,'type');
if (strcmp(type, 'image'))
    ax = get(gco,'Parent');
    
    if(ax == handles.axesX)
        currentSlice = get(handles.xSliceSlider,'Value');
        axID = 1;
        axMaxSize = get(handles.xSliceSlider,'Max');
    elseif(ax == handles.axesY)
        currentSlice = get(handles.ySliceSlider,'Value');
        axID = 2;
        axMaxSize = get(handles.ySliceSlider,'Max');
    elseif(ax == handles.axesZ)
        currentSlice = get(handles.zSliceSlider,'Value');
        axID = 3;
        axMaxSize = get(handles.zSliceSlider,'Max');
    else
        return; % e.g 3D axes.
    end
    
    scrollValue = eventdata.VerticalScrollCount;
    
    
    if(scrollValue > 0)
        if((currentSlice+handles.scrollSteps) <= axMaxSize)
            updateSlice(handles,axID,round(currentSlice+handles.scrollSteps),1);
        end
    elseif(scrollValue < 0)
        if((currentSlice-handles.scrollSteps) >= 1)
            updateSlice(handles,axID,round(currentSlice-handles.scrollSteps),1);
        end
    end
end


%% Callback function for the menu Help %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nothing to do here.
function helpMenu_Callback(~, ~, ~)


%% Callback function for the menu Help -> About %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Opens a "about" dialog.
function helpMenu_about_Callback(~, ~, ~)

waitfor(about);



%% Callback function for the menu -> option -> 3D View %%%%%%%%%%%%%%%%%%%%
% nothing to do here.
function optionsMenu_view3D_Callback(~, ~, ~)



%% Callback function for the menu option -> exit %%%%%%%%%%%%%%%%%%%%%%%%%%
%  closes the application
function optionsMenu_exit_Callback(~, ~, handles)

close(handles.fig);



%% Enables/Disables the zoom mode %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function zoomButton_Callback(hObject, ~, handles)

updateMutualExclusiveButtons(handles,hObject);
if(get(hObject,'Value') == 1)
    % reset others
    zoom on;
elseif(get(hObject,'Value') == 0)
    zoom off;
end


%% Enables/Disables the shift mode %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function shiftButton_Callback(hObject, ~, handles)

updateMutualExclusiveButtons(handles,hObject)
if(get(hObject,'Value') == 1)
    % reset others
    pan on;
elseif(get(hObject,'Value') == 0)
    pan off;
end


%% Callback for option menu -> 3D View -> Enable 3D View %%%%%%%%%%%%%%%%%%
% enables or disables the 3D volume rendering
function optionsMenu_view3D_mode_Callback(hObject, ~, handles)

if(strcmp(get(hObject,'Checked'),'off'))
    set(hObject,'Checked','on');
else
    set(hObject,'Checked','off');
end
handles.view3Dmode = get(hObject,'Checked');
guidata(handles.fig, handles);
update3DView(handles);


%% Callback for option menu -> 3D View -> 3D Rendering %%%%%%%%%%%%%%%%%%%%
% enables or disables full 3D rendering. in case it is disabled the volume
% is rendered by 2D patches (faster)
function optionsMenu_enable3Dview_Callback(hObject, ~, handles)

cState = get(hObject,'checked');
if(strcmp(cState,'on'))
    set(hObject,'checked','off');
    handles.volumeViewEnables = 0;
else
    set(hObject,'checked','on');
    handles.volumeViewEnables = 1;
    set(handles.optionsMenu_enableSlice3D,'checked','off');
    set(handles.optionsMenu_enableInteractiveMIP,'checked','off');
end
guidata(handles.fig, handles);
update3DView(handles);



%% Updates the 3D View %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  clears the view or draws the view and enables/disables sliders/
%  editboxes
function update3DView(handles)

cla(handles.volumeViewAxes);

if(handles.volumeViewEnables == 0)
    set(handles.volumeViewAxes,'Visible','off');
    set(handles.azimuthSlider,'Enable','off');
    set(handles.elevationSlider,'Enable','off');
    set(handles.azimuthEditbox,'Enable','off');
    set(handles.elevationEditbox,'Enable','off');
    set(handles.alphamapSlider,'Enable','off');
    set(handles.alphamapEditbox,'Enable','off');
    set(handles.lowerThresholdSlider,'Enable','off');
    set(handles.upperThresholdSlider,'Enable','off');
    set(handles.invertLogicCheckbox,'Enable','off');
    set(handles.rotateButton,'Enable','off');
elseif(handles.volumeViewEnables == 1) % 3D Volume Rendering mode
    xLim = get(handles.volumeViewAxes,'xLim');
    yLim = get(handles.volumeViewAxes,'yLim');
    textHandle = text((xLim(1)+(xLim(2)-xLim(1)))/2,(yLim(1)+(yLim(2)-yLim(1)))/2,'Rendering 3D View. Stand by.','Color',[1 1 1],'Parent',handles.volumeViewAxes,'HorizontalAlignment','center');
    drawnow;
    drawVolumeView(handles, 1);
    handles = drawRectangle(handles);
    set(handles.azimuthSlider,'Enable','on');
    set(handles.elevationSlider,'Enable','on');
    set(handles.azimuthEditbox,'Enable','on');
    set(handles.elevationEditbox,'Enable','on');
    set(handles.alphamapSlider,'Enable','on');
    set(handles.alphamapEditbox,'Enable','on');
    set(handles.lowerThresholdSlider,'Enable','on');
    set(handles.upperThresholdSlider,'Enable','on');
    set(handles.invertLogicCheckbox,'Enable','on');
    set(handles.rotateButton,'Enable','on');
    delete(textHandle);
elseif(handles.volumeViewEnables == 2)  % slice visualization mode
    drawVolumeView(handles, 1);
    handles = guidata(handles.fig);
    handles = drawRectangle(handles);
    set(handles.azimuthSlider,'Enable','on');
    set(handles.elevationSlider,'Enable','on');
    set(handles.azimuthEditbox,'Enable','on');
    set(handles.elevationEditbox,'Enable','on');
    set(handles.alphamapSlider,'Enable','on');
    set(handles.alphamapEditbox,'Enable','on');
    set(handles.lowerThresholdSlider,'Enable','on');
    set(handles.upperThresholdSlider,'Enable','on');
    set(handles.invertLogicCheckbox,'Enable','on');
    set(handles.rotateButton,'Enable','on');
elseif(handles.volumeViewEnables == 3) % mip visualization
    if(strcmp(handles.reverseZ,'on'))
        temp = handles.volume(:,:,end:-1:1);
    else
        temp = handles.volume;
    end
    temp = rotateVolume(temp,2,90,'nearest',0); % change the orientation of the volume to be in accordance with the MIP processing
    temp = rotateVolume(temp,1,90,'nearest',0);
    handles.rotatedVolume=temp;
    clear temp;
    set(handles.azimuthSlider,'Enable','on');
    set(handles.elevationSlider,'Enable','on');
    set(handles.azimuthEditbox,'Enable','on');
    set(handles.elevationEditbox,'Enable','on');
    %     set(handles.alphamapSlider,'Enable','on');
    %     set(handles.alphamapEditbox,'Enable','on');
    %     set(handles.lowerThresholdSlider,'Enable','on');
    %     set(handles.upperThresholdSlider,'Enable','on');
    %     set(handles.invertLogicCheckbox,'Enable','on');
    set(handles.rotateButton,'Enable','on');
    drawVolumeView(handles, 1);
end
guidata(handles.fig,handles);



%% Closes the Window %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function miniViewer_CloseRequestFcn(hObject, ~, handles)

%save user settings
try
    userSetting.lastSaveDir = handles.lastSaveDir;
    userSetting.colormap = handles.colormap;
    userSetting.colormapFixed = handles.cmapFixed;
    userSetting.view3Dmode = handles.view3Dmode;
    userSetting.cursors2D = handles.cursors2D;
    userSetting.cursors3D = handles.cursors3D;
    userSetting.reverseZ = handles.reverseZ;
    userSetting.renderer = get(handles.fig,'Renderer');
    userSetting.swapXY = handles.swapXY;
    userSetting.windowByMouse = handles.windowByMouse;
    userSetting.histogramBins = handles.histogramBins;
    userSetting.histogramFilterMax = handles.histogramFilterMax;
    userSetting.linkWindowing = handles.linkWindowing;
    userSetting.linkSlicing = handles.linkSlicing;
    save(fullfile(handles.miniViewerDir,'userSettings.mat'),'userSetting');
catch
    disp('Error while saving the user settings. They have not been updated!')
end

delete(hObject);
clear handles;


%% Enables/Disables the mouse rotation mode for the 3D visualization.
function rotateButton_Callback(hObject, ~, handles)

updateMutualExclusiveButtons(handles,hObject);
resetColorFrame(handles);
if(get(hObject,'Value') == 1)
    
    set(handles.azimuthSlider,'Enable','off');
    set(handles.elevationSlider,'Enable','off');
    set(handles.azimuthEditbox,'Enable','off');
    set(handles.elevationEditbox,'Enable','off');
    
    handles.rotate3D = rotate3d(handles.volumeViewAxes);
    set(handles.rotate3D,'ActionPreCallback',@rotation3DPreCallback,'ActionPostCallback',@rotation3DPostCallback,'Enable','on');
else
    set(handles.rotate3D,'Enable','off');
    set(handles.azimuthSlider,'Enable','on');
    set(handles.elevationSlider,'Enable','on');
    set(handles.azimuthEditbox,'Enable','on');
    set(handles.elevationEditbox,'Enable','on');
    
end
guidata(handles.fig, handles);


%% Callback function of the 3d rotation tool. Executed before the user
% changes the view position
function rotation3DPreCallback(obj,evd)

handles = guidata(obj);
if(handles.volumeViewEnables == 3)
    x = [-0.5, -0.5, 0.5, 0.5];
    y = [-0.5, 0.5, 0.5, -0.5];
    
    % small colored cube with axis
    fill3(zeros(size(x(:)))+0.5,x(:),y(:),'r','Parent',handles.volumeViewAxes);
    hold on;
    fill3(zeros(size(x(:)))-0.5,x(:),y(:),'r','Parent',handles.volumeViewAxes);
    fill3(x(:),zeros(size(x(:)))+0.5,y(:),'g','Parent',handles.volumeViewAxes);
    fill3(x(:),zeros(size(x(:)))-0.5,y(:),'g','Parent',handles.volumeViewAxes);
    fill3(x(:),y(:),zeros(size(x(:)))+0.5,'b','Parent',handles.volumeViewAxes);
    fill3(x(:),y(:),zeros(size(x(:)))-0.5,'b','Parent',handles.volumeViewAxes);
    %z-axis with arrow
    line([0 0],[0 0],[-0.75 0.75],'Color','b');
    if(strcmp(handles.reverseZ,'off'))
        line([0 0],[0 0.05],[0.75 0.7],'Color','b');
        line([0 0],[0 -0.05],[0.75 0.7],'Color','b');
        line([0 -0.05],[0 0],[0.75 0.7],'Color','b');
        line([0 0.05],[0 0],[0.75 0.7],'Color','b');
    else
        line([0 0],[0 0.05],[-0.75 -0.7],'Color','b');
        line([0 0],[0 -0.05],[-0.75 -0.7],'Color','b');
        line([0 -0.05],[0 0],[-0.75 -0.7],'Color','b');
        line([0 0.05],[0 0],[-0.75 -0.7],'Color','b');
    end
    
    
    %y-axis with arrow
    line([0 0],[-0.75 0.75],[0,0],'Color','g');
    line([0 0.05],[0.75 0.7],[0,0],'Color','g');
    line([0 -0.05],[0.75 0.7],[0,0],'Color','g');
    line([0 0],[0.75 0.7],[0,0.05],'Color','g');
    line([0 0],[0.75 0.7],[0,-0.05],'Color','g');
    %x-axis with arrow
    line([-0.75 0.75],[0,0],[0,0],'Color','r');
    line([0.75 0.7],[0,0.05],[0,0],'Color','r');
    line([0.75 0.7],[0,-0.05],[0,0],'Color','r');
    line([0.75 0.7],[0,0],[0,0.05],'Color','r');
    line([0.75 0.7],[0,0],[0,-0.05],'Color','r');
    
    hold off;
    handles.currentViewPosition = [get(handles.azimuthSlider,'Value'),get(handles.elevationSlider,'Value')];
    set(evd.Axes,'View',handles.currentViewPosition);
end
set(handles.volumeViewAxes,'Visible','off'); % eq. to 'axis off'
set(handles.volumeViewAxes,'DataAspectRatio',[1 1 1],'DataAspectRatioMode','manual');
guidata(handles.fig,handles);



%% Callback function of the 3d rotation tool. Executed after the user
% changes the view position (mouse released)
function rotation3DPostCallback(obj,evd)

handles = guidata(obj);

[values] = round(get(evd.Axes,'View'));
handles.currentViewPosition = values;

set(handles.azimuthSlider,'Value',handles.currentViewPosition(1));
set(handles.elevationSlider,'Value',handles.currentViewPosition(2));
set(handles.azimuthEditbox,'String',num2str(handles.currentViewPosition(1)));
set(handles.elevationEditbox,'String',num2str(handles.currentViewPosition(2)));

if(handles.volumeViewEnables == 3)
    cla(handles.volumeViewAxes);
    drawVolumeView(handles,0);
end

set(handles.azimuthSlider,'Value',handles.currentViewPosition(1));
set(handles.elevationSlider,'Value',handles.currentViewPosition(2));
set(handles.azimuthEditbox,'String',num2str(handles.currentViewPosition(1)));
set(handles.elevationEditbox,'String',num2str(handles.currentViewPosition(2)));






%% Enables/Disables the data cursor mode
function dataCursorButton_Callback(hObject, ~, handles)

resetColorFrame(handles);
updateMutualExclusiveButtons(handles,hObject)
if(get(hObject,'Value') == 1)
    datacursormode on;
else
    datacursormode off;
end



%% Pressing "Shift" enables the turbo scroll mode for the Scrollwheel function!
% Pressing up & downarrow changes slice in the currently selected slicing
% dimension by handles.scrollSteps (up arrow = increase slice number, down
% arrow = decrease slice number
function miniViewer_KeyPressFcn(~, eventdata, handles)

if(strcmp(eventdata.Key,'shift')) % Turbo Scroll Mode enabled
    handles.scrollSteps = 5;
end
if(strcmp(eventdata.Key,'uparrow') || strcmp(eventdata.Key,'downarrow')) % slice changer by key press
    if(strcmp(handles.activeSlice,'x'))
        ax = handles.axesX;
    elseif(strcmp(handles.activeSlice,'y'))
        ax = handles.axesY;
    elseif(strcmp(handles.activeSlice,'z'))
        ax = handles.axesZ;
    else
        return; % no axes selected yet!
    end
    
    if(ax == handles.axesX)
        currentSlice = get(handles.xSliceSlider,'Value');
        axID = 1;
        axMaxSize = get(handles.xSliceSlider,'Max');
    elseif(ax == handles.axesY)
        currentSlice = get(handles.ySliceSlider,'Value');
        axID = 2;
        axMaxSize = get(handles.ySliceSlider,'Max');
    elseif(ax == handles.axesZ)
        currentSlice = get(handles.zSliceSlider,'Value');
        axID = 3;
        axMaxSize = get(handles.zSliceSlider,'Max');
    else
        return;
    end
    
    scrollValue = 0;
    if(strcmp(eventdata.Key,'uparrow'))
        scrollValue = handles.scrollSteps;  % increase slice number
    elseif(strcmp(eventdata.Key,'downarrow'))
        scrollValue = -handles.scrollSteps;  % decrease slice number
    end
    
    if(scrollValue > 0)
        if((currentSlice+handles.scrollSteps) <= axMaxSize)
            updateSlice(handles,axID,round(currentSlice+handles.scrollSteps),1);
        end
    elseif(scrollValue < 0)
        if((currentSlice-handles.scrollSteps) >= 1)
            updateSlice(handles,axID,round(currentSlice-handles.scrollSteps),1);
        end
    end
end

guidata(handles.fig, handles);



%% Releasing "Shift" disables the turbo scroll mode for the Scrollwheel function!
function miniViewer_KeyReleaseFcn(~, eventdata, handles)

if(strcmp(eventdata.Key,'shift'))
    handles.scrollSteps = 1;
end
guidata(handles.fig, handles);



%% Executes when a slice image is clicked on
function sliceButtonDownFcn(hObject, ~)
handles = guidata(gcf);

% reset the colors
resetColorFrame(handles);

% Highlight the chosen frame
if(strcmp(get(hObject,'Tag'),'x'))
    set(handles.xFrameArray,'BackgroundColor',handles.xHighlightColor);
    axes(handles.axesX);
elseif(strcmp(get(hObject,'Tag'),'y'))
    set(handles.yFrameArray,'BackgroundColor',handles.yHighlightColor);
    axes(handles.axesY);
elseif(strcmp(get(hObject,'Tag'),'z'))
    set(handles.zFrameArray,'BackgroundColor',handles.zHighlightColor);
    axes(handles.axesZ);
elseif(strcmp(get(hObject,'Tag'),'volumeViewAxes'))
    axes(handles.volumeViewAxes);
end



handles.activeSlice = get(hObject,'Tag');
guidata(handles.fig, handles);


%% Callback Function of the Slice Image Context Menu
function sliceImageContextMenu_Callback(~, ~, handles)

cuAx = gca;
if(cuAx == handles.volumeViewAxes)
    set(handles.sliceImageContext_Export_AsImage,'Enable','off'); % make gray in case of the 3D Axes
else
    set(handles.sliceImageContext_Export_AsImage,'Enable','on');
end



%% Callback Function of the Slice Image Context Menu -> Export
function sliceImageContext_Export_Callback(~, ~, ~)



%% Helper function to reset the colored frame to the normal color if any uicontrol is hit
function resetColorFrame(handles)

set(handles.xFrameArray,'BackgroundColor',handles.xNormalColor);
set(handles.yFrameArray,'BackgroundColor',handles.yNormalColor);
set(handles.zFrameArray,'BackgroundColor',handles.zNormalColor);
handles.activeSlice = [];


%%
function sliceImageContext_Export_AsFigure_Callback(~, ~, handles)

cuAx = gca;

hNew = figure;
ax = axes('Parent',hNew);
if(cuAx ~= handles.volumeViewAxes) % if called by slice
    if(strcmp(handles.activeSlice,'x'))
        copyobj(findobj('Type','image','Parent',handles.axesX),ax);
        clim = get(handles.axesX,'CLim');
        set(ax,'YDir','reverse');
    elseif(strcmp(handles.activeSlice,'y'))
        copyobj(findobj('Type','image','Parent',handles.axesY),ax);
        clim = get(handles.axesY,'CLim');
        set(ax,'YDir','reverse');
    elseif(strcmp(handles.activeSlice,'z'))
        copyobj(findobj('Type','image','Parent',handles.axesZ),ax);
        clim = get(handles.axesZ,'CLim');
        set(ax,'YDir','reverse');
    end
    axis auto;
    axis image;
else % called by the volume view axes
    copyobj(findobj('Parent',handles.volumeViewAxes),ax);
    clim = get(handles.volumeViewAxes,'CLim');
    view(ax,get(handles.azimuthSlider,'Value'),get(handles.elevationSlider,'Value'));
    
    if(strcmp(handles.reverseZ,'on'))
        set(ax,'ZDir','reverse');
    elseif(strcmp(handles.reverseZ,'off'))
        set(ax,'ZDir','normal');
    end
    set(ax,'Visible','off'); % eq. to 'axis off'
    
    % set(ax,'OuterPosition',handles.volumeViewPos);
    set(ax,'DataAspectRatio',[1 1 1],'DataAspectRatioMode','manual');
    aMap = 0:(1/64):1;
    set(get(ax,'Parent'),'AlphaMap',aMap.*get(handles.alphamapSlider,'Value'));
end

colormap(hNew,handles.colormap);
set(ax,'CLim',clim);




%% Function to save the currently clicked slice image as image file in a
%  chosable file format (jpg, bmp,...)
function sliceImageContext_Export_AsImage_Callback(~, ~, handles) %#ok<*DEFNU>

imageData = [];
clim = [];
if(strcmp(handles.activeSlice,'x'))
    imageData = get(handles.xImage,'CData');
    clim = get(handles.axesX,'CLim');
elseif(strcmp(handles.activeSlice,'y'))
    imageData = get(handles.yImage,'CData');
    clim = get(handles.axesY,'CLim');
elseif(strcmp(handles.activeSlice,'z'))
    imageData = get(handles.zImage,'CData');
    clim = get(handles.axesZ,'CLim');
end

if(~isempty(imageData) && ~isempty(clim) && size(size(imageData),2)<3)
    filterList = {'*.bmp';'*.jpg';'*.png';'*.tiff'};
    [fileName,pathName] = uiputfile(filterList,'Save Slice Image As',handles.lastSaveDir);
    if(~isempty(fileName) && ~isempty(pathName) && sum(fileName == 0)<1 && sum(pathName == 0)<1)
        imagePath = fullfile(pathName,fileName);
        
        %I ranges from 0 - 1
        cmap = eval(sprintf('%s(255)',handles.colormap'));
        I=mat2gray(imageData,clim);
        IndI = ceil(I*size(cmap,1));
        imwrite(IndI,cmap,imagePath);
        
        handles.lastSaveDir = pathName;
    end
elseif(~isempty(imageData) && ~isempty(clim) && size(size(imageData),2) == 3) %RGB image
    filterList = {'*.bmp';'*.jpg';'*.png';'*.tiff'};
    [fileName,pathName] = uiputfile(filterList,'Save Slice Image As',handles.lastSaveDir);
    if(~isempty(fileName) && ~isempty(pathName) && sum(fileName == 0)<1 && sum(pathName == 0)<1)
        imagePath = fullfile(pathName,fileName);
        imwrite(imageData,imagePath);
        handles.lastSaveDir = pathName;
    end
end
guidata(handles.fig, handles);


%% Callback function for slice 3D mode submenu
function optionsMenu_slice3D_Callback(~, ~, ~)





%% Callback function to enable/disable slice 3D mode
function optionsMenu_enableSlice3D_Callback(hObject, ~, handles)

cState = get(hObject,'checked');
if(strcmp(cState,'on'))
    set(hObject,'checked','off');
    handles.volumeViewEnables = 0;
else
    set(hObject,'checked','on');
    set(handles.optionsMenu_enable3Dview,'checked','off');
    set(handles.optionsMenu_enableInteractiveMIP,'checked','off');
    handles.volumeViewEnables = 2;
end
guidata(handles.fig, handles);
update3DView(handles);




%% Updates the slice 3D view with the current slices --> displaces
% the slice, renews image, sets alphamapping
function updateSlice3Dview(handles)

% getting the current slice numbers from the sliders
xPos = get(handles.ySliceSlider,'Value');
yPos = get(handles.xSliceSlider,'Value');
zPos = get(handles.zSliceSlider,'Value');

set(handles.slice3DhandlesY,'YData',ones(size(get(handles.slice3DhandlesY,'YData'))) .* yPos);
set(handles.slice3DhandlesY,'CData',get(handles.xImage,'CData'));
set(handles.slice3DhandlesY,'AlphaData',double(get(handles.slice3DhandlesY,'CData') > 0).* get(handles.alphamapSlider,'Value'));
set(handles.slice3DhandlesY,'AlphaDataMapping','none');

set(handles.slice3DhandlesX,'XData',ones(size(get(handles.slice3DhandlesX,'XData'))) .* xPos);
set(handles.slice3DhandlesX,'CData',get(handles.yImage,'CData'));
set(handles.slice3DhandlesX,'AlphaData',double(get(handles.slice3DhandlesX,'CData') > 0).* get(handles.alphamapSlider,'Value'));
set(handles.slice3DhandlesX,'AlphaDataMapping','none');

set(handles.slice3DhandlesZ,'ZData',ones(size(get(handles.slice3DhandlesZ,'ZData'))) .* zPos);
set(handles.slice3DhandlesZ,'CData',get(handles.zImage,'CData'));
set(handles.slice3DhandlesZ,'AlphaData',double(get(handles.slice3DhandlesZ,'CData') > 0).* get(handles.alphamapSlider,'Value'));
set(handles.slice3DhandlesZ,'AlphaDataMapping','none');



%% Callback function of the 3D submenu
function optionsMenu_3D_Callback(~, ~, ~)


%% Callback function of the reverse Z flag changer in menu
function optionsMenu_3D_reverseZ_Callback(hObject, ~, handles)

if(strcmp(get(hObject,'Checked'),'off'))
    set(hObject,'Checked','on');
    set(handles.volumeViewAxes,'ZDir','reverse');
elseif(strcmp(get(hObject,'Checked'),'on'))
    set(hObject,'Checked','off');
    set(handles.volumeViewAxes,'ZDir','normal');
end
handles.reverseZ = get(hObject,'Checked');

% recalculate the rotated volume
if(handles.volumeViewEnables == 3) % mip visualization
    if(strcmp(handles.reverseZ,'on'))
        temp = handles.volume(:,:,end:-1:1);
    else
        temp = handles.volume;
    end
    temp = rotateVolume(temp,2,90,'nearest',0); % change the orientation of the volume to be in accordance with the MIP processing
    temp = rotateVolume(temp,1,90,'nearest',0);
    handles.rotatedVolume=temp;
    drawVolumeView(handles);
end

guidata(handles.fig,handles);

%% Callback function for the invert Logic Checkbox --> changes whether the lower and upper
%  threshold should use </> resp. >/< operations
function invertLogicCheckbox_Callback(~, ~, handles)

resetColorFrame(handles);
drawVolumeView(handles, 1);
handles = drawRectangle(handles);
guidata(handles.fig,handles);



%% Callback function for the window center edit box. Allows setting the windowing center
%  by an exact value
function windowCenterEditbox_Callback(hObject, ~, handles)

stringValue = get(hObject,'String');
numValue = str2double(stringValue);
if(isnan(numValue))
    errordlg('The value entered is not numeric.')
    return;
end
if(numValue > get(handles.windowCenterSlider,'Max'))
    numValue =  get(handles.windowCenterSlider,'Max');
    set(hObject,'String',num2str(numValue));
end
if(numValue < get(handles.windowCenterSlider,'Min'))
    numValue =  get(handles.windowCenterSlider,'Min');
    set(hObject,'String',num2str(numValue));
end
set(handles.windowCenterSlider,'Value',numValue);
if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'on')) % max Values of all
    adaptVolumeWindowing(handles,1,1);
else
    adaptSliceWindowing(handles,1,1);
end




%% Create function for the window center edit box.
function windowCenterEditbox_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Callback function for the window width edit box. Allows setting the windowing width
%  by an exact value
function windowWidthEditbox_Callback(hObject, ~, handles)

stringValue = get(hObject,'String');
numValue = str2double(stringValue);
if(isnan(numValue))
    errordlg('The value entered is not numeric.')
    return;
end
if(numValue > get(handles.windowWidthSlider,'Max'))
    numValue =  get(handles.windowWidthSlider,'Max');
    set(hObject,'String',num2str(numValue));
end
if(numValue < get(handles.windowWidthSlider,'Min'))
    numValue =  get(handles.windowWidthSlider,'Min');
    set(hObject,'String',num2str(numValue));
end
set(handles.windowWidthSlider,'Value',numValue);
if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'on')) % max Values of all
    adaptVolumeWindowing(handles,1,1);
else
    adaptSliceWindowing(handles,1,1);
end




%% Create function for the window center edit box.
function windowWidthEditbox_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Callback function of the context menu entry to reset the window centering
%  to the default values
function windowCenterContextMenu_reset_Callback(~, ~, handles)

set(handles.windowCenterSlider,'Value',handles.minValue + 0.5 * (handles.maxValue - handles.minValue));
set(handles.windowCenterEditbox,'String',num2str(get(handles.windowCenterSlider,'Value')));
if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'on')) % max Values of all
    adaptVolumeWindowing(handles,1,1);
else
    adaptSliceWindowing(handles,1,1);
end


%% Callback function of the context menu entry to reset the window centering
%  to the default values
function windowWidthContextMenu_reset_Callback(~, ~, handles)


maxWidth = handles.maxValue - handles.minValue;

set(handles.windowWidthSlider,'Value',maxWidth);
set(handles.windowWidthEditbox,'String',num2str(get(handles.windowWidthSlider,'Value')));
if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'on')) % max Values of all
    adaptVolumeWindowing(handles,1,1);
else
    adaptSliceWindowing(handles,1,1);
end



%% Callback function of the context menu for the window width controls
%  nothing to do here...
function windowWidthContextMenu_Callback(~, ~, ~)

%% Callback function of the context menu for the window width controls
%  nothing to do here...
function windowCenterContextMenu_Callback(~, ~, ~)


%% Callback function of the distance measurement toggle button
% enable the measuring mode: requests two coordinates from the users by
% the ginput-function, calculates the distance between the coordinates and
% displays the distance as text and line.
% toggling the button to the state "off" removes the line, to use the tool
% again the user has to toggle button to the state "on" again
function distMeasureButton_Callback(hObject, ~, handles)

resetColorFrame(handles);
updateMutualExclusiveButtons(handles,hObject);

if(get(hObject,'Value') == 1)
    if(contains(struct2array(ver), 'Image Processing Toolbox'))
        uiwait(handles.fig);
        currentAxes = gca(handles.fig);
        
        pos = get(currentAxes,'CurrentPoint');
        pos = pos(1,1:2);
        
        xValues = get(currentAxes,'xLim');
        distX = xValues(2)-xValues(1);
        yValues = get(currentAxes,'yLim');
        distY = yValues(2)-yValues(1);
        
        initialDistancePerc = 10;
        
        startpoint = pos;
        if(startpoint(1) + (distX/initialDistancePerc) < xValues(2))
            endpoint(1) = startpoint(1) + (distX/initialDistancePerc);
        elseif(startpoint(1) - (distX/initialDistancePerc) > xValues(1))
            endpoint(1) = startpoint(1) - (distX/initialDistancePerc);
        else
            endpoint(1) = startpoint(1) + 1;
        end
        
        if(startpoint(2) + (distY/initialDistancePerc) < yValues(2))
            endpoint(2) = startpoint(2) + (distY/initialDistancePerc);
        elseif(startpoint(2) - (distY/initialDistancePerc) > yValues(1))
            endpoint(2) = startpoint(2) - (distY/initialDistancePerc);
        else
            endpoint(2) = startpoint(2) + 1;
        end
        
        c = get(currentAxes,'Children');
        currentTag = [];
        for idx = 1:size(c,1)
            if(strcmp(c(idx).Type,'image'))
                currentTag = c(idx).Tag;
            end
        end
        
        if(~isempty(currentTag) && (strcmp(currentTag,'x') || strcmp(currentTag,'y') || strcmp(currentTag,'z')))
            if(get(hObject,'Value') == 1) % state may have change while waiting for a click on the axis
                handles.distanceToolHandle = imdistline(currentAxes,[startpoint(1) endpoint(1)],[startpoint(2) endpoint(2)]);
                if(handles.imageResolutionAvailable)
                    setLabelTextFormatter(handles.distanceToolHandle,'%7.6f m'); % assuming SI units -> m
                else
                    setLabelTextFormatter(handles.distanceToolHandle,'%7.6f px'); % assuming SI units -> m
                end
                
            end
        else
            warndlg('Click on X, Y or Z axes to enable distance measurement');
        end
    else % old implementation with ginput
        % enable the measuring mode
        validExit = 0;
        try
            [x,y] = ginput(2);
            validExit = 1;
        catch
            
        end
        if(validExit)
            handles.measurementLineHandle(1) = line(x,y,'Color',[0 0 0],'Marker','.'); % drawing a black line
            imSize(1,:) = get(get(handles.measurementLineHandle(1),'Parent'),'XLim');
            imSize(2,:) = get(get(handles.measurementLineHandle(1),'Parent'),'YLim');
            handles.measurementLineHandle(2) = line(x+(0.0025*imSize(1,2)),y+(0.0025*imSize(2,2)),'Color',[1 1 1],'Marker','.'); % drawing a white line slightly off the black line
            % description Text --> px or mm
            
            if(handles.imageResolutionAvailable)
                % multiply by resolution --> depending on current axis!
                currentAxisHandle = get(handles.measurementLineHandle(1),'Parent');
                if(currentAxisHandle == handles.axesX)
                    xRes = handles.imageResolution(3);
                    yRes = handles.imageResolution(2);
                elseif(currentAxisHandle == handles.axesY)
                    xRes = handles.imageResolution(3);
                    yRes = handles.imageResolution(1);
                elseif(currentAxisHandle == handles.axesZ)
                    xRes = handles.imageResolution(2);
                    yRes = handles.imageResolution(1);
                end
                dText = 'm';
            else
                xRes = 1;
                yRes = 1;
                dText = 'px';
            end
            if(~handles.startpointAvailable)
                distance = sqrt(((x(1)-x(2))*xRes)^2 + ((y(1)-y(2))*yRes)^2);
            else
                distance = sqrt(((x(1)-x(2)))^2 + ((y(1)-y(2)))^2);
            end
            
            
            
            handles.measurementTextHandle(1) = text(x(1)+((x(2)-x(1))/2),y(1)+((y(2)-y(1))/2),sprintf('%7.6f %s',distance,dText),'Color',[0 0 0]); % rendering the text at the center position of the line
            handles.measurementTextHandle(2) = text(x(1)+(0.0025*imSize(1,2))+((x(2)-x(1))/2),y(1)+(0.0025*imSize(2,2))+((y(2)-y(1))/2),sprintf('%7.6f %s',distance,dText),'Color',[1 1 1]); % rendering the text at the center position of the line --> shadow (white)
            set(handles.measurementLineHandle,'Visible','on');
            set(handles.measurementTextHandle,'Visible','on');
        end
    end

else
    if (isfield(handles,'distanceToolHandle'))
        delete(handles.distanceToolHandle);
        handles = rmfield(handles,'distanceToolHandle');
    end
    if(isfield(handles,'measurementLineHandle'))
        set(handles.measurementLineHandle,'Visible','off');
    end
    if(isfield(handles,'measurementTextHandle'))
        set(handles.measurementTextHandle,'Visible','off');
    end
end
guidata(handles.fig,handles);


% Updates the mutually exclusive buttons for zoom, shift, data cursors,
% distance measurement etc.
function updateMutualExclusiveButtons(handles,sourceHandle)

value = get(sourceHandle,'Value');

set(handles.zoomButton,'Value',0);
set(handles.zoomButton,'BackgroundColor',[0.941 0.941 0.941]);
set(handles.shiftButton,'Value',0);
set(handles.shiftButton,'BackgroundColor',[0.941 0.941 0.941]);
set(handles.rotateButton,'Value',0);
set(handles.rotateButton,'BackgroundColor',[0.941 0.941 0.941]);
set(handles.dataCursorButton,'Value',0);
set(handles.dataCursorButton,'BackgroundColor',[0.941 0.941 0.941]);
set(handles.distMeasureButton,'Value',0);
set(handles.distMeasureButton,'BackgroundColor',[0.941 0.941 0.941]);
zoom off;
pan off;
rotate3d(handles.volumeViewAxes,'off');
set(handles.azimuthSlider,'Value',handles.currentViewPosition(1));
set(handles.elevationSlider,'Value',handles.currentViewPosition(2));
set(handles.azimuthEditbox,'String',num2str(handles.currentViewPosition(1)));
set(handles.elevationEditbox,'String',num2str(handles.currentViewPosition(2)));
datacursormode off;

try
    set(handles.measurementLineHandle,'Visible','off');
    set(handles.measurementTextHandle,'Visible','off');
catch
    
end

set(sourceHandle,'Value',value);
if(value == 1)
    set(sourceHandle,'BackgroundColor',[0.3 0.5 1]);
end


%% Callback function of the export menu
%  nothing to do here...
function optionsMenu_export_Callback(~, ~, ~)



%% Callback function of the menu entry to export all slices to image files
function optionsMenu_export_allSlices_Callback(~, ~, handles)

filterList = {'*.png';'*.bmp';'*.jpg';'*.tiff'};
[fileName,pathName] = uiputfile(filterList,'Save Slice Image As');
if(~isempty(fileName) && ~isempty(pathName) && sum(fileName == 0)<1 && sum(pathName == 0)<1) % catching the case that cancel is pressed in the uiputfile
    x = get(handles.xSliceSlider,'Value');
    y = get(handles.ySliceSlider,'Value');
    z = get(handles.zSliceSlider,'Value');
    if strcmp(fileName(end-3:end), '.png')
        fileType='.png';
        fileName=fileName(1:end-4);
    elseif strcmp(fileName(end-3:end), '.bmp')
        fileType='.bmp';
        fileName=fileName(1:end-4);
    elseif strcmp(fileName(end-3:end), '.jpg')
        fileType='.jpg';
        fileName=fileName(1:end-4);
    elseif strcmp(fileName(end-4:end), '.tiff')
        fileType='.tiff';
        fileName=fileName(1:end-5);
    end
    
    % x-image
    imageData = get(handles.xImage,'CData');
    clim = get(handles.axesX,'CLim');
    if(~isempty(imageData) && ~isempty(clim) && size(size(imageData),2)<3)
        newFileName=[fileName, '_x' num2str(x), fileType];
        
        if(~isempty(newFileName) && ~isempty(pathName))
            imagePath = fullfile(pathName,newFileName);
            
            %I ranges from 0 - 1
            cmap = eval(sprintf('%s(255)',handles.colormap'));
            I=mat2gray(imageData,clim);
            IndI = ceil(I*size(cmap,1));
            imwrite(IndI,cmap,imagePath);
        end
    elseif(~isempty(imageData) && ~isempty(clim) && size(size(imageData),2) == 3) %RGB image
        newFileName=[fileName, '_x' num2str(x), fileType];
        if(~isempty(newFileName) && ~isempty(pathName))
            imagePath = fullfile(pathName,newFileName);
            imwrite(imageData,imagePath);
        end
    end
    
    % y-image
    imageData = get(handles.yImage,'CData');
    clim = get(handles.axesY,'CLim');
    if(~isempty(imageData) && ~isempty(clim) && size(size(imageData),2)<3)
        newFileName=[fileName, '_y', num2str(y), fileType];
        
        if(~isempty(newFileName) && ~isempty(pathName))
            imagePath = fullfile(pathName,newFileName);
            
            %I ranges from 0 - 1
            cmap = eval(sprintf('%s(255)',handles.colormap'));
            I=mat2gray(imageData,clim);
            IndI = ceil(I*size(cmap,1));
            imwrite(IndI,cmap,imagePath);
        end
    elseif(~isempty(imageData) && ~isempty(clim) && size(size(imageData),2) == 3) %RGB image
        newFileName=[fileName, '_y', num2str(y), fileType];
        if(~isempty(newFileName) && ~isempty(pathName))
            imagePath = fullfile(pathName,newFileName);
            imwrite(imageData,imagePath);
        end
    end
    
    % z-image
    imageData = get(handles.zImage,'CData');
    clim = get(handles.axesZ,'CLim');
    if(~isempty(imageData) && ~isempty(clim) && size(size(imageData),2)<3)
        newFileName=[fileName, '_z', num2str(z), fileType];
        
        if(~isempty(newFileName) && ~isempty(pathName))
            imagePath = fullfile(pathName,newFileName);
            
            %I ranges from 0 - 1
            cmap = eval(sprintf('%s(255)',handles.colormap'));
            I=mat2gray(imageData,clim);
            IndI = ceil(I*size(cmap,1));
            imwrite(IndI,cmap,imagePath);
        end
    elseif(~isempty(imageData) && ~isempty(clim) && size(size(imageData),2) == 3) %RGB image
        newFileName=[fileName, '_z', num2str(z), fileType];
        if(~isempty(newFileName) && ~isempty(pathName))
            imagePath = fullfile(pathName,newFileName);
            imwrite(imageData,imagePath);
        end
    end
end


%% Callback function of the export menu entry to copy a screenshot of the
%  GUI to the clipboard
function optionsMenu_export_toClipboard_Callback(~, ~, handles)

if(ispc)
    set(handles.fig,'PaperPositionMode','auto');
    print(handles.fig,'-dmeta','-r0');
else
    disp('Export to clipboard not supported for non Windows systems');
end


%% Callback function of the tools menu entry. Not used currently
function toolsMenu_Callback(~, ~, ~)



%% Callback function of the tools menu entry "Movie Player". Open the
%  rotation movie player
function tools_rotatingMoviePlayer_Callback(~, ~, handles)

moviePlayer(handles.volume,handles.colormap);


%% Callback function of the tools menu entry "Iso Surface Visualization". Open the
%  rotation movie player
function isoSurfaceVis_Callback(~, ~, handles)

isoSurfaceVis(handles.volume);


%% Callback function of the Options menu entry "renderer": nothing to do here
function optionsMenu_renderer_Callback(~, ~, ~)



%% Callback function of the Options menu entry renderer --> OpenGL
% sets the render mode of the figure to OpenGL
function optionsMenu_renderer_openGL_Callback(~, ~, handles)

set(handles.fig,'Renderer','OpenGL');
set(handles.optionsMenu_renderer_openGL,'Checked','on');
set(handles.optionsMenu_renderer_zBuffer,'Checked','off');
set(handles.optionsMenu_renderer_painter,'Checked','off');

%% Callback function of the Options menu entry renderer --> zBuffer
% sets the render mode of the figure to zbuffer
function optionsMenu_renderer_zBuffer_Callback(~, ~, handles)

set(handles.fig,'Renderer','zbuffer');
set(handles.optionsMenu_renderer_openGL,'Checked','off');
set(handles.optionsMenu_renderer_zBuffer,'Checked','on');
set(handles.optionsMenu_renderer_painter,'Checked','off');



%% Callback function of the Options menu entry renderer --> Painters
% sets the render mode of the figure to painters
function optionsMenu_renderer_painter_Callback(~, ~, handles)

set(handles.fig,'Renderer','painters');
set(handles.optionsMenu_renderer_openGL,'Checked','off');
set(handles.optionsMenu_renderer_zBuffer,'Checked','off');
set(handles.optionsMenu_renderer_painter,'Checked','on');


%% Callback function of the Options menu entry interactive mip: nothing to do here
function optionsMenu_interactiveMIP_Callback(~, ~, ~)



%% Callback function of the Options menu entry 3D visualization --> interactive MIP
% changes/enables the 3D mode to the interactive MIP visualization
function optionsMenu_enableInteractiveMIP_Callback(hObject, ~, handles)

cState = get(hObject,'checked');
if(strcmp(cState,'on'))
    set(hObject,'checked','off');
    handles.volumeViewEnables = 0;
else
    set(hObject,'checked','on');
    handles.volumeViewEnables = 3;
    % deactivating some 3d options if we are coming from a previous 3d mode
    set(handles.alphamapSlider,'Enable','off');
    set(handles.alphamapEditbox,'Enable','off');
    set(handles.lowerThresholdSlider,'Enable','off');
    set(handles.upperThresholdSlider,'Enable','off');
    set(handles.optionsMenu_enableSlice3D,'checked','off');
    set(handles.optionsMenu_enable3Dview,'checked','off');
end
guidata(handles.fig, handles);
update3DView(handles);


%% Callback function of the Options menu entry 2D --> swap X/Y axis
% changes the view of all three 2D axes such that x and y is swapped
function optionsMenu_swapXY_Callback(hObject, ~, handles)

cState = get(hObject,'checked');

if(strcmp(cState,'on'))
    set(hObject,'checked','off');
    set(handles.axesX, 'View', [0, 90]);
    set(handles.axesY, 'View', [0, 90]);
    set(handles.axesZ, 'View', [0, 90]);
    handles.swapXY = 0;
else
    set(hObject,'checked','on');
    set(handles.axesX, 'View', [-90, 90]);
    set(handles.axesY, 'View', [-90, 90]);
    set(handles.axesZ, 'View', [-90, 90]);
    handles.swapXY = 1;
end

guidata(handles.fig, handles);


%% Callback function of the Context menu entry Reverse X
% swaps the x-axis of the currently active axes between 'normal' and 'reverse'
function sliceImageContext_reverseX_Callback(~, ~, handles)

if(strcmp(handles.activeSlice,'x'))
    currentHandle = handles.axesX;
elseif(strcmp(handles.activeSlice,'y'))
    currentHandle = handles.axesY;
elseif(strcmp(handles.activeSlice,'z'))
    currentHandle = handles.axesZ;
end

if(strcmp(get(currentHandle,'XDir'),'normal'))
    set(currentHandle,'XDir','reverse');
else
    set(currentHandle,'XDir','normal');
end


%% Callback function of the Context menu entry Reverse Y
% swaps the y-axis of the currently active axes between 'normal' and 'reverse'
function sliceImageContext_reverseY_Callback(~, ~, handles)

if(strcmp(handles.activeSlice,'x'))
    currentHandle = handles.axesX;
elseif(strcmp(handles.activeSlice,'y'))
    currentHandle = handles.axesY;
elseif(strcmp(handles.activeSlice,'z'))
    currentHandle = handles.axesZ;
end

if(strcmp(get(currentHandle,'YDir'),'normal'))
    set(currentHandle,'YDir','reverse');
else
    set(currentHandle,'YDir','normal');
end


%% Callback function for the mouse movement. Executes on mouse motion over
% figure - except title and menu.
function miniViewer_WindowButtonMotionFcn(~, ~, handles)

% Windowing based on mouse movement
if(handles.windowByMouse)
    if(handles.windowButtonDown == 1)
        if(strcmp(handles.activeSlice,'x'))
            ax = handles.axesX;
        elseif(strcmp(handles.activeSlice,'y'))
            ax = handles.axesY;
        elseif(strcmp(handles.activeSlice,'z'))
            ax = handles.axesZ;
        else
            return;
        end
        C = get(ax, 'CurrentPoint');
        if(~isfield(handles,'lastMousePos'))
            handles.lastMousePos = [C(1,1) C(1,2)]; % first time used
        else
            diff = handles.lastMousePos - [C(1,1) C(1,2)];      % difference to last mouse position
            handles.lastMousePos = [C(1,1) C(1,2)];             % updating last mouse position
            
            % adapt the window width based on y movement (up/down)
            windowWidthStep = get(handles.windowWidthSlider,'SliderStep');
            sStep = double(get(handles.windowWidthSlider,'Max')-get(handles.windowWidthSlider,'Min')) * windowWidthStep(1);
            if(diff(2)<0)
                incWidth = -sStep(1);
            elseif(diff(2)>0)
                incWidth = sStep(1);
            else
                incWidth = 0;
            end
            
            if(get(handles.windowWidthSlider,'Value')+incWidth > get(handles.windowWidthSlider,'Min') && get(handles.windowWidthSlider,'Value')+incWidth < get(handles.windowWidthSlider,'Max'))
                set(handles.windowWidthSlider,'Value',get(handles.windowWidthSlider,'Value')+incWidth);
                set(handles.windowWidthEditbox,'String',num2str(get(handles.windowWidthSlider,'Value')));
            end
            
            % adapt the window center based on x movement (left/right)
            windowCenterStep = get(handles.windowCenterSlider,'SliderStep');
            sStep = double(get(handles.windowCenterSlider,'Max')-get(handles.windowCenterSlider,'Min')) * windowCenterStep(1);
            if(diff(1)<0)
                incCenter = sStep(1);
            elseif(diff(1)>0)
                incCenter = -sStep(1);
            else
                incCenter = 0;
            end
            
            if(get(handles.windowCenterSlider,'Value')+incCenter > get(handles.windowCenterSlider,'Min') && get(handles.windowCenterSlider,'Value')+incCenter < get(handles.windowCenterSlider,'Max'))
                set(handles.windowCenterSlider,'Value',get(handles.windowCenterSlider,'Value')+incCenter);
                set(handles.windowCenterEditbox,'String',num2str(get(handles.windowCenterSlider,'Value')));
            end
            
            % update the windowing of images
            if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'on')) % max Values of all
                adaptVolumeWindowing(handles,1,1);
            else
                adaptSliceWindowing(handles,1,1);
            end
            
        end
        guidata(handles.fig,handles);
    end
    
end

%% Window Button Down function - when mouse button is clicked
% sets state variable
function miniViewer_WindowButtonDownFcn(~, ~, handles)

handles.windowButtonDown = 1;

uiresume(handles.fig);
guidata(handles.fig,handles);


%% Window Button Up function - when mouse button is released
function miniViewer_WindowButtonUpFcn(~, ~, handles)

handles.windowButtonDown = 0;
guidata(handles.fig,handles);


%% Callback function of the windowing by Mouse submenu of options --> colormap.
% enables/disables the windowing by the mouse movement input
function optionsMenu_colormap_windowByMouse_Callback(hObject, ~, handles)

if(strcmp(get(hObject,'Checked'),'off'))
    set(hObject,'Checked','on');
    handles.windowByMouse = 1;
else
    set(hObject,'Checked','off');
    handles.windowByMouse = 0;
end

guidata(handles.fig,handles);


%% Internal function to update the histogram if bins are changed or window
% center / window width values are changed
function handles = updateHistogram(handles)

if(handles.drawHistogram)
    if(~isfield(handles,'histogramValues') || isempty(handles.histogramValues)) % if not available compute the histogram once
        
        if(verLessThan('matlab','8.4')) % backward compatibility for very old MATLAB version -> using hist. 
            if (isa(handles.volume,'single') || isa(handles.volume,'double') || isa(handles.volume, 'locigal'))
                
                % Binning & histogram computation
                xValues = linspace(handles.minValue,handles.maxValue,handles.histogramBins);
                xEdges = xValues+(xValues(2)-xValues(1))/2;
                                
                [handles.histogramValues, handles.histogramEdges] = hist(handles.volume(:),xEdges);
            else
                warning('Data type is not supported for histogram generation. Histogram can not be displayed.')
                return;
            end
        else % recommended vesion of histogram computation using histcounts. 
            
            % Binning & histogram computation.
            xEdges = linspace(handles.minValue,handles.maxValue,handles.histogramBins+1);
            % xValues holds the central position of the bins for the bar
            % plot. 
            xValues = xEdges(1:end-1);
            xValues = xValues+(xValues(2)-xValues(1))/2;
            
            [handles.histogramValues,handles.histogramEdges] = histcounts(handles.volume(:),xEdges);
        end
        
        
        % filtering out the bin the occurs most frequently --> e.g. if
        % background is zero this is useful!
        if(handles.histogramFilterMax == 1)
            handles.histogramValues(handles.histogramValues == max(handles.histogramValues)) = 0;
        end
        
        % plot the histogram to the axes object
        handles.barplot = bar(xValues,handles.histogramValues,'BarWidth',1,'Parent',handles.histogramAxes,'FaceColor',[1 1 1],'EdgeColor',[0.8 0.8 0.8]);
        set(handles.barplot,'UIContextMenu',handles.histogramContextMenu);
        
        % create the box with current window center and window width
        x = get(handles.windowCenterSlider,'Value') - (get(handles.windowWidthSlider,'Value')/2);
        w = get(handles.windowWidthSlider,'Value');
        
        % semi-transparent box: dragable to adapt window center only
        handles.windowBox = patch('XData',[x x x+w x+w],'YData',[0 max(handles.histogramValues)*1.1 max(handles.histogramValues)*1.1 0],'FaceColor',[0.8 0 0],'EdgeColor',[1 0 0],'FaceAlpha',0.5,'Parent',handles.histogramAxes);
        set(handles.windowBox,'UIContextMenu',handles.histogramContextMenu);
        draggable(handles.windowBox,'h',[min(xValues), max(xValues)],@windowBoxMoved);
        
        % left and right border of the box: dragable to adjust window width
        % (and accordingly the center)
        handles.windowBoundaryLinesLeft = line([x x],[0 max(handles.histogramValues)*1.1],'Parent',handles.histogramAxes);
        set(handles.windowBoundaryLinesLeft,'Color',[1 0 0],'LineWidth',2,'Tag','left');
        draggable(handles.windowBoundaryLinesLeft,'h',[min(xValues), max(xValues)],@windowLineMoved);
        
        handles.windowBoundaryLinesRight = line([x+w x+w],[0 max(handles.histogramValues)*1.1],'Parent',handles.histogramAxes);
        set(handles.windowBoundaryLinesRight,'Color',[1 0 0],'LineWidth',2,'Tag','right');
        draggable(handles.windowBoundaryLinesRight,'h',[min(xValues), max(xValues)],@windowLineMoved);
        
        % nice coloring of the histogram plot
        set(handles.histogramAxes,'XColor',[0.3 0.3 0.3]);
        set(handles.histogramAxes,'YColor',[0.3 0.3 0.3]);
        set(handles.histogramAxes,'Color',[0.0 0.0 0.0]);
        set(handles.histogramAxes,'XTick',[]);
        set(handles.histogramAxes,'YTick',[]);
        set(handles.histogramAxes,'XLim',[handles.minValue, handles.maxValue]);
        set(handles.histogramAxes,'YLim',[min(handles.histogramValues), max(handles.histogramValues)*1.1]);
        
        guidata(handles.fig,handles);
        
    else % just change the position according to window center and window width
        x = get(handles.windowCenterSlider,'Value') - (get(handles.windowWidthSlider,'Value')/2);
        w = get(handles.windowWidthSlider,'Value');
        set(handles.windowBox,'XData',[x x x+w x+w],'YData',[0 max(handles.histogramValues)*1.1 max(handles.histogramValues)*1.1 0]);
        set(handles.windowBoundaryLinesLeft,'XData',[x x],'YData',[0 max(handles.histogramValues)*1.1]);
        set(handles.windowBoundaryLinesRight,'XData',[x+w x+w],'YData',[0 max(handles.histogramValues)*1.1]);
    end
end


%% Callback function for histogram context menu bin entries
function histogramListCallback(hObject, ~)

handles = guidata(gcf);

handles.histogramBins = str2double(get(hObject,'Label'));
handles.histogramValues = [];
guidata(handles.fig,handles);
updateHistogram(handles);  % recalculate the histogram!

cObjects = get(handles.histogramContextMenu_numBins,'Children');

for n = 1:size(cObjects,1)
    if(strcmp(get(cObjects(n),'Label'),num2str(handles.histogramBins)))
        set(cObjects(n),'Checked','on');
    else
        set(cObjects(n),'Checked','off');
    end
end



%% Callback draggable window box (shift left to right to adapt windowing)
function windowBoxMoved(hObject, ~)

handles = guidata(gcf);
pos = get(hObject,'XData');
windowCenter = mean([pos(1) pos(3)]);
windowWidth = pos(3)-pos(1);

set(handles.windowBoundaryLinesLeft,'XData',[pos(1) pos(1)],'YData',[0 max(handles.histogramValues)*1.1]);
set(handles.windowBoundaryLinesRight,'XData',[pos(3) pos(3)],'YData',[0 max(handles.histogramValues)*1.1]);

set(handles.windowWidthSlider,'Value',windowWidth);
set(handles.windowCenterSlider,'Value',windowCenter);

set(handles.windowWidthEditbox,'String',num2str(get(handles.windowWidthSlider,'Value')));
set(handles.windowCenterEditbox,'String',num2str(get(handles.windowCenterSlider,'Value')));

if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'on')) % max Values of all
    adaptVolumeWindowing(handles,1,1);
else
    adaptSliceWindowing(handles,1,1);
end



%% Callback draggable window box boundaries (shift left to right to adapt windowing)
function windowLineMoved(hObject,~)

handles = guidata(gcf);
pos = get(hObject,'XData');
posLeft = get(handles.windowBoundaryLinesLeft,'XData');
posRight = get(handles.windowBoundaryLinesRight,'XData');

if(strcmp(get(hObject,'Tag'),'left'))
    if(pos(1) >= posRight(1))
        set(handles.windowBoundaryLinesRight,'XData',pos+eps);
        posRight = pos + handles.addOnEps;
    end
elseif(strcmp(get(hObject,'Tag'),'right'))
    if(pos(1) <= posLeft(1))
        set(handles.windowBoundaryLinesLeft,'XData',pos-eps);
        posLeft = pos - handles.addOnEps;
    end
end

if(strcmp(get(hObject,'Tag'),'left'))
    windowWidth = posRight(1)-pos(1);
    windowCenter = mean([posRight(1) pos(1)]);
elseif(strcmp(get(hObject,'Tag'),'right'))
    windowWidth = pos(1)-posLeft(1);
    windowCenter = mean([posLeft(1) pos(1)]);
end

%emergency limitation - might occure if line is move to fast(?)
if(windowWidth < get(handles.windowWidthSlider,'Min'))
   windowWidth = get(handles.windowWidthSlider,'Min');
elseif(windowWidth > get(handles.windowWidthSlider,'Max'))
   windowWidth = get(handles.windowWidthSlider,'Max');
end

if(windowCenter < get(handles.windowCenterSlider,'Min'))
   windowCenter = get(handles.windowCenterSlider,'Min');
elseif(windowCenter > get(handles.windowCenterSlider,'Max'))
   windowCenter = get(handles.windowCenterSlider,'Max');
end


set(handles.windowWidthSlider,'Value',windowWidth);
set(handles.windowCenterSlider,'Value',windowCenter);
x = windowCenter - windowWidth/2;
set(handles.windowBox,'XData',[x x x+windowWidth x+windowWidth],'YData',[0 max(handles.histogramValues)*1.1 max(handles.histogramValues)*1.1 0]);

set(handles.windowWidthEditbox,'String',num2str(get(handles.windowWidthSlider,'Value')));
set(handles.windowCenterEditbox,'String',num2str(get(handles.windowCenterSlider,'Value')));

if(strcmp(get(handles.optionsMenu_colormap_keepFixed,'Checked'),'on')) % max values of all
    adaptVolumeWindowing(handles,1,1);
else
    adaptSliceWindowing(handles,1,1);
end



%% Callback function of the context menu for histogram
function histogramContextMenu_Callback(~, ~, ~)


%% Callback function of the context menu entry for histogram
function histogramContextMenu_numBins_Callback(~, ~, ~)


%% Callback function of the context menu entry for histogram filter of most
% prominent value.
function histogramContextMenu_filterMax_Callback(hObject, ~, handles)

state = get(hObject,'Checked');
if(strcmp(state,'on'))
    set(hObject,'Checked','off');
    handles.histogramFilterMax = 0;
elseif(strcmp(state,'off'))
    set(hObject,'Checked','on')
    handles.histogramFilterMax = 1;
end
handles.histogramValues = [];
guidata(handles.fig,handles);
updateHistogram(handles);

%% Callback function options menu -> export -> for publication
function optionsMenu_export_forPublication_Callback(~, ~, handles)

exportForPublication(handles,get(handles.optionsMenu_swapXY,'Checked'));



%% Callback function for options menu -> linked miniViewer. 
function optionsMenu_linkedViewer_Callback(~, ~, ~)



%% Callback function for options menu -> linked miniViewer -> Enable slice linking. 
function optionsMenu_linkedViewer_enableSlicingLink_Callback(hObject, ~, handles)

state = get(hObject,'Checked');
if(strcmp(state,'on'))
    set(hObject,'Checked','off');
    handles.linkSlicing = false;
    if(~isempty(handles.linkedFig))
        tempHandles = guidata(handles.linkedFig);
        tempHandles.linkSlicing = false;
        set(tempHandles.optionsMenu_linkedViewer_enableSlicingLink,'Checked','off');
        guidata(handles.linkedFig,tempHandles);
    end
elseif(strcmp(state,'off'))
    set(hObject,'Checked','on');
    handles.linkSlicing = true;
    if(~isempty(handles.linkedFig))
        tempHandles = guidata(handles.linkedFig);
        tempHandles.linkSlicing = true;
        set(tempHandles.optionsMenu_linkedViewer_enableSlicingLink,'Checked','on');
        guidata(handles.linkedFig,tempHandles);
    end
end
    
guidata(handles.fig,handles);



%% Callback function for options menu -> linked miniViewer -> Enable window linking. 
function optionsMenu_linkedViewer_enableWindowingLink_Callback(hObject, ~, handles)

state = get(hObject,'Checked');
if(strcmp(state,'on'))
    set(hObject,'Checked','off');
    handles.linkWindowing = false;
    if(~isempty(handles.linkedFig))
        tempHandles = guidata(handles.linkedFig);
        tempHandles.linkWindowing = false;
        set(tempHandles.optionsMenu_linkedViewer_enableWindowingLink,'Checked','off');
        guidata(handles.linkedFig,tempHandles);
    end
elseif(strcmp(state,'off'))
    set(hObject,'Checked','on');
    handles.linkWindowing = true;
    if(~isempty(handles.linkedFig))
        tempHandles = guidata(handles.linkedFig);
        tempHandles.linkWindowing = true;
        set(tempHandles.optionsMenu_linkedViewer_enableWindowingLink,'Checked','on');
        guidata(handles.linkedFig,tempHandles);
    end
end
    
guidata(handles.fig,handles);
