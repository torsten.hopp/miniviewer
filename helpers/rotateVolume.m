%% helper function: rotates the volume with given axis, angle, interpolation method and cropping parameter
function vol =  rotateVolume(vol, axis, angle, interpolmethod, crop)

if(axis == 3)
    if(crop == 1)
        vol = imrotate(vol, angle, interpolmethod,'crop');
    else
        vol = imrotate(vol, angle, interpolmethod);
    end
elseif(axis == 2)
    vol = permute(vol,[1 3 2]);
    
    if(crop == 1)
        vol = imrotate(vol, angle, interpolmethod,'crop');
    else
        vol = imrotate(vol, angle, interpolmethod);
    end
    vol = ipermute(vol,[1 3 2]);
elseif(axis == 1)
    vol = permute(vol,[3 2 1]);
    if(crop == 1)
        vol = imrotate(vol, angle, interpolmethod,'crop');
    else
        vol = imrotate(vol, angle, interpolmethod);
    end
    vol = ipermute(vol,[3 2 1]);
end