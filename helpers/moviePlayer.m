function varargout = moviePlayer(varargin)
% MOVIEPLAYER MATLAB code for moviePlayer.fig
%      MOVIEPLAYER, by itself, creates a new MOVIEPLAYER or raises the existing
%      singleton*.
%
%      H = MOVIEPLAYER returns the handle to a new MOVIEPLAYER or the handle to
%      the existing singleton*.
%
%      MOVIEPLAYER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MOVIEPLAYER.M with the given input arguments.
%
%      MOVIEPLAYER('Property','Value',...) creates a new MOVIEPLAYER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before moviePlayer_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to moviePlayer_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help moviePlayer

% Last Modified by GUIDE v2.5 02-Sep-2016 16:08:54

% Begin initialization code - DO NOT EDIT

gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @moviePlayer_OpeningFcn, ...
                   'gui_OutputFcn',  @moviePlayer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT




% --- Executes just before moviePlayer is made visible.
function moviePlayer_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to moviePlayer (see VARARGIN)

global stop;

if (~isempty(varargin)),
    if(ndims(varargin{1})>2) %#ok<ISMAT>
        handles.volume = varargin{1};
        handles.colormap = varargin{2};
    else
        return;
    end
end

% Choose default command line output for moviePlayer
handles.output = hObject;

set(handles.saveAs,'Enable','off');
handles.currentFrame = 1;

handles.moviePlayerFig = gcf;

% Update handles structure
guidata(hObject, handles);

axes(handles.movieAxis);
axis off;

% UIWAIT makes moviePlayer wait for user response (see UIRESUME)
% uiwait(handles.moviePlayerFig);


% --- Outputs from this function are returned to the command line.
function varargout = moviePlayer_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function frameSlider_Callback(hObject, eventdata, handles)
% hObject    handle to frameSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

k = get(hObject,'Value');
if(round(k)-k ~= 0)
   k = round(k);
   set(hObject,'Value',k);
end
displayFrame(handles,k);


% --- Executes during object creation, after setting all properties.
function frameSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frameSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in playButton.
function playButton_Callback(hObject, eventdata, handles)
% hObject    handle to playButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global stop;
stop = 0;

loop = get(handles.loopCheckbox,'Value');
framerate = str2double(get(handles.frameRateTextfield,'String'));

if(loop)
    for k = get(handles.frameSlider,'Value'):size(handles.frames,3) % once to the end
        if(~stop)
            displayFrame(handles,k);
            pause(1/framerate);
        end
    end
    while(~stop) % then looping
        for k = 1:size(handles.frames,3)
            if(~stop)
                displayFrame(handles,k);
                pause(1/framerate);
            end
        end
    end
else % play once from last frame
    if(get(handles.frameSlider,'Value') == size(handles.frames,3));
       set(handles.frameSlider,'Value',1); 
    end
    for k = get(handles.frameSlider,'Value'):size(handles.frames,3)
        if(~stop)
            displayFrame(handles,k);
            pause(1/framerate);
        end
    end    
end
guidata(hObject,handles);



function displayFrame(handles,k)
axes(handles.movieAxis);
imagesc(handles.frames(:,:,k));
if(get(handles.fixColormapCheckbox,'Value'))
    maxValue = max(max(handles.frames(:,:,1)));
    minValue = min(min(handles.frames(:,:,1)));   
    set(handles.movieAxis,'CLim',[minValue maxValue]);
end
axis image;
axis off;
colormap(handles.colormap);
set(handles.angleTextfield,'String',sprintf('%5.2f�',handles.angleArray(k)));
set(handles.frameSlider,'Value',k);




% --- Executes on button press in StopButton.
function StopButton_Callback(hObject, eventdata, handles)
% hObject    handle to StopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global stop;
stop = 1;
guidata(hObject,handles);


% --- Executes on button press in loopCheckbox.
function loopCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to loopCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% before = get(hObject,'Value');
%% set(hObject,'Value',~before);

% Hint: get(hObject,'Value') returns toggle state of loopCheckbox



function startAngleTextfield_Callback(hObject, eventdata, handles)
% hObject    handle to startAngleTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of startAngleTextfield as text
%        str2double(get(hObject,'String')) returns contents of startAngleTextfield as a double


% --- Executes during object creation, after setting all properties.
function startAngleTextfield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to startAngleTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function endAngleTextfield_Callback(hObject, eventdata, handles)
% hObject    handle to endAngleTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of endAngleTextfield as text
%        str2double(get(hObject,'String')) returns contents of endAngleTextfield as a double


% --- Executes during object creation, after setting all properties.
function endAngleTextfield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to endAngleTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function angleStepTextfield_Callback(hObject, eventdata, handles)
% hObject    handle to angleStepTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of angleStepTextfield as text
%        str2double(get(hObject,'String')) returns contents of angleStepTextfield as a double


% --- Executes during object creation, after setting all properties.
function angleStepTextfield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angleStepTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in sumRadioButton.
function sumRadioButton_Callback(hObject, eventdata, handles)
% hObject    handle to sumRadioButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

value = get(hObject,'Value');
set(handles.mipRadioButton,'Value',~value);

% Hint: get(hObject,'Value') returns toggle state of sumRadioButton


% --- Executes on button press in mipRadioButton.
function mipRadioButton_Callback(hObject, eventdata, handles)
% hObject    handle to mipRadioButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of mipRadioButton
value = get(hObject,'Value');
set(handles.sumRadioButton,'Value',~value);


% --- Executes on button press in precalculateButton.
function precalculateButton_Callback(hObject, eventdata, handles)
% hObject    handle to precalculateButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


set(handles.playButton,'Enable','off');
set(handles.StopButton,'Enable','off');
set(handles.loopCheckbox,'Enable','off');
set(handles.fixColormapCheckbox,'Enable','off');
set(handles.frameRateTextfield,'Enable','off');
set(handles.frameSlider,'Enable','off');
set(handles.saveAs,'Enable','off');

vol = handles.volume-min(handles.volume(:));
vol = single(vol); % single for speedup of the rotation.

startAngle = str2double(get(handles.startAngleTextfield,'String'));
endAngle = str2double(get(handles.endAngleTextfield,'String'));
angleStep = str2double(get(handles.angleStepTextfield,'String'));


rotationAxis = get(handles.rotationAxisPopup,'Value');
projectionAxis = get(handles.projectionAxisPopup,'Value');
if(get(handles.sumRadioButton,'Value'))
    projectionType = 1; 
elseif(get(handles.mipRadioButton,'Value'))
    projectionType = 2; 
end
counter = 1;
angleArray = startAngle:angleStep:endAngle;

handles.angleArray = angleArray;

hProgressbar = waitbar(0,'Calculating projections...'); 
clear frames;
for angle = angleArray;
    if(rotationAxis == 1)
        currentAngle = [angle 0 0];
    elseif(rotationAxis == 2)
        currentAngle = [0 angle 0];
    elseif(rotationAxis == 3)
        currentAngle = [0 0 angle];
    end
    frames(:,:,counter) = imresize(createProjectionImage(vol, currentAngle, projectionAxis, projectionType, 1, 1, 1, 0, 1, [], 0, 0, 0),str2double(get(handles.resizeFactorTextfield,'String')));
    counter = counter + 1;
    waitbar(counter / length(angleArray));
end
close(hProgressbar);
% frames = frames ./ max(frames(:));
handles.frames = frames;

set(handles.playButton,'Enable','on');
set(handles.StopButton,'Enable','on');
set(handles.loopCheckbox,'Enable','on');
set(handles.fixColormapCheckbox,'Enable','on');
set(handles.frameRateTextfield,'Enable','on');
set(handles.saveAs,'Enable','on');
set(handles.frameSlider,'Enable','on');
set(handles.frameSlider,'Min',1,'Max',length(angleArray),'Value',1,'SliderStep',[1/(length(angleArray)-1) 1/(length(angleArray)-1)*3]);

guidata(hObject,handles);


% --- Executes on selection change in rotationAxisPopup.
function rotationAxisPopup_Callback(hObject, eventdata, handles)
% hObject    handle to rotationAxisPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns rotationAxisPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from rotationAxisPopup


% --- Executes during object creation, after setting all properties.
function rotationAxisPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rotationAxisPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in projectionAxisPopup.
function projectionAxisPopup_Callback(hObject, eventdata, handles)
% hObject    handle to projectionAxisPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns projectionAxisPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from projectionAxisPopup


% --- Executes during object creation, after setting all properties.
function projectionAxisPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to projectionAxisPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function frameRateTextfield_Callback(hObject, eventdata, handles)
% hObject    handle to frameRateTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of frameRateTextfield as text
%        str2double(get(hObject,'String')) returns contents of frameRateTextfield as a double


% --- Executes during object creation, after setting all properties.
function frameRateTextfield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frameRateTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close moviePlayerFig.
function moviePlayerFig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to moviePlayerFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global stop;
stop = 1;

% Hint: delete(hObject) closes the figure
delete(hObject);


% --------------------------------------------------------------------
function fileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function saveAs_Callback(hObject, eventdata, handles)
% hObject    handle to saveAs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% convert the projections to frames
% maxValue = max(handles.frames(:));
% minValue = min(handles.frames(:));

[filename, pathname] = uiputfile('*.avi','Save movie as');

if(~isequal(filename,0) && ~isequal(pathname,0) && ~isempty(filename) && ~isempty(pathname))
   
    eval(['cmap = colormap(',handles.colormap,'(256));']);
    
    projs = double(handles.frames);
    
    if(get(handles.fixColormapCheckbox,'Value'))
        minValue = min(min(projs(:,:,1)));
        maxValue = max(max(projs(:,:,1)));
    else
        minValue = min(projs(:));
        maxValue = max(projs(:));
    end
    projs = projs - minValue;
    projs(projs<0) = 0;
    projs = projs ./ maxValue;
    projs = projs .* 255;
    projs(projs>255) = 255;
    projs = projs +1;
    
    writerObj = VideoWriter(fullfile(pathname,filename));
    writerObj.FrameRate = str2double(get(handles.frameRateTextfield,'String'));
    open(writerObj);
    for k = 1:size(handles.frames,3)
        f = im2frame(projs(:,:,k),cmap);
        writeVideo(writerObj,f);
    end
    
    close(writerObj);
end



% --------------------------------------------------------------------
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

close(handles.moviePlayerFig);


% --- Executes on button press in fixColormapCheckbox.
function fixColormapCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to fixColormapCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fixColormapCheckbox



function resizeFactorTextfield_Callback(hObject, eventdata, handles)
% hObject    handle to resizeFactorTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of resizeFactorTextfield as text
%        str2double(get(hObject,'String')) returns contents of resizeFactorTextfield as a double


% --- Executes during object creation, after setting all properties.
function resizeFactorTextfield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to resizeFactorTextfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function saveFramesAs_Callback(hObject, eventdata, handles)
% hObject    handle to saveFramesAs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


frames = handles.frames;
[filename, pathname] = uiputfile('*.mat','Save frames as');

if (~isequal(filename,0) && ~isequal(pathname,0) && ~isempty(filename) && ~isempty(pathname))
    save(fullfile(pathname,filename),'frames');
end