function varargout = isoSurfaceVis(varargin)
% ISOSURFACEVIS MATLAB code for isoSurfaceVis.fig
%      ISOSURFACEVIS, by itself, creates a new ISOSURFACEVIS or raises the existing
%      singleton*.
%
%      H = ISOSURFACEVIS returns the handle to a new ISOSURFACEVIS or the handle to
%      the existing singleton*.
%
%      ISOSURFACEVIS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ISOSURFACEVIS.M with the given input arguments.
%
%      ISOSURFACEVIS('Property','Value',...) creates a new ISOSURFACEVIS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before isoSurfaceVis_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to isoSurfaceVis_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help isoSurfaceVis

% Last Modified by GUIDE v2.5 06-Sep-2016 17:26:53

% Begin initialization code - DO NOT EDIT

gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @isoSurfaceVis_OpeningFcn, ...
                   'gui_OutputFcn',  @isoSurfaceVis_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT




% --- Executes just before isoSurfaceVis is made visible.
function isoSurfaceVis_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to isoSurfaceVis (see VARARGIN)

if (~isempty(varargin)),
    if(ndims(varargin{1})>2) %#ok<ISMAT>
        handles.volume = varargin{1};
    else
        return;
    end
end

handles.opacity = 1;
handles.currentColor = [1 0 0];
set(handles.colorChoserButton,'BackgroundColor',handles.currentColor);
set(handles.surfaceOpacitySlider,'Min',0,'Max',1,'Value',1);

% Choose default command line output for isoSurfaceVis
handles.output = hObject;

handles.fig = gcf;
set(handles.fig,'Renderer','opengl');

handles.currentThreshold = mean(handles.volume(:));
set(handles.thresholdTextbox,'String',num2str(mean(handles.volume(:))));
set(handles.complexitySlider,'Min',0.0,'Max',1,'Value',0.05);

[histValues, centers] = hist(handles.volume(:),256);
histHandles = bar(centers,histValues,'Parent',handles.histogramAxis);
set(histHandles,'FaceColor',[1 1 1]);
set(histHandles,'EdgeColor',[1 1 1]);
set(handles.histogramAxis,'Color',[0.2 0.2 0.2]);
set(handles.histogramAxis,'xtick',[]);
set(handles.histogramAxis,'xticklabel',[]);
set(handles.histogramAxis,'ytick',[]);
set(handles.histogramAxis,'yticklabel',[]);
set(handles.histogramAxis,'box','off');
axis tight;
axis off;

initialPos = mean(handles.volume(:));
handles.thresholdSlider = line([initialPos initialPos],[0 max(histValues)],'Color',[1 0 0],'LineWidth',1,'Tag','histogramSlider','Parent',handles.histogramAxis);
draggable(handles.thresholdSlider,'h',[centers(1) centers(end-1)],@lineMoved);


% Update handles structure
guidata(hObject, handles);




function lineMoved(lineHandle)

xPos = get(lineHandle,'XData');
handles = guidata(gcf);
handles.currentThreshold = xPos(1);
set(handles.thresholdTextbox,'String',num2str(xPos(1)));
guidata(handles.fig,handles);





% --- Outputs from this function are returned to the command line.
function varargout = isoSurfaceVis_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function drawIsoSurface(handles)

axes(handles.visAxis);
cla;
xLim = get(handles.visAxis,'xLim');
yLim = get(handles.visAxis,'yLim');
textHandle = text((xLim(1)+(xLim(2)-xLim(1)))/2,(yLim(1)+(yLim(2)-yLim(1)))/2,'Rendering iso surface View. Stand by...','Color',[1 1 1],'Parent',handles.visAxis,'HorizontalAlignment','center');
drawnow;
t = handles.currentThreshold;
[X, Y, Z] = meshgrid(1:size(handles.volume,2),1:size(handles.volume,1),1:size(handles.volume,3));
fv = isosurface(X,Y,Z,handles.volume,t);
fv = reducepatch(fv,get(handles.complexitySlider,'Value'));
handles.patchHandle = patch(fv);
isonormals(X,Y,Z,handles.volume,handles.patchHandle);
set(handles.patchHandle,'FaceColor',handles.currentColor);
set(handles.patchHandle,'FaceAlpha',get(handles.surfaceOpacitySlider,'Value'));
set(handles.patchHandle,'EdgeColor','none');
daspect([1,1,1]);
view(3); 
set(handles.visAxis,'XLim',[1 size(handles.volume,2)]);
set(handles.visAxis,'YLim',[1 size(handles.volume,1)]);
set(handles.visAxis,'ZLim',[1 size(handles.volume,3)]);
axis off;
camlight;
lighting gouraud;
delete(textHandle);
guidata(handles.fig,handles);




function thresholdTextbox_Callback(hObject, eventdata, handles)
% hObject    handle to thresholdTextbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


handles.currentThreshold = str2double(get(hObject,'String'));
set(handles.thresholdSlider,'XData',[handles.currentThreshold handles.currentThreshold]);




% --- Executes during object creation, after setting all properties.
function thresholdTextbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thresholdTextbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in colorChoserButton.
function colorChoserButton_Callback(hObject, ~, handles)
% hObject    handle to colorChoserButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

c = uisetcolor(handles.currentColor);
set(handles.colorChoserButton,'BackgroundColor',c);
handles.currentColor = c;
try 
    set(handles.patchHandle,'FaceColor',handles.currentColor);
catch
    % not set yet...
end
guidata(hObject,handles);



% --- Executes when user attempts to close isoSurfaceVisFig.
function isoSurfaceVisFig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to isoSurfaceVisFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --------------------------------------------------------------------
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


close(handles.fig);


% --------------------------------------------------------------------
function fileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function saveAs_Callback(hObject, eventdata, handles)
% hObject    handle to saveAs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function surfaceOpacitySlider_Callback(hObject, eventdata, handles)
% hObject    handle to surfaceOpacitySlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.patchHandle,'FaceAlpha',get(hObject,'Value'));


% --- Executes during object creation, after setting all properties.
function surfaceOpacitySlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to surfaceOpacitySlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes on button press in rotate3DButton.
function rotate3DButton_Callback(hObject, eventdata, handles)

if(get(hObject,'Value') == 1)
    rotate3d(handles.visAxis,'on');
    set(hObject,'BackgroundColor',[0.3 0.5 1]);  % consistent with MiniViewer
    set(handles.thresholdTextbox,'Enable','off');
    set(handles.complexitySlider,'Enable','off');
    set(handles.surfaceOpacitySlider,'Enable','off');
    set(handles.colorChoserButton,'Enable','off');
    set(handles.renderButton,'Enable','off');
else
    rotate3d(handles.visAxis,'off');
    set(hObject,'BackgroundColor',[.9 .9 .9]);
    set(handles.thresholdTextbox,'Enable','on');
    set(handles.complexitySlider,'Enable','on');
    set(handles.surfaceOpacitySlider,'Enable','on');
    set(handles.colorChoserButton,'Enable','on');
    set(handles.renderButton,'Enable','on');
end


% --- Executes on slider movement.
function complexitySlider_Callback(hObject, eventdata, handles)
% hObject    handle to complexitySlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes during object creation, after setting all properties.
function complexitySlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to complexitySlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in renderButton.
function renderButton_Callback(hObject, eventdata, handles)
% hObject    handle to renderButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

drawIsoSurface(handles);
