function [projImage] = createProjectionImage(volImg, angle, projAxis, type, rotInterpolMethod, projInterpolMethod, projType, weighted, crop, param, performCut, adaptImageSize, output)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% projections. note: if rotation is done, the size of the image increases!
% description
% -------------------------------------------------------------------------
% produces a projection image over a specified axis after rotating the
% volume by a specified angle. creates summation or max Intensity
% For perspective projection the image size also increases!
%
% parameters:
% -------------------------------------------------------------------------
%   - volImg:           image volume data
%   - angle:            vector with angle for x, y, z rotation: [x y z].
%                       Rotation is done in order: 1) x, 2) y, 3) z
%   - projAxis:         specifies the axis over which the projection is
%                       done. 1 = x, 2 = y, 3 = z.
%   - type:             specifies nature of projection.
%                       1 = summation, 2 = max intensity, 3 = X-ray like
%                       projection
%                       (ATTENTION: X-ray like projection needs segmented volume
%                       with values 0, 1, 2, 3 representing 0 = air, 1 = gland, 2 = fat, 3 = muscle!)
%   - rotInterpolmethod:interpolation method for rotations. 1 = bilinear (default), 2 = nearest neighbour
%   - projInterpolMethod:interpolation method for the perspective projection (interpolation). 1 = linear (default), 2 = nearest neighbout, 3 = cubic
%   - projType:         1 = parallel (default), 2 = perspective
%   - weighted:         specifies if image is weighted by number of pixels
%                       hit during projection.
%                       0 = weighting disabled (default), 1 = weighting enabled
%   - crop:             0 = increase image size for rotation (default), 1 = crop image after rotation
%   - param:            struct of additional parameters
%                       * for perspective projection:
%                           param.sourcePos = position of the x-ray source                            
%                           param.resMRT = resolution of MRI
%                           param.detectorCenterPoint = center point of the detector in space
%                           param.detectorNormal = normal vector of the detector 
%                       * for X-ray like projection:
%                           param.classes.fat = intensity of the class for fatty tissue
%                           param.classes.gland = intensity of the class for glandular tissue
%                           param.classes.muscle = intensity of the class for muscle tissue
%   - performCut        0 = do not cut the volume. 1 = cut the volume to
%                           the image containing part in projection
%                           direction, e.g. the breast in the image, remove
%                           background before and after the volume
%   - adaptImageSize    0 = do not increase projection image size when
%                           doing a perspective projection, 1 = increase
%                           the projection image size
%   - output:           0 = do not display output, 1 = display information
%                       about the method
%
%
% return values:
% -------------------------------------------------------------------------
%   - projImage:        projection image as 2D matrix
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: t.hopp
% created: may 2010
% last change: jul 2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% parameter validation
if(isempty(rotInterpolMethod))
    interpolmethod = 1;
else
    interpolmethod = rotInterpolMethod;
end

if(isempty(projInterpolMethod))
   pInterp = 'linear'; 
else
   if(projInterpolMethod == 1)
       pInterp = 'linear'; 
   elseif(projInterpolMethod == 2)
       pInterp = 'nearest'; 
   elseif(projInterpolMethod == 3)
       pInterp = 'cubic'; 
   end
end

if(isempty(projType))
    projType = 1;
end

if(isempty(weighted))
    weighted = 0;
end

if(isempty(crop))
    crop = 0;
end




%%% commented param checks for speedup
% if(projType == 2 && (isempty(param) || isempty(param.dist)))
%     error('no source distance given for perspective projection');
% end
% 
% if(type == 3 && ~isempty(find(volImg > 3, 1)))
%     error('For X-ray like projection only a classified image with values 0,1,2,3 allowed!');
% end
% 
% if(projAxis < 1 || projAxis > 3)
%     error('projection axis has to be 1 (=x), 2 (=y), or 3 (=z)');
% end


if(output)
    fprintf('**** PARAMETER SUMMARY ****************************************\n');
    fprintf('  *  rotation:          %5.2f� (x-Axis),    %5.2f� (y-Axis),    %5.2f� (z-Axis)\n',angle(1),angle(2),angle(3));
    if(interpolmethod == 1)
        fprintf('  *  rot. interpol.:    linear\n');
    elseif(interpolmethod == 2)
        fprintf('  *  rot. interpol.:    nearest neighbour\n');
    end
    fprintf('  *  proj. interpol.:   %s\n',pInterp);
    if(crop == 1)
        fprintf('  *  rot. cropping:     yes\n');
    else
        fprintf('  *  rot. cropping:     no\n');
    end
    if(projAxis == 1)
        fprintf('  *  projection axis:   x\n');
    elseif(projAxis == 2)
        fprintf('  *  projection axis:   y\n');
    elseif(projAxis == 3)
        fprintf('  *  projection axis:   z\n');
    end
    if(type == 1)
        fprintf('  *  nature:            summation\n');
    elseif(type == 2)
        fprintf('  *  nature:            MIP\n');
    elseif(type == 3)
        fprintf('  *  nature:            X-ray-like\n');
    end
    if(projType == 1)
        fprintf('  *  projection type:   parallel\n');
    elseif(projType == 2)
        fprintf('  *  projection type:   perspective from given source\n');
    end
    if(weighted == 1)
        fprintf('  *  weighted:          yes\n');
    else
        fprintf('  *  weighted:          no\n');
    end
    if(performCut == 1)
        fprintf('  *  perform cut:       yes\n');
    else
        fprintf('  *  perform cut:       no\n');
    end
    if(adaptImageSize == 1)
        fprintf('  *  adapt image size:  yes\n');
    else
        fprintf('  *  adapt image size:  no\n');
    end
    
    fprintf('************************************************************\n');
end



%% Initialization for X-ray like projection
if(type == 3)
    keV = 31000;
    attenuationTermFat = 0.250; % Hubbel/Seltzer: 0.3063;
    attenuationTermGland = 0.372; % Hubbel/Seltzer: 0.3403;
    attenuationTermMuscle = 0.380; % Hubbel/Seltzer: 3.783E-01;
    
    densityFat = 9.500E-01;
    densityGland = 1.020E+00;
    densityMuscle =  1.050E+00;
    
    resMRT = param.resMRT;
    
    if(projType == 2) % prepare structure to pass to projection function
        p.resMRT = resMRT;
        p.keV = keV;
        p.attenuationTermFat = attenuationTermFat;
        p.attenuationTermGland = attenuationTermGland;
        p.attenuationTermMuscle = attenuationTermMuscle;
        p.densityFat = densityFat;
        p.densityGland = densityGland;
        p.densityMuscle = densityMuscle;
        
    end
    
    
    if(length(size(volImg)) == 3)  % assuming a classified image with only class values
        volImg(volImg == param.classes.fat) = -attenuationTermFat .* densityFat;
        volImg(volImg == param.classes.gland) = -attenuationTermGland .* densityGland;
        volImg(volImg == param.classes.muscle) = -attenuationTermMuscle .* densityMuscle;
    elseif(length(size(volImg)) == 4) % assuming a probability distribution for each voxel with 4th dimension: the classes
        volImgAtt = (volImg(:,:,:,param.classes.fat) .* (-attenuationTermFat .* densityFat)) + (volImg(:,:,:,param.classes.gland) .* (-attenuationTermGland .* densityGland)) + (volImg(:,:,:,param.classes.muscle) .* (-attenuationTermMuscle .* densityMuscle));
        volImg = squeeze(volImgAtt);
    end
    
end


%% rotate volume by given angles
if(angle(1) ~= 0)
    if(output)
        fprintf('  rotating around x by %5.2f�\n',angle(1));
    end
    volImg = rotateVolume(volImg, 1, angle(1), interpolmethod, crop);
end
if(angle(2) ~= 0)
    if(output)
        fprintf('  rotating around y by %5.2f�\n',angle(2));
    end
    volImg = rotateVolume(volImg, 2, angle(2), interpolmethod, crop);
end
if(angle(3) ~= 0)
    if(output)
        fprintf('  rotating around z by %5.2f�\n',angle(3));
    end
    volImg = rotateVolume(volImg, 3, angle(3), interpolmethod, crop);
end
% mask = volImg > 0;



%% Initialization for perspective projection
if(projType == 2)
    
    p.pInterp = pInterp;
    
    if(performCut)
        cut = 1;
        for y = 1:size(volImg,projAxis)
            if(projAxis == 1)
                slice2 = squeeze(volImg(y,:,:));
            elseif(projAxis == 2)
                slice2 = squeeze(volImg(:,y,:));
            elseif(projAxis == 3)
                slice2 = squeeze(volImg(:,:,y));
            end
            if(~isempty(find(slice2 > 0,1)))
                cut = y;
                break;
            end
        end
        cut2 = size(volImg,projAxis);
        for y = size(volImg,projAxis):-1:1
            if(projAxis == 1)
                slice2 = squeeze(volImg(y,:,:));
            elseif(projAxis == 2)
                slice2 = squeeze(volImg(:,y,:));
            elseif(projAxis == 3)
                slice2 = squeeze(volImg(:,:,y));
            end
            if(~isempty(find(slice2 > 0,1)))
                cut2 = y;
                break;
            end
        end
        
        
        if(projAxis == 1)
            volImg = volImg(cut:cut2,:,:);
        elseif(projAxis == 2)
            volImg = volImg(:,cut:cut2,:);
        elseif(projAxis == 3)
            volImg = volImg(:,:,cut:cut2);
        end
    end
    
    if(projAxis == 1)
        sourcePos = param.sourcePos;  %[param.dist round(size(volImg,2)/2)  round(size(volImg,3)/2)];
        projImageSize = [size(volImg,2),size(volImg,3)];
        
        if(adaptImageSize)
            % prepare the initial 2D image --> increase the image size according
            % to the geometry of volume and source point
            P1 = [size(volImg,1) sourcePos(2) 1];
            P2 = [size(volImg,1) 1 sourcePos(3)];
            
            c = sourcePos(3);
            a = sqrt((sourcePos(1)-P1(1)).^2 + (sourcePos(2)-P1(2)).^2 + (sourcePos(3)-P1(3)).^2);
            alpha = acosd(1-((c^2)/(2*a^2)));
            shiftZ = ceil(size(volImg,1) * tand(alpha)); % size increase in z on one side --> ceil for maximum. (aka shift for everything, times to for new image size)
            
            c = sourcePos(2);
            a = sqrt((sourcePos(1)-P2(1)).^2 + (sourcePos(2)-P2(2)).^2 + (sourcePos(3)-P2(3)).^2);
            alpha = acos(1-((c^2)/(2*a^2)));
            shiftY = ceil(size(volImg,1) * tan(alpha));  % size increase in y on one side --> ceil for maximum. (aka shift for everything, times to for new image size)
            
            projImageSize = [size(volImg,2)+(shiftY*2) size(volImg,3)+(shiftZ*2)];
            sourcePos = sourcePos + [0 shiftY shiftZ];
            % shift volume
            mriVolTemp = zeros([size(volImg,1) size(volImg,2)+(shiftY*2) size(volImg,3)+(shiftZ*2)]);
            mriVolTemp(:,shiftY:shiftY+size(volImg,2)-1,shiftZ:shiftZ+size(volImg,3)-1) = volImg;
            volImg = mriVolTemp;
            clear mriVolTemp;
        end
    elseif(projAxis == 2)
        sourcePos = param.sourcePos; %[round(size(volImg,1)/2) param.dist round(size(volImg,3)/2)];
        projImageSize = [size(volImg,1),size(volImg,3)];
        
        if(adaptImageSize)
            % prepare the initial 2D image --> increase the image size according
            % to the geometry of volume and source point
            P1 = [sourcePos(1) size(volImg,2) 1];
            P2 = [1 size(volImg,2) sourcePos(3)];
            
            c = sourcePos(3);
            a = sqrt((sourcePos(1)-P1(1)).^2 + (sourcePos(2)-P1(2)).^2 + (sourcePos(3)-P1(3)).^2);
            alpha = acosd(1-((c^2)/(2*a^2)));
            shiftZ = ceil(size(volImg,2) * tand(alpha)); % size increase in z on one side --> ceil for maximum. (aka shift for everything, times to for new image size)
            
            c = sourcePos(1);
            a = sqrt((sourcePos(1)-P2(1)).^2 + (sourcePos(2)-P2(2)).^2 + (sourcePos(3)-P2(3)).^2);
            alpha = acos(1-((c^2)/(2*a^2)));
            shiftX = ceil(size(volImg,2) * tan(alpha));  % size increase in y on one side --> ceil for maximum. (aka shift for everything, times to for new image size)
            
            projImageSize = [size(volImg,1)+(shiftX*2) size(volImg,3)+(shiftZ*2)];
            sourcePos = sourcePos + [shiftX 0 shiftZ];
            % shift volume
            mriVolTemp = zeros([size(volImg,1)+(shiftX*2) size(volImg,2) size(volImg,3)+(shiftZ*2)]);
            mriVolTemp(shiftX:shiftX+size(volImg,1)-1,:,shiftZ:shiftZ+size(volImg,3)-1) = volImg;
            volImg = mriVolTemp;
            clear mriVolTemp;
        end
    elseif(projAxis == 3)
        sourcePos = param.sourcePos; % [round(size(volImg,1)/2) round(size(volImg,2)/2) param.dist];
        projImageSize = [size(volImg,1),size(volImg,2)];
        if(adaptImageSize)
            % prepare the initial 2D image --> increase the image size according
            % to the geometry of volume and source point
            P1 = [sourcePos(1) 1 size(volImg,3)];
            P2 = [1 sourcePos(2) size(volImg,3)];
            
            c = sourcePos(2);
            a = sqrt((sourcePos(1)-P1(1)).^2 + (sourcePos(2)-P1(2)).^2 + (sourcePos(3)-P1(3)).^2);
            alpha = acosd(1-((c^2)/(2*a^2)));
            shiftY = ceil(size(volImg,3) * tand(alpha)); % size increase in z on one side --> ceil for maximum. (aka shift for everything, times to for new image size)
            
            c = sourcePos(1);
            a = sqrt((sourcePos(1)-P2(1)).^2 + (sourcePos(2)-P2(2)).^2 + (sourcePos(3)-P2(3)).^2);
            alpha = acos(1-((c^2)/(2*a^2)));
            shiftX = ceil(size(volImg,3) * tan(alpha));  % size increase in y on one side --> ceil for maximum. (aka shift for everything, times to for new image size)
            
            projImageSize = [size(volImg,1)+(shiftX*2) size(volImg,2)+(shiftY*2)];
            sourcePos = sourcePos + [shiftX shiftY 0];
            % shift volume
            mriVolTemp = zeros([size(volImg,1)+(shiftX*2) size(volImg,2)+(shiftY*2) size(volImg,3)]);
            mriVolTemp(shiftX:shiftX+size(volImg,1)-1,shiftY:shiftY+size(volImg,2)-1,:) = volImg;
            volImg = mriVolTemp;
            clear mriVolTemp;
        end
        
        
    end
    %     weightImage = zeros(size(projImage));
    %     projImageSize = size(size(volImg,1),size(volImg,2));
    
    % Prepare interpolation
%     [X2, Y2, Z2] = meshgridvectors(volImg);
%     V = permute(volImg,[2 1 3]');
%     F = griddedInterpolant({X2,Y2,Z2},V,pInterp);
    
    
    
        F = [];
    
end



%%% NEW VECTORIZED IMPLEMENTATION %%%%%%%%%%%%%

if(projAxis == 1) % projection over x
    if(type == 1) % summation
        if(projType == 1) % parallel
            projImage = squeeze(sum(volImg,1));
            weightImage = squeeze(sum(volImg>0,1));
        elseif(projType == 2) % perspective
            [y,z] = meshgrid(1:size(volImg,2), 1:size(volImg,3));
            [projImage, weightImage] = projectPixel(volImg, sourcePos, ones(size(y(:))), y(:), z(:), type, p, F, projAxis);
            projImage = reshape(projImage,projImageSize(2),projImageSize(1))';
            weightImage = reshape(weightImage,projImageSize(2),projImageSize(1))';
        end
    elseif(type == 2) % MIP
        if(projType == 1) % parallel
            projImage = squeeze(max(volImg,[],1));
            weightImage = squeeze(sum(volImg>0,1));
        elseif(projType == 2) % perspective
            [y,z] = meshgrid(1:size(volImg,2), 1:size(volImg,3));
            [projImage, weightImage] = projectPixel(volImg, sourcePos, ones(size(y(:))), y(:), z(:), type, p, F, projAxis);
            projImage = reshape(projImage,projImageSize(2),projImageSize(1))';
            weightImage = reshape(weightImage,projImageSize(2),projImageSize(1))';
        end
    elseif(type == 3) % X-ray like
        if(projType == 1) % parallel
            projImage = squeeze(-log(keV .* exp((sum(volImg,1).*resMRT))));
%             numPxFat = squeeze(sum(volImg == 2,1));
%             numPxGland = squeeze(sum(volImg == 1,1));
%             numPxMuscle = squeeze(sum(volImg == 3,1));
%             projImage = -log(keV .* exp((-attenuationTermFat .* densityFat .* (numPxFat.*resMRT)) - (attenuationTermGland .* densityGland .* (numPxGland.*resMRT)) - (attenuationTermMuscle .* densityMuscle .* (numPxMuscle.*resMRT))));
            weightImage = squeeze(sum(volImg>0,1));
        elseif(projType == 2) % perspective
            [y,z] = meshgrid(1:size(volImg,2), 1:size(volImg,3));
            [projImage, weightImage] = projectPixel(volImg, sourcePos, ones(size(y(:))), y(:), z(:), type, p, F, projAxis);
            projImage = reshape(projImage,projImageSize(2),projImageSize(1))';
            weightImage = reshape(weightImage,projImageSize(2),projImageSize(1))';
        end
    end
elseif(projAxis == 2) % projection over x
    if(type == 1) % summation
        if(projType == 1) % parallel
            projImage = squeeze(sum(volImg,2));
            weightImage = squeeze(sum(volImg>0,2));
        elseif(projType == 2) % perspective
            [x,z] = meshgrid(1:size(volImg,1), 1:size(volImg,3));
            [projImage, weightImage] = projectPixel(volImg, sourcePos, x(:), ones(size(x(:))), z(:), type, p, F, projAxis);
            projImage = reshape(projImage,projImageSize(2),projImageSize(1))';
            weightImage = reshape(weightImage,projImageSize(2),projImageSize(1))';
        end
    elseif(type == 2) % MIP
        if(projType == 1) % parallel
            projImage = squeeze(max(volImg,[],2));
            weightImage = squeeze(sum(volImg>0,2));
        elseif(projType == 2) % perspective
            [x,z] = meshgrid(1:size(volImg,1), 1:size(volImg,3));
            [projImage, weightImage] = projectPixel(volImg, sourcePos, x(:), ones(size(x(:))), z(:), type, p, F, projAxis);
            projImage = reshape(projImage,projImageSize(2),projImageSize(1))';
            weightImage = reshape(weightImage,projImageSize(2),projImageSize(1))';
        end
    elseif(type == 3) % X-ray like
        if(projType == 1) % parallel
            projImage = squeeze(-log(keV .* exp((sum(volImg,2).*resMRT))));
%             numPxFat = squeeze(sum(volImg == 2,2));
%             numPxGland = squeeze(sum(volImg == 1,2));
%             numPxMuscle = squeeze(sum(volImg == 3,2));
%             projImage = -log(keV .* exp((-attenuationTermFat .* densityFat .* (numPxFat.*resMRT)) - (attenuationTermGland .* densityGland .* (numPxGland.*resMRT)) - (attenuationTermMuscle .* densityMuscle .* (numPxMuscle.*resMRT))));
            weightImage = squeeze(sum(volImg>0,2));
        elseif(projType == 2) % perspective
            [x,z] = meshgrid(1:size(volImg,1), 1:size(volImg,3));
            [projImage, weightImage] = projectPixel(volImg, sourcePos, x(:), ones(size(x(:))), z(:), type, p, F, projAxis);
            projImage = reshape(projImage,projImageSize(2),projImageSize(1))';
            weightImage = reshape(weightImage,projImageSize(2),projImageSize(1))';
        end
    end
elseif(projAxis == 3) % projection over x
    if(type == 1) % summation
        if(projType == 1) % parallel
            projImage = squeeze(sum(volImg,3));
            weightImage = squeeze(sum(volImg>0,3));
        elseif(projType == 2) % perspective
            [x,y] = meshgrid(1:size(volImg,1), 1:size(volImg,2));
            [projImage, weightImage] = projectPixel(volImg, sourcePos, x(:), y(:), ones(size(x(:))), type, p, F, projAxis);
            projImage = reshape(projImage,projImageSize(2),projImageSize(1))';
            weightImage = reshape(weightImage,projImageSize(2),projImageSize(1))';
        end
    elseif(type == 2) % MIP
        if(projType == 1) % parallel
            projImage = squeeze(max(volImg,[],3));
            weightImage = squeeze(sum(volImg>0,3));
        elseif(projType == 2) % perspective
            [x,y] = meshgrid(1:size(volImg,1), 1:size(volImg,2));
            [projImage, weightImage] = projectPixel(volImg, sourcePos, x(:), y(:), ones(size(x(:))), type, p, F, projAxis);
            projImage = reshape(projImage,projImageSize(2),projImageSize(1))';
            weightImage = reshape(weightImage,projImageSize(2),projImageSize(1))';
        end
    elseif(type == 3) % X-ray like
        if(projType == 1) % parallel
            projImage = squeeze(-log(keV .* exp((sum(volImg,3).*resMRT))));
            weightImage = squeeze(sum(volImg>0,3));
        elseif(projType == 2) % perspective
            [x,y] = meshgrid(1:size(volImg,1), 1:size(volImg,2));
            [projImage, weightImage] = projectPixel(volImg, sourcePos, x(:), y(:), ones(size(x(:))), type, p, F, projAxis);
            projImage = reshape(projImage,projImageSize(2),projImageSize(1))';
            weightImage = reshape(weightImage,projImageSize(2),projImageSize(1))';
        end
    end
end



if(type == 3)
    projImage = projImage - min(projImage(:));
end

if(weighted == 1)
    projImage = projImage ./ weightImage;
end
projImage(isnan(projImage)) = 0;
projImage(~isfinite(projImage)) =0;



end


function rMat = getRotationMatrix3D(axis,angle)
    if(axis == 1)
       rMat = [ 1 0 0 0; ...
                0 cosd(angle) -sind(angle) 0;...
                0 sind(angle) cosd(angle) 0; ...
                0 0 0 1]; 
    elseif(axis == 2)
       rMat = [ cosd(angle) 0 sind(angle) 0; ...
                0 1 0 0;...
                -sind(angle) 0 cosd(angle) 0; ...
                0 0 0 1 ]; 
    elseif(axis == 3)
        rMat = [ cosd(angle) -sind(angle) 0 0; ...
                sind(angle) cosd(angle) 0 0;...
                0 0 1 0; ...
                0 0 0 1];
    end
end


%% helper function: rotates the volume with given axis, angle, interpolation method and cropping parameter
function vol =  rotateVolume(vol, axis, angle, interpolmethod, crop)

if(axis == 3)
    if(interpolmethod == 1)
        if(crop == 1)
            vol = imrotate(vol, angle, 'bilinear','crop');
        else
            vol = imrotate(vol, angle, 'bilinear');
        end
    elseif(interpolmethod == 2)
        if(crop == 1)
            vol = imrotate(vol, angle, 'nearest','crop');
        else
            vol = imrotate(vol, angle, 'nearest');
        end
    end
elseif(axis == 2)
    vol = permute(vol,[1 3 2]);
    if(interpolmethod == 1)
        if(crop == 1)
            vol = imrotate(vol, angle, 'bilinear','crop');
        else
            vol = imrotate(vol, angle, 'bilinear');
        end
    elseif(interpolmethod == 2)
        if(crop == 1)
            vol = imrotate(vol, angle, 'nearest','crop');
        else
            vol = imrotate(vol, angle, 'nearest');
        end
    end
    vol = ipermute(vol,[1 3 2]);
elseif(axis == 1)
    vol = permute(vol,[3 2 1]);
    if(interpolmethod == 1)
        if(crop == 1)
            vol = imrotate(vol, angle, 'bilinear','crop');
        else
            vol = imrotate(vol, angle, 'bilinear');
        end
    elseif(interpolmethod == 2)
        if(crop == 1)
            vol = imrotate(vol, angle, 'nearest','crop');
        else
            vol = imrotate(vol, angle, 'nearest');
        end
    end
    vol = ipermute(vol,[3 2 1]);
end


end



function [val, weightImage] = projectPixel(a, sourcePos, xPos, yPos, zPos, method, param, F, projAxis)

pixelPos = [xPos yPos zPos];

% "Punkt-Richtungsform"
pVec = sourcePos;
rVec = [pixelPos(:,1) - sourcePos(1), pixelPos(:,2) - sourcePos(2), pixelPos(:,3) - sourcePos(3)];

if(projAxis == 1)
    X = 1:size(a,1);
    numPlanes = size(X,2);
    temp = repmat(X'-pVec(:,1),size(rVec,1),1);
    [p,q] = meshgrid(1:size(rVec,1), X);
    rVec = rVec(p(:),:);
    kVec = temp ./ rVec(:,1);
    Y = pVec(:,2) + (kVec .* rVec(:,2));
    Z = pVec(:,3) + (kVec .* rVec(:,3));
    % correction for the perspective through a voxel -> distance the ray
    % travels through a voxel
    dist = sqrt((xPos-X(end)).^2 + (yPos-Y(size(X,2):size(X,2):end)).^2 + (zPos-Z(size(X,2):size(X,2):end)).^2);
    sizePerVoxel = (dist ./ size(X,2))';
    X = q(:);
elseif(projAxis == 2)
    Y = 1:size(a,2);
    numPlanes = size(Y,2);
    temp = repmat(Y'-pVec(:,2),size(rVec,1),1);
    [p,q] = meshgrid(1:size(rVec,1), Y);
    rVec = rVec(p(:),:);
    kVec = temp ./ rVec(:,2);
    X = pVec(:,1) + (kVec .* rVec(:,1));
    Z = pVec(:,3) + (kVec .* rVec(:,3));
    % correction for the perspective through a voxel -> distance the ray
    % travels through a voxel
    dist = sqrt((xPos-X(size(Y,2):size(Y,2):end)).^2 + (yPos-Y(end)).^2 + (zPos-Z(size(Y,2):size(Y,2):end)).^2);
    sizePerVoxel = (dist ./ size(Y,2))';
    Y = q(:);
elseif(projAxis == 3)
    Z = 1:size(a,3);
    numPlanes = size(Z,2);
    temp = repmat(Z'-pVec(:,3),size(rVec,1),1);
    [p,q] = meshgrid(1:size(rVec,1), Z);
    rVec = rVec(p(:),:);
    kVec = temp ./ rVec(:,3);
    X = pVec(:,1) + (kVec .* rVec(:,1));
    Y = pVec(:,2) + (kVec .* rVec(:,2));
    % correction for the perspective through a voxel -> distance the ray
    % travels through a voxel
    dist = sqrt((xPos-X(size(Z,2):size(Z,2):end)).^2 + (yPos-Y(size(Z,2):size(Z,2):end)).^2 + (zPos-Z(end)).^2);
    sizePerVoxel = (dist ./ size(Z,2))';
    Z = q(:);
end



% fast 3D interpolation from MATLAB file exchange
% values = mirt3D_mexinterp(a,Y,X,Z); % only linear!

% fast 3D interpolation using mexed function from file exchange  (see http://www.mathworks.com/matlabcentral/fileexchange/21702-3d-volume-interpolation-with-ba-interp3--fast-interp3-replacement)

if(strcmp(param.pInterp,'linear'))
    values = mirt3D_mexinterp(double(a),Y,X,Z);
else
    values = ba_interp3(double(a),Y,X,Z,param.pInterp);
end

% tic;
% values = interp3(double(a),Y,X,Z,param.pInterp);
% toc;


% values = F(Y,X,Z);
values = reshape(values,numPlanes,[]);

if(method == 1)
    val = sum(values);
    weightImage = sum(values>0);
elseif(method == 2)
    val = max(values);
    weightImage = sum(values>0);
elseif(method == 3)
    weightImage = sum(values>0);
    val = -log(param.keV .* exp(sum(values) .* param.resMRT .* sizePerVoxel));  % summing up the attenuation coefficients - no need to count, allows other than nearest neighbour interpolation!
    
end


end


