function rotationmatrix = get3DRotationMatrix(d, dim) 
%% Get 3D rotation matrix form a predefined set
%
% *Description*
%
% Returns a 3D rotation matrix out of a set of matrices, depending on the
% dimension of rotation (axis) and a given angle.
%
%
% *Input*
%
% * |d|: angle (double)
% * |dim|: dimension (int)
%
%
% *Output*
%
% * |rotationmatrix|: precalculated 3D rotation matrix
%
%
% *Example*
%
%  d = 0.1082;
%  dim = 3;
%  rotationmatrix = get3DRotationMatrix(d, dim)
%
%


% If the variables inside the method are empty (do not exist) we should get an error
if ~exist('d', 'var') || isempty(d) || ~exist('dim', 'var') || isempty(dim)
    errorStruct.message = 'dimension or angle has no value !!';
    errorStruct.identifier = 'get3DRotationMatrix:IsEmpty';
    error(errorStruct);
end

% The variables should not be arrays, so an error will be received
if length(dim) > 1 || length(d) > 1
    errorStruct.message = 'Enter a Number!';
        errorStruct.identifier = 'get3DRotationMatrix:InputOutOfBounds';
        error(errorStruct);  
end

% If variables are not given as numbers, we should get an error
if ~isnumeric(dim)
    errorStruct.message = 'Enter a Number!';
    errorStruct.identifier = 'get3DRotationMatrix:InputMustBeNumeric';
    error(errorStruct);
end


if ~isnumeric(d)
    errorStruct.message = 'Enter a Number!';
    errorStruct.identifier = 'get3DRotationMatrix:InputMustBeNumeric';
    error(errorStruct);
end

% The rotation matrix method has two entries, angle(d) and dimension(dim = must be between 1-3)
% That will get an error if they have wrong entries
switch dim
    case 3
        rotationmatrix = [  cos(d)  -sin(d)    0            0;  ...
                            sin(d)   cos(d)    0            0;  ...
                            0         0          1          0;  ...
                            0         0          0          1];
    case 2
        rotationmatrix = [  cos(d)   0          sin(d)      0;  ...
                            0         1          0          0;  ...
                            -sin(d)   0          cos(d)     0;  ...
                            0         0          0          1];
    case 1
        rotationmatrix = [  1         0          0            0;  ...
                            0         cos(d)   -sin(d)        0;  ...
                            0         sin(d)    cos(d)        0;  ...
                            0         0          0            1];
    otherwise
        error('get3DRotationMatrix:InputOutOfBounds', 'Input Must Be(1,2 oder 3): %d' ,dim);
end
end
