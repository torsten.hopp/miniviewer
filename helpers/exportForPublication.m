function exportForPublication(externalHandles,stateSwapXY)

% Initial options.
handles.xLabel = 'x'; 
handles.yLabel = 'y'; 
handles.zLabel = 'z'; 
handles.quantityUnit = ' ';

handles.titleFontName = 'Times';
handles.titleFontSize = 12;
handles.titleFontWeight = 'normal';

handles.axisLabelFontName = 'Times';
handles.axisLabelFontSize = 12;
handles.axisLabelFontWeight = 'normal';

handles.axisFontName = 'Times';
handles.axisFontSize = 12;
handles.axisFontWeight = 'normal';

handles.pageWidth = 21;
handles.pageHeight = (21/3);

handles.displayColobar = 1;
handles.displayTitle = 1;

handles.sizeList = {'4','5','6','7','8','9','10','11','12','14','16','18','20','24','28','32','40','48'};
handles.fontList = {'Arial','Times','Courier'};

handles.border = 0.05;



%%%%%%% gathering data from current miniViewer instance
handles.aspects = [get(externalHandles.axesX,'XLim')/get(externalHandles.axesX,'YLim'), get(externalHandles.axesY,'XLim')/get(externalHandles.axesY,'YLim'), get(externalHandles.axesZ,'XLim')/get(externalHandles.axesZ,'YLim')];

handles.currentXSlice = get(externalHandles.xSliceSlider,'Value');
if(externalHandles.startpointAvailable && externalHandles.imageResolutionAvailable)
    handles.currentXSlice =  externalHandles.imageStartpoint(1) + ((handles.currentXSlice - 1) .* externalHandles.imageResolution(1));
end

handles.currentYSlice = get(externalHandles.ySliceSlider,'Value');
if(externalHandles.startpointAvailable && externalHandles.imageResolutionAvailable)
    handles.currentYSlice =  externalHandles.imageStartpoint(2) + ((handles.currentYSlice - 1) .* externalHandles.imageResolution(2));
end

handles.currentZSlice = get(externalHandles.zSliceSlider,'Value');
if(externalHandles.startpointAvailable && externalHandles.imageResolutionAvailable)
    handles.currentZSlice =  externalHandles.imageStartpoint(3) + ((handles.currentZSlice - 1) .* externalHandles.imageResolution(3));
end

handles.axesX = externalHandles.axesX;
handles.axesY = externalHandles.axesY;
handles.axesZ = externalHandles.axesZ;

handles.startpointAvailable = externalHandles.startpointAvailable;
handles.imageResolutionAvailable = externalHandles.imageResolutionAvailable;
if(handles.startpointAvailable)
    handles.imageStartpoint = externalHandles.imageStartpoint;
end
if(handles.imageResolutionAvailable)
    handles.imageResolution = externalHandles.imageResolution;
end
handles.cmap = colormap(externalHandles.fig);

%%%%%%%% intial settings for figure
handles.fig = figure;
set(handles.fig,'MenuBar', 'none');
set(handles.fig,'ToolBar', 'none');
set(handles.fig,'PaperSize',[handles.pageWidth handles.pageHeight]);
set(handles.fig,'PaperUnits','centimeters');
set(handles.fig,'PaperType','<custom>');
set(handles.fig,'PaperPositionMode','auto');
set(handles.fig,'Units','centimeters');
set(handles.fig,'Position',[0 0 handles.pageWidth handles.pageHeight]);



%%%%%%%%%%%%%%%%  creating menus
handles.exportMenu = uimenu(handles.fig,'Label','Export');

m = uimenu(handles.exportMenu,'Label','Export as image...','Callback',@(m,eventdata)exportMenu_exportAsImage_Callback(m,eventdata,guidata(m)));
handles.exportMenu_exportAsImage = m;

m = uimenu(handles.exportMenu,'Label','Export as TIKZ...','Callback',@(m,eventdata)exportMenu_exportAsTikz_Callback(m,eventdata,guidata(m)));
handles.exportMenu_exportAsTikz = m;

m = uimenu(handles.exportMenu,'Label','Copy to clipboard','Callback',@(m,eventdata)exportMenu_copyToClipboard_Callback(m,eventdata,guidata(m)));
handles.exportMenu_copyToClipboard = m;


handles.settingsMenu = uimenu(handles.fig,'Label','Settings');

m = uimenu(handles.settingsMenu,'Label','Colobar','Callback',@(m,eventdata)settingsMenu_toggleColorbar_Callback(m,eventdata,guidata(m)));
handles.settingsMenu_toggleColorbar = m;
if(handles.displayColobar)
   set(handles.settingsMenu_toggleColorbar,'Checked','on');
else
   set(handles.settingsMenu_toggleColorbar,'Checked','off'); 
end

m = uimenu(handles.settingsMenu,'Label','Title','Callback',@(m,eventdata)settingsMenu_toggleTitle_Callback(m,eventdata,guidata(m)),'Separator','on');
handles.settingsMenu_toggleTitle = m;
if(handles.displayTitle)
   set(handles.settingsMenu_toggleTitle,'Checked','on');
else
   set(handles.settingsMenu_toggleTitle,'Checked','off'); 
end


m = uimenu(handles.settingsMenu,'Label','Title Font Name');
handles.settingsMenu_TitleFontName = m;
for idx = 1:size(handles.fontList,2)
    m = uimenu(handles.settingsMenu_TitleFontName,'Label',handles.fontList{idx},'Callback',@(m,eventdata)settingsMenu_TitleFontName_Callback(m,eventdata,guidata(m)),'UserData',handles.fontList{idx});
    handles.settingsMenu_TitleFontNameEntries(idx) = m;
    if(strcmp(handles.fontList{idx},handles.titleFontName))
        set(handles.settingsMenu_TitleFontNameEntries(idx),'Checked','on');
    else
        set(handles.settingsMenu_TitleFontNameEntries(idx),'Checked','off');
    end
end

m = uimenu(handles.settingsMenu,'Label','Title Font Size');
handles.settingsMenu_TitleFontSize = m;
for idx = 1:size(handles.sizeList,2)
    m = uimenu(handles.settingsMenu_TitleFontSize,'Label',handles.sizeList{idx},'Callback',@(m,eventdata)settingsMenu_TitleFontSize_Callback(m,eventdata,guidata(m)),'UserData',str2double(handles.sizeList(idx)));
    handles.settingsMenu_TitleFontSizeEntries(idx) = m;
    if(str2double(handles.sizeList(idx))==handles.titleFontSize)
        set(handles.settingsMenu_TitleFontSizeEntries(idx),'Checked','on');
    else
        set(handles.settingsMenu_TitleFontSizeEntries(idx),'Checked','off');
    end
end


m = uimenu(handles.settingsMenu,'Label','Axis Font Name','Separator','on');
handles.settingsMenu_AxisFontName = m;
for idx = 1:size(handles.fontList,2)
    m = uimenu(handles.settingsMenu_AxisFontName,'Label',handles.fontList{idx},'Callback',@(m,eventdata)settingsMenu_AxisFontName_Callback(m,eventdata,guidata(m)),'UserData',handles.fontList{idx});
    handles.settingsMenu_AxisFontNameEntries(idx) = m;
    if(strcmp(handles.fontList{idx},handles.axisFontName))
        set(handles.settingsMenu_AxisFontNameEntries(idx),'Checked','on');
    else
        set(handles.settingsMenu_AxisFontNameEntries(idx),'Checked','off');
    end
end


m = uimenu(handles.settingsMenu,'Label','Axis Font Size');
handles.settingsMenu_AxisFontSize = m;
for idx = 1:size(handles.sizeList,2)
    m = uimenu(handles.settingsMenu_AxisFontSize,'Label',handles.sizeList{idx},'Callback',@(m,eventdata)settingsMenu_AxisFontSize_Callback(m,eventdata,guidata(m)),'UserData',str2double(handles.sizeList(idx)));
    handles.settingsMenu_AxisFontSizeEntries(idx) = m;
    if(str2double(handles.sizeList(idx))==handles.axisFontSize)
        set(handles.settingsMenu_AxisFontSizeEntries(idx),'Checked','on');
    else
        set(handles.settingsMenu_AxisFontSizeEntries(idx),'Checked','off');
    end
end


m = uimenu(handles.settingsMenu,'Label','Axis Labels...','Callback',@(m,eventdata)settingsMenu_axisLabel_Callback(m,eventdata,guidata(m)));
handles.settingsMenu_axisLabel = m;

m = uimenu(handles.settingsMenu,'Label','Colorbar Label...','Callback',@(m,eventdata)settingsMenu_colorbarLabel_Callback(m,eventdata,guidata(m)),'Separator','on');
handles.settingsMenu_colorbarLabel = m;



guidata(handles.fig,handles);

% create and rearrange the view
handles = createView(handles);
handles.swapXY = stateSwapXY;
updateView(handles);

end


function handles = createView(handles)

if(handles.displayColobar) % smaller if colobar is shown. axes set empirically to 90% of the total width
    reductionFactor = 0.9;
else
    reductionFactor = 1;
end
width = [1/3 - handles.border, 1/3 - handles.border, 1/3 - handles.border] .* reductionFactor;
left = [handles.border/2 handles.border*1.5+width(1) width(1)+width(2)+handles.border*2.5];
height = (1-handles.border);
bottom = (handles.border/2) + ((1-handles.border)-height)/2;

% create axes
handles.ax(1) = axes('Parent',handles.fig,'Box','on');
handles.ax(2) = axes('Parent',handles.fig,'Box','on');
handles.ax(3) = axes('Parent',handles.fig,'Box','on');

set(handles.ax(1),'OuterPosition',[left(1),bottom,width(1),height]);
set(handles.ax(2),'OuterPosition',[left(2),bottom,width(2),height]);
set(handles.ax(3),'OuterPosition',[left(3),bottom,width(3),height]);

% copy children object from miniViewer handles, arrange CLim, XLim, YLim,
% Title, Labels
copyobj(findobj('Type','image','Parent',handles.axesX),handles.ax(1));
clim = get(handles.axesX,'CLim');
set(handles.ax,'CLim',clim);
set(handles.ax(1),'XLim',get(handles.axesX,'XLim'));
set(handles.ax(1),'YLim',get(handles.axesX,'YLim'));
if(handles.startpointAvailable && handles.imageResolutionAvailable)
    handles.t(1) = title(handles.ax(1),sprintf('%s = %6.2f',handles.xLabel, handles.currentXSlice));
else
    handles.t(1) = title(handles.ax(1),sprintf('%s = %i',handles.xLabel, handles.currentXSlice));
end
handles.l(1) = xlabel(handles.ax(1),handles.zLabel);
handles.l(2) = ylabel(handles.ax(1),handles.yLabel);

copyobj(findobj('Type','image','Parent',handles.axesY),handles.ax(2));
clim = get(handles.axesY,'CLim');
set(handles.ax(2),'CLim',clim);
set(handles.ax(2),'XLim',get(handles.axesY,'XLim'));
set(handles.ax(2),'YLim',get(handles.axesY,'YLim'));
if(handles.startpointAvailable && handles.imageResolutionAvailable)
    handles.t(2) = title(handles.ax(2),sprintf('%s = %6.2f',handles.yLabel, handles.currentYSlice));
else
    handles.t(2) = title(handles.ax(2),sprintf('%s = %i',handles.yLabel, handles.currentYSlice));
end
handles.l(3) = xlabel(handles.ax(2),handles.zLabel);
handles.l(4) = ylabel(handles.ax(2),handles.xLabel);

copyobj(findobj('Type','image','Parent',handles.axesZ),handles.ax(3));
clim = get(handles.axesZ,'CLim');
set(handles.ax(3),'CLim',clim);
set(handles.ax(3),'XLim',get(handles.axesZ,'XLim'));
set(handles.ax(3),'YLim',get(handles.axesZ,'YLim'));
if(handles.startpointAvailable && handles.imageResolutionAvailable)
    handles.t(3) = title(handles.ax(3),sprintf('%s = %6.2f',handles.zLabel, handles.currentZSlice));
else
    handles.t(3) = title(handles.ax(3),sprintf('%s = %i',handles.zLabel, handles.currentZSlice));
end
handles.l(5) = xlabel(handles.ax(3),handles.yLabel);
handles.l(6) = ylabel(handles.ax(3),handles.xLabel);

% axes settings
set(handles.ax,'YDir','reverse');
set(handles.ax,'TickDir','out');
set(handles.ax,'ActivePositionProperty','OuterPosition');
set(handles.ax,'TitleFontSizeMultiplier',1);
set(handles.ax,'LabelFontSizeMultiplier',1);
set(handles.ax,'DataAspectRatio',[1 1 1]);
set(handles.ax,'DataAspectRatioMode','manual');
set(handles.ax,'PlotBoxAspectRatioMode','auto');

% add colorbar if required
if(handles.displayColobar)
    pos = get(handles.ax(3),'Position');
    cB = colorbar;
    set(handles.ax(3),'Position',pos);
    handles.l(7) = ylabel(cB,handles.quantityUnit);
    set(handles.l,'FontName',handles.axisLabelFontName);
    set(handles.l,'FontSize',handles.axisLabelFontSize);
    set(handles.l,'FontWeight',handles.axisLabelFontWeight);  
end

% set the fonts
set(handles.t,'FontName',handles.titleFontName);
set(handles.t,'FontSize',handles.titleFontSize);
set(handles.t,'FontWeight',handles.titleFontWeight);

set(handles.ax,'FontName',handles.axisFontName);
set(handles.ax,'FontSize',handles.axisFontSize);
set(handles.ax,'FontWeight',handles.axisFontWeight);

% adapt the figure's colormap according to the colormap used in the
% MiniViewer
colormap(handles.fig,handles.cmap);

guidata(handles.fig,handles);

end



function updateView(handles)

if(handles.displayColobar)
    reductionFactor = 0.9;
else
    reductionFactor = 1;
end


% recalculate axis position
width = [1/3 - handles.border, 1/3 - handles.border, 1/3 - handles.border] .* reductionFactor;
left = [handles.border/2 handles.border*1.5+width(1) width(1)+width(2)+handles.border*2.5];
height = (1-handles.border);
bottom = (handles.border/2) + ((1-handles.border)-height)/2;


if(handles.displayColobar)
    pos = get(handles.ax(3),'Position');
    cB = colorbar(handles.ax(3));
    set(handles.ax(3),'Position',pos);
    handles.l(7) = ylabel(cB,handles.quantityUnit);
    set(handles.l(7),'FontName',handles.axisLabelFontName);
    set(handles.l(7),'FontSize',handles.axisLabelFontSize);
    set(handles.l(7),'FontWeight',handles.axisLabelFontWeight);
else
    colorbar(handles.ax(3),'off');
end

set(handles.ax(1),'OuterPosition',[left(1),bottom,width(1),height]);
set(handles.ax(2),'OuterPosition',[left(2),bottom,width(2),height]);
set(handles.ax(3),'OuterPosition',[left(3),bottom,width(3),height]);

camPos1 = get(handles.ax(1),'CameraPosition');
camPos2 = get(handles.ax(2),'CameraPosition');
camPos3 = get(handles.ax(3),'CameraPosition');

camTarget1 = get(handles.ax(1),'CameraTarget');
camTarget2 = get(handles.ax(2),'CameraTarget');
camTarget3 = get(handles.ax(3),'CameraTarget');

camViewAngle1 = get(handles.ax(1),'CameraViewAngle');
camViewAngle2 = get(handles.ax(2),'CameraViewAngle');
camViewAngle3 = get(handles.ax(3),'CameraViewAngle');

maxViewAngle = max([camViewAngle1 camViewAngle2 camViewAngle3]);
maxCamPos = max([camPos1(3) camPos2(3) camPos3(3)]);

set(handles.ax(1),'CameraPosition',[camPos1(1) camPos1(2) maxCamPos]);
set(handles.ax(2),'CameraPosition',[camPos2(1) camPos2(2) maxCamPos]);
set(handles.ax(3),'CameraPosition',[camPos3(1) camPos3(2) maxCamPos]);

set(handles.ax(1),'CameraViewAngle',maxViewAngle);
set(handles.ax(2),'CameraViewAngle',maxViewAngle);
set(handles.ax(3),'CameraViewAngle',maxViewAngle);

set(handles.ax(1),'CameraTarget',camTarget1);
set(handles.ax(2),'CameraTarget',camTarget2);
set(handles.ax(3),'CameraTarget',camTarget3);

if(strcmp(handles.swapXY,'on'))    
    set(handles.ax(1), 'View', [-90, 90]);
    set(handles.ax(2), 'View', [-90, 90]);
    set(handles.ax(3), 'View', [-90, 90]);
else
    set(handles.ax(1), 'View', [0, 90]);
    set(handles.ax(2), 'View', [0, 90]);
    set(handles.ax(3), 'View', [0, 90]);            

end

guidata(handles.fig,handles);

end


%%%%%%%%% Menu handling
function exportMenu_exportAsTikz_Callback(~,~,handles)

filter = {'*.tex';};
[filename, pathname, filterIndex] = uiputfile(filter,'Save as TIKZ ...');

if(~isempty(filename) && ~isempty(pathname) && ~isempty(filterIndex) && filename ~= 0 && pathname ~= 0 && filterIndex ~= 0) 
    chosenFilter = filter{filterIndex};
    
    if(~strfind(filename,chosenFilter(2:end)))
        filename = [filename chosenFilter(2:end)];
    end
    
    matlab2tikz('filename',fullfile(pathname,filename),'figurehandle',handles.fig,'width','1\textwidth');
end

end


function exportMenu_exportAsImage_Callback(~,~,handles)

filter = {'*.eps';'*.pdf';'*.emf';'*.png';'*.jpg';'*.tiff';'*.bmp';};
[filename, pathname, filterIndex] = uiputfile(filter,'Save as image ...');

if(filterIndex > 0)
    chosenFilter = filter{filterIndex};
    
    if(~strfind(filename,chosenFilter(2:end)))
        filename = [filename chosenFilter(2:end)];
    end
    
    option = '';
    switch filterIndex
        case 1
            option = '-depsc';
        case 2
            option = '-dpdf';
        case 3
            option = '-dmeta';
        case 4
            option = '-dpng';
        case 5
            option = '-djpeg';
        case 6
            option = '-dtiff';
        case 7
            option = '-dbmp';
    end
    
    print(handles.fig,fullfile(pathname,filename),option,'-r300');
end
end


function exportMenu_copyToClipboard_Callback(~,~,handles)

if(ispc)
    set(handles.fig,'PaperPositionMode','auto');
    print(handles.fig,'-dmeta','-r0');
else
    disp('Export to clipboard not supported for non Windows systems');
end

end


function settingsMenu_toggleColorbar_Callback(h,~,handles)

if(handles.displayColobar == 0)
    handles.displayColobar = 1;
    set(h,'Checked','on');
else
    handles.displayColobar = 0;
    set(h,'Checked','off');
end

guidata(handles.fig,handles);

updateView(handles);

end



function settingsMenu_toggleTitle_Callback(h,~,handles)

if(handles.displayTitle == 0)
    handles.displayTitle = 1;
    set(h,'Checked','on');
    set(handles.t,'Visible','on');
else
    handles.displayTitle = 0;
    set(h,'Checked','off');
    set(handles.t,'Visible','off');
end

guidata(handles.fig,handles);

end






function settingsMenu_TitleFontSize_Callback(h,~,handles)

    fontSize = get(h,'UserData');
    handles.titleFontSize = fontSize;
    set(handles.t,'FontSize',handles.titleFontSize);
    
    for idx = 1:size(handles.sizeList,2)
        if(str2double(handles.sizeList(idx))==handles.titleFontSize)
            set(handles.settingsMenu_TitleFontSizeEntries(idx),'Checked','on');
        else
            set(handles.settingsMenu_TitleFontSizeEntries(idx),'Checked','off');
        end
    end
    
    guidata(handles.fig,handles);
    updateView(handles);
    
end


function settingsMenu_TitleFontName_Callback(h,~,handles)

    fontName = get(h,'UserData');
    handles.titleFontName = fontName;
    set(handles.t,'FontName',handles.titleFontName);

    for idx = 1:size(handles.fontList,2)
        if(strcmp(handles.fontList{idx},handles.titleFontName))
            set(handles.settingsMenu_TitleFontNameEntries(idx),'Checked','on');
        else
            set(handles.settingsMenu_TitleFontNameEntries(idx),'Checked','off');
        end
    end
    
    guidata(handles.fig,handles);
    updateView(handles);
    
end



function settingsMenu_AxisFontSize_Callback(h,~,handles)

    fontSize = get(h,'UserData');
    handles.axisLabelFontSize = fontSize;
    set(handles.ax,'FontSize',handles.axisLabelFontSize);
    set(handles.l(1:6),'FontSize',handles.axisLabelFontSize);
    
    
    for idx = 1:size(handles.sizeList,2)
        if(str2double(handles.sizeList(idx))==handles.axisLabelFontSize)
            set(handles.settingsMenu_AxisFontSizeEntries(idx),'Checked','on');
        else
            set(handles.settingsMenu_AxisFontSizeEntries(idx),'Checked','off');
        end
    end
    set(handles.t,'FontSize',handles.titleFontSize);

    guidata(handles.fig,handles);
    updateView(handles);
    
end


function settingsMenu_AxisFontName_Callback(h,~,handles)

    fontName = get(h,'UserData');
    handles.axisFontName = fontName;
    set(handles.ax,'FontName',handles.axisFontName);
    set(handles.l(1:6),'FontName',handles.axisFontName);
    
    for idx = 1:size(handles.fontList,2)
        if(strcmp(handles.fontList{idx},handles.axisFontName))
            set(handles.settingsMenu_AxisFontNameEntries(idx),'Checked','on');
        else
            set(handles.settingsMenu_AxisFontNameEntries(idx),'Checked','off');
        end
    end
    set(handles.t,'FontName',handles.titleFontName);
    
    guidata(handles.fig,handles);
    updateView(handles);
    
end



function settingsMenu_colorbarLabel_Callback(~,~,handles)
    prompt = {'Colorbar label:'};
    dlg_title = 'Specify colorbar label';
    num_lines = 1;
    defaultans{1} = handles.quantityUnit;
    answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    handles.quantityUnit = answer{1,1};
    try 
        set(handles.l(7),'String',handles.quantityUnit);
    catch
        
    end
    guidata(handles.fig,handles);
    
end



function settingsMenu_axisLabel_Callback(~,~,handles)
    prompt = {'X-axis','Y-axis','Z-axis'};
    dlg_title = 'Specify axis labels';
    num_lines = 1;
    defaultans = {handles.xLabel,handles.yLabel,handles.zLabel};
    answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    handles.xLabel = answer{1};
    handles.yLabel = answer{2};
    handles.zLabel = answer{3};
    try 
        set(handles.l(1),'String',handles.zLabel);
        set(handles.l(2),'String',handles.yLabel);
        set(handles.l(3),'String',handles.zLabel);
        set(handles.l(4),'String',handles.xLabel);
        set(handles.l(5),'String',handles.yLabel);
        set(handles.l(6),'String',handles.xLabel);
        
        if(handles.startpointAvailable && handles.imageResolutionAvailable)
            set(handles.t(1),'String',sprintf('%s = %6.2f',handles.xLabel, handles.currentXSlice));
            set(handles.t(2),'String',sprintf('%s = %6.2f',handles.yLabel, handles.currentYSlice));
            set(handles.t(3),'String',sprintf('%s = %6.2f',handles.zLabel, handles.currentZSlice));
        else
            set(handles.t(1),'String',sprintf('%s = %i',handles.xLabel, handles.currentXSlice));
            set(handles.t(2),'String',sprintf('%s = %i',handles.yLabel, handles.currentYSlice));
            set(handles.t(3),'String',sprintf('%s = %i',handles.zLabel, handles.currentZSlice));
        end
    catch
        
    end
    guidata(handles.fig,handles);
    
end
