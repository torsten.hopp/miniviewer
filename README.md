# miniViewer 
(aka <b>mini</b>(malistic)(volume)<b>Viewer</b>)</h1> is a MATLAB software for viewing and analyzing 3D intensity and 3D RGB data. 

It offers slice-based visualization (multiplanar reconstruction) as well as several methods for volume visualization. It features windowing, zooming, moving tools, interactive cursors, distance measurement, histogram analysis, colormap adaptions etc. and may be further advanced by tools in a plug-in like manner. 

Please report bugs and requests for features in the gitlab issue tracker!

Written and maintained by T.Hopp  
Institute for Data Processing and Electronics (IPE)  
Karlsruhe Institute of Technology (KIT)  

e-mail: [torsten.hopp@kit.edu](mailto:torsten.hopp@kit.edu) 

This software is provided under BSD 3-Clause License.

## Table of contents
[[_TOC_]]
## Prerequisites

* MATLAB R2018b or later but may run on older MATLAB versions (maybe without some features)
* MATLAB Image Processing Toolbox for certain functions required.

## How to use it
Starting miniViewer works by using the MATLAB command line interface, e.g.:
```matlab
[handle] = miniViewer(volumeImage)
```

The matlab function miniViewer can get several combinations of input parameters. 

### Basic parameters
* **Usage with one parameter**<br>
    (1) first parameter (```volumeImage```) is the volume to be displayed. <br>
        -> opens the miniViewer GUI to display the volume slice-based with standard settings. <br>

```matlab
[handle] = miniViewer(volumeImage)
```

* **Usage with two or more parameters**<br>
    (1) first parameter (```volumeImage```) is the volume to be displayed <br>
    (2) advanced parameter(s), see list below.

```matlab
[handle] = miniViewer(volumeImage,'parameter',value)
```

### Advanced parameters
* ```'file'```<br>
path to a file containing the volume to be displayed
* ```'var'```<br>
variable name within the given file. Must be available if 'file' is also given
* ```'resolution'```<br>
resolution information. [3x1] array giving resolution of the image in x,y and z. OR [scalar] giving an isotropic resolution for x,y and z. Note: interpreted to be in m (SI units)!
* ```'startpoint'```<br>
startpoint of the image in real world coordinate system. [3x1] array giving the x,y,z coordinates. Note: interpreted to be in m (SI units)!
* ```'initSlice'```<br>
initial slices after opening. [3x1] array giving the x,y and z slice. Note that the values have to be inside the bounds of the volume size! By default the middle slice in x/y/z directions are taken as initial slices.
* ```'title'```<br>
title of the window as a character array. By default the variable name is used.
* ```'linkTo'```<br>
handle to an already opened miniViewer instance allows to link two miniViewers if the volume is the same size.
* ```'histogram'```<br>
logical switch (true/false) to turn on histogram feature + interactive windowing. By default histogram is enabled. It might be wise to disable the histogram for very large volumes to avoid long computation times.
* ```'windowCenter'```<br>
Numeric value expressing the center of the intensity mapping window.
* ```'windowWidth'```<br>
Numeric value expressing the width of the intensity mapping window.

## Examples
#### Displaying a variable from the MATLAB workspace 
The variable is given as single argument to miniViewer

```matlab
load wmri;
miniViewer(X);
```

#### Passing initial slices
The variable is given as argument to miniViewer. The additional argument 'initSlice' is given with the initial slices to be displayed as [x y z], here slice 1 in x-direction, slice 2 in y-direction, slice 3 in z-direction.

```matlab
load wmri;
miniViewer(X,'initSlice',[1 2 3]);
```


#### Passing anisotropic resolution information
The variable (image to be displayed) is given as first argument, the resolution is given as additional argument in vector format, e.g. [0.00091 0.00091 0.003] in m!

```
load wmri;
resolution = [0.00091 0.00091 0.003];
miniViewer(X,'resolution',resolution);
```


#### Passing isotropic resolution information
The variable (image to be displayed) is given as first argument, the resolution is given as additional argument in scalar format, assuming an isotropic resolution of the volume

```matlab
load wmri;
resolution = [0.00091];
miniViewer(X,'resolution',resolution);
```


#### Passing image resolution and startpoint information 
The variable (image to be displayed) is given as first argument, the resolution and the image startpoint is given as additional argument in scalar format, assuming an anisotropic resolution of the volume. Note that the view will be scaled according to the given anisotropic resolution.

```matlab
load wmri;
resolution = [0.00091 0.00091 0.0025];
startpoint = [-0.1 -0.1 0];
miniViewer(X,'resolution',resolution,'startpoint',startpoint);
```

#### Loading a volume from a MATLAB file and display it
The first argument is neglected as the image content is loaded from the file. In the additional arguments the filepath and the variable name to be loaded from the particular file is given.

```matlab
miniViewer([],'file','pathTo/MatFile.mat','var','variableName');
```


#### Link axes in two instances
This mode allows to browse in two instances side by side. Note that volumeImage1 and volumeImage2 are required to be of the same size.

```matlab
handle1 = miniViewer(volumeImage1);
handle2 = miniViewer(volumeImage2,'linkTo',handle1);
```

## License
This software is provided under BSD 3-Clause License. See LICENSE for details.

## Acknowledgment of used libraries
This software uses the following libraries or parts of the following libraries:
- Viewer3D by Dirk-Jan Kroon -- https://de.mathworks.com/matlabcentral/fileexchange/21993-viewer3d
- vol3d v2 by Oliver Woodford  -- https://de.mathworks.com/matlabcentral/fileexchange/22940-vol3d-v2
- draggable by Francois Bouffard -- https://de.mathworks.com/matlabcentral/fileexchange/4179-draggable 
- matlab2tikz/matlab2​tikz by Nico Schlömer -- https://de.mathworks.com/matlabcentral/fileexchange/22022-matlab2tikz-matlab2tikz

See subfolder helpers/lib for licenses.
