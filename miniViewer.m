function handle = miniViewer(varargin)
%miniViewer - Displays 3D images slicewise and allows volume rendering
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allows displaying of 3D images slicewise and as a volume rendering.
% Includes functionality to browse through slices in each direction of
% space, windowing, zooming, shifting, colormap adaption etc.
%
%
% Inputs:
%    varargin - structure of parameters passed to the miniViewer
%    
%    BASIC PARAMETERS
%       usage with one parameter
%           (1) first parameter is the volume to be displayed. 
%               opens the miniViewer GUI to display the volume slice-based 
%               with standard settings. 
%   
%       usage with two or more parameters
%           (1) first parameter is the volume to be displayed
%           (2) advanced parameter(s), see list below.
%                                           
%
%    USAGE OF ADVANCED PARAMETERS
%           'file'      -   path to a file containing the volume to be
%                           displayed
%           'var'       -   variable name within the given file. Must be
%                           available if 'file' is also given
%           'resolution'-   resolution information. [3x1] array giving
%                           resolution of the image in x,y and z. 
%                           OR [scalar] giving an isotropic resolution for
%                           x,y and z. Note: in m (SI units)!
%           'startpoint'-   startpoint of the image in real world
%                           coordinate system. [3x1] array giving the 
%                           x,y,z coordinates. Note: in m (SI units)!
%           'initSlice' -   initial slices after opening. [3x1] array
%                           giving the x,y and z slice
%           'title'     -   title of the window. By default the variable
%                           name is used
%           'linkTo'    -   handle to an already opened miniViewer instance
%                           allows to link two miniViewers if the volume 
%                           is the same size.
%           'histogram' -   logical switch (true/false) to turn on
%                           histogram feature + interactive windowing. By
%                           default histogram is enabled.
%           'windowCenter'- center value of intensity level windowing. By
%                           default using the average intensity. 
%           'windowWidth'-  width of the intensity level windowing. By
%                           default the full range of intensities.
%
% Outputs:
%    handle - the handle to the figure object
%
% Example Usage:
%
%       DISPLAYING A VARIABLE FROM THE WORKSPACE
%       The variable is given as single argument to miniViewer
%       ===================================================================
%       load wmri;
%       miniViewer(X);
%
%
%       DISPLAYING A VARIABLE FROM THE WORKSPACE + GIVING INITIAL SLICES
%       The variable is given as argument to miniViewer. The additional
%       argument 'initSlice' is given with the initial slices to be
%       displayed as [x y z].
%       ===================================================================
%       load wmri;
%       miniViewer(X,'initSlice',[1 2 3]);
%
%
%       DISPLAYING A VARIABLE FROM THE WORKSPACE + GIVING RESOLUTION
%       INFORMATION
%       The variable (image to be displayed) is given as first argument,
%       the resolution is given as additional argument in vector format, e.g.
%       [0.00091 0.00091 0.003] in m!
%       ===================================================================
%       load wmri;
%       resolution = [0.00091 0.00091 0.003];
%       miniViewer(X,'resolution',resolution);
%
%
%       DISPLAYING A VARIABLE FROM THE WORKSPACE + GIVING RESOLUTION
%       ISOTROPIC INFORMATION
%       The variable (image to be displayed) is given as first argument,
%       the resolution is given as additional argument in scalar format,
%       assuming an isotropic resolution of the volume
%       ===================================================================
%       load wmri;
%       resolution = [0.00091];
%       miniViewer(X,'resolution',resolution);
%
%
%       DISPLAYING A VARIABLE FROM THE WORKSPACE + GIVING RESOLUTION AND
%       IMAGE STARTPOINT INFORMATION
%       The variable (image to be displayed) is given as first argument,
%       the resolution and the image startpoint is given as additional 
%       argument in scalar format, assuming an isotropic resolution of the 
%       volume
%       ===================================================================
%       load wmri;
%       resolution = [0.00091 0.00091 0.00091];
%       startpoint = [-0.1 -0.1 0];
%       miniViewer(X,'resolution',resolution,'startpoint',startpoint);
%
%
%
%       LOADING A VOLUME FROM A MATLAB FILE AND DISPLAY IT
%       The first argument is neglected as the image content is loaded from 
%       the file. In the additional arguments the filepath and the variable 
%       name to be loaded from the particular file is given.
%       ===================================================================
%       miniViewer([],'file','pathTo/MatFile.mat','var','variableName');
%
%
%
% Author: Torsten Hopp
%         Karlsruhe Institute of Technology
%         Institute for Data Processing and Electronics
%
% email:  torsten.hopp@kit.edu
% Website: http://www.ipe.kit.edu
%
%
% This software is provided under BSD 3-Clause License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% addig subpaths
currentFile = mfilename('fullpath');
[miniViewerDir] = fileparts(currentFile);
% check if already in path
if isempty(strfind(path,fullfile(miniViewerDir,'mainGUI')))
    addpath(fullfile(miniViewerDir,'mainGUI'));
end

if(nargin == 0)
    
    disp('No input variable given. Exiting...');
    return;
    
    % To Do: add a dialog in this case?
    
elseif(nargin == 1)  % usually simplest case: just display the variable
    if(isnumeric(varargin{1}) || islogical(varargin{1}))
        %check: 3D image or 4D RGB?
        varSize = size(varargin{1});
        if(size(varSize,2) == 3 || (size(varSize,2) == 4 && varSize(4) == 3))
            if(isreal(varargin{1}))
                handle = miniViewerGUI(varargin{1});
                set(handle,'Name',[get(handle,'Name'), ' -- [', inputname(1), ']']);
            else
                error('[MiniViewer:] Can''t handle complex volums.');
            end
        else
            error('[MiniViewer:] Not a valid volume. 3D or 4D (size(4th dim) == 3 required)');
        end
    else
        error('[MiniViewer:] Not a valid volume. Variable is not numeric.');
    end
elseif(nargin >= 2) % advanced options --> parsing the inputs. 
    
    parser = inputParser();
    validationFcnRes = @(x) isnumeric(x) && (isscalar(x) || length(x) ==3);
    addOptional(parser,'resolution',[],validationFcnRes);    
    validationFcnStartpoint = @(x) isnumeric(x) && length(x) ==3;
    addOptional(parser,'startpoint',[],validationFcnStartpoint);
    
    validationFcnTitle = @(x) ischar(x);
    addOptional(parser,'title',[],validationFcnTitle);
    
    validationFcnFile = @(x) exist(x,'file');
    addOptional(parser,'file',[],validationFcnFile);
    addOptional(parser,'var',[]);    
    validationFcnSlices = @(x) isnumeric(x) && length(x) == 3;
    addOptional(parser,'initSlice',[],validationFcnSlices);
    
    validationFcnLink = @(x) (strcmp(get(x,'Type'),'figure') && strcmp(get(x,'Tag'),'miniViewer'));
    addOptional(parser,'linkTo',[],validationFcnLink);
    
    validationFcnHist = @(x) islogical(x) && length(x) ==1;
    addOptional(parser,'histogram',[],validationFcnHist); 
    
    validationFcnWindowCenter = @(x) isnumeric(x) && length(x) ==1;
    addOptional(parser,'windowCenter',[],validationFcnWindowCenter); 
    
    validationFcnWindowWidth = @(x) isnumeric(x) && length(x) ==1;
    addOptional(parser,'windowWidth',[],validationFcnWindowWidth); 
    
    parse(parser,varargin{2:end});
    pResult = parser.Results;
    
   
    % if file and var are given: load the variable from the file and
    % use it as volume
    if(~isempty(pResult.file) && ~isempty(pResult.var))  % in this case the volume given as first parameter is neglected!
        disp('   loading from file...');
        % check the file Info: variable in the file?
        if(~exist(pResult.file,'file'))
            error('[MiniViewer:] File not found');
        end
        fileInfo = whos('-file', pResult.file);
        varExists = 0;
        varIndex = 0;
        for idx = 1:size(fileInfo,1)
            if(strcmp(fileInfo(idx).name,pResult.var))
                varExists = 1;
                varIndex = idx;
            end
        end
        if(varExists)
            % check: 3D image or 4D RGB?
            varSize = fileInfo(varIndex).size;
            if(size(varSize,2) == 3 || (size(varSize,2) == 4 && varSize(4) == 3))
                load(pResult.file,pResult.var);
                disp('   opening MiniViewer...');
                try
                    eval(sprintf('handle = miniViewerGUI(%s,pResult);',pResult.var));
                    if(isempty(pResult.title))
                        windowTitle = [pResult.file, ' : ',pResult.var];
                    else
                        windowTitle = pResult.title;
                    end
                    set(handle,'Name',[get(handle,'Name'), ' -- [', windowTitle ,']']);
                catch
                    error('[MiniViewer:] Unspecified error');
                end
            else
                error('[MiniViewer:] Not a valid volume. 3D or 4D (size(4th dim) == 3 required)');
            end
        else
            error('[MiniViewer:] Variable not found in the given file');
        end
    elseif(~isempty(pResult.file) && isempty(pResult.var))
        error('[MiniViewer:] No variable name given! ''File'' argument always has to go along with the ''Var'' argument!');
    elseif(~isempty(pResult.file) && isempty(pResult.var))
        error('[MiniViewer:] No filename given! ''Var'' argument always has to go along with the ''File'' argument!');
    else % if no filename and variable is given --> use the volume and send the parsed arguments to the main GUI
        %check: 3D image or 4D RGB?
        if(isnumeric(varargin{1}) || islogical(varargin{1}))
            %check: 3D image or 4D RGB?
            varSize = size(varargin{1});
            if(size(varSize,2) == 3 || (size(varSize,2) == 4 && varSize(4) == 3))
                handle = miniViewerGUI(varargin{1},pResult);
                if(isempty(pResult.title))
                    windowTitle = inputname(1);
                else
                    windowTitle = pResult.title;
                end
                set(handle,'Name',[get(handle,'Name'), ' -- [', windowTitle, ']']);
            else
                error('[MiniViewer:] Not a valid volume. 3D or 4D (size(4th dim) == 3 required)');
            end
        else
            error('[MiniViewer:] Not a valid volume. Variable is not numeric.');
        end
    end
else
    error('[MiniViewer:] Unsupported number of arguments. For correct usage see comments in miniViewer.m');
end

end

